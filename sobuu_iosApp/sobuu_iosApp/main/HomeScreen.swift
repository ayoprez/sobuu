import SwiftUI
import shared

enum BookStatusType {
    case CURRENTLY_READING
    case ALREADY_READ
    case GIVE_UP
}

struct HomeScreen: View {
    let appName: String = SharedRes.strings().general_appName.stringResource()
    var body: some View {
        NavigationView {
            VStack {
                Text("Book Title")
                Image(systemName: "book")
            }
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: .infinity,
                alignment: .topLeading
            )
            .overlay(
                ProfileView(
                    isLoading: false, 
                    hasError: false,
                    errorText: nil,
                    userFirstname: "John",
                    userLastname: "Connor",
                    username: "JoCo"
                )
                    .padding(.trailing, 20)
                    .offset(x: 0, y: -50),
                alignment: .topTrailing
            )
            .background(ColorToken.greenSheen)
            .navigationTitle(
                Text(appName)
                    .foregroundColor(ColorToken.darkLava)
                    .font(.SolwayAppLogo)
            )
        }
        .navigationBarHidden(true)
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
    }
}
