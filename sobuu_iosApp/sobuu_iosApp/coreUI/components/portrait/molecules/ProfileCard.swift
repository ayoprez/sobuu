import SwiftUI

struct ProfileCard: View {
    let firstName: String?
    let lastName: String?
    let username: String?
    
    let textColor = ColorToken.darkLava
    let topPadding: CGFloat = 20
    let bottomPadding: CGFloat = 30
    let startPadding: CGFloat = 50
    
    var body: some View {
        Text((firstName != nil && lastName != nil) ? "\(firstName!) \(lastName!)" : (username != nil) ? "\(username!)" : "")
            .frame(maxWidth: .infinity)
            .padding(.top, topPadding)
            .padding(.bottom, bottomPadding)
            .padding(.leading, startPadding)
            .font(CustomFont.bodyLarge)
            .foregroundStyle(textColor)
    }
}

#Preview {
    ProfileCard(
        firstName: "Indra",
        lastName: "Cosmos",
        username: "Inco"
    )
}
