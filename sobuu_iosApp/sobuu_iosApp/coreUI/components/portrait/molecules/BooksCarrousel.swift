import SwiftUI

struct BooksCarrousel: View {
    let count: Int
    let content: (index) -> View
    
    var body: some View {
        GeometryReader { geo in
            ZStack() {
                ForEach(0..<count, id: \.self) { index in
                    content(index)
                }
            }
            .padding(.leading, 50)
            .gesture(
                DragGesture()
                    .onEnded({ value in
                        let threshold: CGFloat = 50
                        if (value.translation.width > threshold) {
                            withAnimation {
                                currentIndex = max(0, self.currentIndex - 1)
                            }
                        } else if(value.translation.width < -threshold) {
                            withAnimation {
                                currentIndex = min(self.bookList!.count - 1, self.currentIndex + 1)
                            }
                        }
                    })
            )
        }
    }
}

#Preview {
    BooksCarrousel(
        content: {}
    )
}
