import SwiftUI

struct ProfileLoading: View {
    
    let backgroundColor = ColorToken.whiteBlue
    let loadingColor = ColorToken.greenSheen
    
    var body: some View {
        ProgressView()
            .controlSize(.large)
            .progressViewStyle(CircularProgressViewStyle(tint: loadingColor))
    }
}

#Preview {
    ProfileLoading()
}
