import SwiftUI
import shared

struct TopBarWithSearchAndProfile: View {
    
    let navigateToProfileScreen: () -> Void
    let searchFieldValue: String
    let onSearchFieldValueChange: (String) -> Void
    let onSearchButtonClick: () -> Void
    let clearTextButtonClick: () -> Void
    let onSearchFieldFocusChange: (Bool) -> Void
    
    let backgroundColor = ColorToken.whiteBlue
    
    var body: some View {
        VStack {
            TopBarAppName()
            
            GeometryReader { geo in
                HStack {
                    SearchBar(
                        searchFieldValue: searchFieldValue,
                        onSearchFieldValueChange: onSearchFieldValueChange,
                        onSearchButtonClick: onSearchButtonClick,
                        clearText: clearTextButtonClick,
                        onSearchFieldFocusChange: onSearchFieldFocusChange
                    )
                    
                    ProfileIconButton(navigateToProfileScreen: navigateToProfileScreen)
                        .frame(maxWidth: geo.size.width / 4)
                }
            }
            
        }
        .background(backgroundColor)
    }
}

#Preview {
    TopBarWithSearchAndProfile(
        navigateToProfileScreen: {},
        searchFieldValue: "",
        onSearchFieldValueChange: { _ in },
        onSearchButtonClick: {},
        clearTextButtonClick: {},
        onSearchFieldFocusChange: { _ in}
    ).loadCustomFonts()
}
