import SwiftUI

struct LegalTextAcceptanceSwitch: View {
    @State private var toggleChecked: Bool = true
    let termsAction: () -> Void
    let privacyAction: () -> Void
    
    let uncheckedBackgroundColor: Color = ColorToken.darkLava
    let checkedBackgroundColor: Color = ColorToken.vermilion
    
    init(
        termsAction: @escaping () -> Void,
        privacyAction: @escaping () -> Void
    ) {
        self.termsAction = termsAction
        self.privacyAction = privacyAction
    }
    
    var body: some View {
        HStack {
            Toggle("", isOn: $toggleChecked)
                .labelsHidden()
                .tint(toggleChecked ? checkedBackgroundColor : uncheckedBackgroundColor)
            AcceptanceLegalTextWithLinks(termsAction: termsAction, privacyAction: privacyAction)
                .padding(.horizontal, 10)
        }
        .frame(maxWidth: .infinity)
    }
}

#Preview {
    LegalTextAcceptanceSwitch(
        termsAction: {},
        privacyAction: {}
    )
}
