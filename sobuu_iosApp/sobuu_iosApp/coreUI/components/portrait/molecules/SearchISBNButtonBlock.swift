import SwiftUI
import shared

struct SearchISBNButtonBlock: View {
    
    let navigateToAddBookScreenManually: () -> Void
    
    let buttonTextColor = ColorToken.whiteBlue
    let buttonColor = ColorToken.greenSheen
    let textColor = ColorToken.darkLava
    
    var body: some View {
        VStack {
            Text(SharedRes.strings().search_main_addBookInvitation.stringResource())
                .frame(maxWidth: .infinity)
                .font(CustomFont.bodyLarge)
                .foregroundColor(textColor)
            
            Button(action: navigateToAddBookScreenManually) {
                    Label(
                        title:{ Text(SharedRes.strings().search_main_addBookManually.stringResource())
                        },
                        icon: {}
                    )
                    .font(CustomFont.bodyMedium)
                    .foregroundColor(buttonTextColor)
                    .frame(maxWidth: .infinity)
                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                }
            .frame(maxWidth: .infinity)
            .background(buttonColor)
            .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
        }
    }
}

#Preview {
    SearchISBNButtonBlock(navigateToAddBookScreenManually: {})
        .loadCustomFonts()
}
