import SwiftUI
import shared

struct SearchWarningTextWithEmail: View {
    
    let onEmailClick: () -> Void
    let bookRequestEmail: String
    
    
    let textColor = ColorToken.darkLava
    let paddingSize: CGFloat = 5
    
    var body: some View {
        
        VStack {
            Text(SharedRes.strings().search_main_noBookMessage.stringResource())
                .font(CustomFont.bodyLarge)
                .foregroundStyle(textColor)
            
            Text(bookRequestEmail)
                .onTapGesture(perform: onEmailClick)
                .padding(paddingSize)
                .font(CustomFont.bodyLarge.bold())
                .foregroundStyle(textColor)
        }
    }
}

#Preview {
    SearchWarningTextWithEmail(
        onEmailClick: {},
        bookRequestEmail: "a@b.c"
    )
}
