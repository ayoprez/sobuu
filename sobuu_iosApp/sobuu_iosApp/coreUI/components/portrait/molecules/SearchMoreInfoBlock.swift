import SwiftUI
import shared

struct SearchMoreInfoBlock: View {
    
    let navigateToAddBookScreen: (Bool) -> Void
    let displayMoreInfo: Bool
    let displaySearchFurther: Bool
    let displayWarningWithEmail: Bool
    
    var savedEmailInClipboard: Bool = false
    
    var body: some View {
        if(displayMoreInfo) {
            VStack {
                if (savedEmailInClipboard) {
                    // Show alert
                }
                
                if (displaySearchFurther) {
                    SearchFurtherBlock(navigateToAddBookScreenByISBN: { navigateToAddBookScreen(false)
                    })
                }
                
                if(displayWarningWithEmail) {
                    SearchResultSendEmailOrAddManuallyBlock(
                        navigateToAddBookScreen: { _ in navigateToAddBookScreen(true) },
                        savedOnClipboard: { _ in
                            // Complete in the future. Not important now
                        }
                    )
                }
            }
        }
    }
}

#Preview {
    SearchMoreInfoBlock(
        navigateToAddBookScreen: { _ in },
        displayMoreInfo: true,
        displaySearchFurther: true,
        displayWarningWithEmail: true
    ).loadCustomFonts()
}
