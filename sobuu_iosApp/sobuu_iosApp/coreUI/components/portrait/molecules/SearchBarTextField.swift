import SwiftUI
import shared

struct SearchBarTextField: View {
    @State var text: String
    let onSearchButtonClick: () -> Void
    let clearText: () -> Void
    
    let contentColor: Color = ColorToken.darkLava
    let iconSize: CGFloat = 24
    let radius: CGFloat = 10
    
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundStyle(contentColor)
                .frame(width: iconSize, height: iconSize)
                .onTapGesture(perform: onSearchButtonClick)
            
            TextField(
                SharedRes.strings().home_main_searchBook.stringResource(),
                text: $text
            )
            .font(CustomFont.bodyLarge)
            .foregroundColor(contentColor)
            .lineLimit(1)
            
            Image(systemName: "xmark.circle.fill")
                .foregroundStyle(contentColor)
                .frame(width: iconSize, height: iconSize)
                .opacity(text.isEmpty ? 0 : 1)
                .onTapGesture(perform: clearText)
        }
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.allCorners])
    }
}

#Preview {
    SearchBarTextField(
        text: "",
        onSearchButtonClick: {},
        clearText: {}
    )
}

#Preview {
    SearchBarTextField(
        text: "Brand",
        onSearchButtonClick: {},
        clearText: {}
    )
}
