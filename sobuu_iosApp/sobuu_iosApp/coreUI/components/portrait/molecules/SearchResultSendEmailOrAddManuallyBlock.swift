import SwiftUI
import shared
import UniformTypeIdentifiers

struct SearchResultSendEmailOrAddManuallyBlock: View {
    
    let navigateToAddBookScreen: (Bool) -> Void
    let savedOnClipboard: (Bool) -> Void
    
    let bookRequestEmail = SharedRes.strings().search_main_requestBooksEmail.stringResource()

    var body: some View {
        
        
        VStack {
            SearchWarningTextWithEmail(
                onEmailClick: {
                    UIPasteboard.general.setValue(bookRequestEmail, forPasteboardType: UTType.plainText.identifier)
                    savedOnClipboard(true)
                },
                bookRequestEmail: bookRequestEmail
            )
            SearchISBNButtonBlock(navigateToAddBookScreenManually: { navigateToAddBookScreen(true)
            })
        }
    }
}

#Preview {
    SearchResultSendEmailOrAddManuallyBlock(
        navigateToAddBookScreen: { _ in },
        savedOnClipboard: { _ in }
    )
}
