import SwiftUI
import shared

struct BottomRoundedOutlinedTextField: View {
    var isSecuredField: Bool
    var hintText: String
    var isError: Bool
    @State var text: String
    
    let iconColor: Color = ColorToken.spanishGrey
    let contentColor: Color
    let iconSize: CGFloat = 24
    let radius: CGFloat = 10
    let icon: String
    
    init(isSecuredField: Bool, text: String, hintText: String, isError: Bool) {
        self.isSecuredField = isSecuredField
        self.hintText = hintText
        self.text = text
        self.isError = isError
        
        icon = if(isSecuredField) {
            "lock.fill"
        } else {
            "person.fill"
        }
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundStyle(iconColor)
                .frame(width: iconSize, height: iconSize)
            
            if(isSecuredField) {
                SecureField(
                    hintText,
                    text: $text
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(contentColor)
                .background(ColorToken.whiteBlue)
            } else {
                TextField(
                    hintText,
                    text: $text
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(contentColor)
            }
            
        }
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .background(ColorToken.whiteBlue)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.bottomLeft, .bottomRight])
    }
}

#Preview {
    BottomRoundedOutlinedTextField(
        isSecuredField: false,
        text: "",
        hintText: SharedRes.strings().authorization_auth_password.stringResource(), 
        isError: false
    ).loadCustomFonts()
}

#Preview {
    BottomRoundedOutlinedTextField(
        isSecuredField: false,
        text: "Wrong",
        hintText: SharedRes.strings().authorization_auth_password.stringResource(),
        isError: true
    ).loadCustomFonts()
}
