import SwiftUI

struct SquareOutlinedTextField: View {
    var isSecuredField: Bool
    var hintText: String
    var isError: Bool
    @State var text: String
    
    let iconColor: Color = ColorToken.spanishGrey
    let contentColor: Color
    let iconSize: CGFloat = 24
    let radius: CGFloat = 0
    let icon: String
    
    init(isSecuredField: Bool, text: String, hintText: String, icon: String, isError: Bool) {
        self.isSecuredField = isSecuredField
        self.hintText = hintText
        self.text = text
        self.isError = isError
        self.icon = icon
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundStyle(iconColor)
                .frame(width: iconSize, height: iconSize)
            
            if(isSecuredField) {
                SecureField(
                    hintText,
                    text: $text
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(contentColor)
                .background(ColorToken.whiteBlue)
            } else {
                TextField(
                    hintText,
                    text: $text
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(contentColor)
            }
        }
        .frame(width: .infinity, alignment: .leading)
        .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
        .background(ColorToken.whiteBlue)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.allCorners])
        
    }
}

#Preview {
    SquareOutlinedTextField(
        isSecuredField: true,
        text: "",
        hintText: "Email",
        icon: "mail",
        isError: false
    ).loadCustomFonts()
}

#Preview {
    SquareOutlinedTextField(
        isSecuredField: false,
        text: "Wrong",
        hintText: "Email",
        icon: "lock.fill",
        isError: true
    ).loadCustomFonts()
}
