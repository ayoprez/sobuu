import SwiftUI

struct NoBorderLongTextInputField: View {
    @State var text: String
    var hintText: String
    var isError: Bool
    
    let radius: CGFloat = 10
    
    let contentColor: Color
    
    init(text: String, hintText: String, isError: Bool) {
        self.hintText = hintText
        self.isError = isError
        
        if(text.isEmpty) {
            self.text = hintText
        } else {
            self.text = text
        }
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        TextEditor(
            text: $text
        )
        .font(CustomFont.bodyLarge)
        .foregroundColor(self.text == hintText ? ColorToken.spanishGrey : contentColor)
        .lineLimit(nil)
        .multilineTextAlignment(.leading)
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .colorMultiply(ColorToken.whiteBlue)
        .onTapGesture {
            if self.text == hintText {
                self.text = ""
            }
        }
    }
}

#Preview {
    NoBorderLongTextInputField(
        text: "",
        hintText: "Write your comment",
        isError: false
    ).loadCustomFonts()
}

#Preview {
    NoBorderLongTextInputField(
        text: "I think that this should be a longer text, because I'm trying to check if this field really goes into more than...",
        hintText: "Write your comment",
        isError: true
    ).loadCustomFonts()
}

