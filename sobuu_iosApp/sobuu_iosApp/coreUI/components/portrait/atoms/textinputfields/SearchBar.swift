import SwiftUI
import shared

struct SearchBar: View {
    @FocusState private var focusedField: Bool
    
    @State var searchFieldValue: String
    let onSearchFieldValueChange: (String) -> Void
    let onSearchButtonClick: () -> Void
    let clearText: () -> Void
    let onSearchFieldFocusChange: (Bool) -> Void
    
    let hintText = SharedRes.strings().home_main_searchBook.stringResource()
    
    let textColor = ColorToken.darkLava
    let hintColor = ColorToken.spanishGrey
    let background = ColorToken.whiteBlue
    let errorColor = ColorToken.vermilion
    
    let textFont = CustomFont.bodyMedium
    
    var body: some View {
        HStack {
            IconToken.searchIcon
                .padding(10)
            
            TextField(hintText, text: $searchFieldValue)
                .padding(.vertical, 10)
                .font(textFont)
                .foregroundStyle(textColor)
                .focused($focusedField)
            
            if(focusedField) {
                IconToken.cancelIcon
                    .padding(10)
            }
        }
        .background(RoundedRectangle(cornerRadius: 5).stroke(textColor, lineWidth: 1))
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 10)
    }
}

#Preview {
    SearchBar(
        searchFieldValue: "",
        onSearchFieldValueChange: { _ in },
        onSearchButtonClick: {},
        clearText: {},
        onSearchFieldFocusChange: {_ in }
    ).loadCustomFonts()
}
