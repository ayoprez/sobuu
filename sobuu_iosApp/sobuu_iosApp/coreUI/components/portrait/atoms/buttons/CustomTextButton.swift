import SwiftUI

struct CustomTextButton: View {
    let text: String
    let color: Color
    let action: () -> Void
    
    init(
        text: String,
        color: Color,
        action: @escaping () -> Void
    ) {
        self.text = text
        self.color = color
        self.action = action
    }
    
    var body: some View {
        Button(action: action) {
            Text(text)
                .font(CustomFont.bodySmall)
                .foregroundColor(color)
        }
    }
}

#Preview {
    BaseContentView {
        CustomTextButton(
            text: "Privacy", color: ColorToken.whiteBlue, action: {}
        )
        .loadCustomFonts()
    }
}
