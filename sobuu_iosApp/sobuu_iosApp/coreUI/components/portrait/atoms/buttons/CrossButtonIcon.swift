import SwiftUI

struct CrossButtonIcon: View {
    
    let iconColor = ColorToken.whiteBlue
    let onButtonClick: () -> Void
    
    var body: some View {
        Button(action: onButtonClick) {
            Image(systemName: "xmark.circle")
                .resizable()
                .foregroundColor(iconColor)
                .frame(width: 24, height: 24)
        }
        .padding(12)
    }
}

#Preview {
    CrossButtonIcon(onButtonClick: {})
}
