import SwiftUI
import shared

struct CreateAccountButton: View {
    var didClickButton: () -> Void
    var isEnabled: Bool
    
    let textColor: Color = ColorToken.whiteBlue
    let buttonColor: Color
    
    
    init(didClickButton: @escaping () -> Void, isEnabled: Bool) {
        self.didClickButton = didClickButton
        self.isEnabled = isEnabled
        
        buttonColor = isEnabled ? ColorToken.vermilion : ColorToken.spanishGrey
    }
    
    var body: some View {
        Button(action: didClickButton) {
                Label(
                    title: { Text(SharedRes.strings().authorization_auth_createAccount.stringResource()) },
                    icon: {}
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(textColor)
                .frame(maxWidth: .infinity)
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            }
        .buttonStyle(.plain)
        .disabled(!isEnabled)
        .frame(maxWidth: .infinity)
        .background(buttonColor)
        .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
    }
}

#Preview {
    CreateAccountButton(didClickButton: {}, isEnabled: true)
}

#Preview {
    CreateAccountButton(didClickButton: {}, isEnabled: false)
}
