import SwiftUI

struct ProfileIconButton: View {
    
    let navigateToProfileScreen: () -> Void
    
    let profileIconColor = ColorToken.spanishGrey
    let iconSize: CGFloat = 42
    
    var body: some View {
        IconToken.profile
            .foregroundStyle(profileIconColor)
            .font(.system(size: iconSize))
            .frame(maxWidth: .infinity)
            .onTapGesture(perform: navigateToProfileScreen)
    }
}

#Preview {
    ProfileIconButton(
        navigateToProfileScreen: {}
    )
}
