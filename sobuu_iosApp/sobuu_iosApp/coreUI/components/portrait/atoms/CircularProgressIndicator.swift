import SwiftUI

struct CircularProgressIndicator: View {
    let progress: String
    
    
    let borderColor = ColorToken.vermilion.opacity(0.7)
    let contentColor = ColorToken.greenSheen.opacity(0.7)
    let textColor = ColorToken.whiteBlue
    
    let indicatorSize: CGFloat = 70
    let contentSize: CGFloat = 55
    
    init(progress: Double) {
        self.progress = progress.removeZerosFromEnd()
    }
    
    var body: some View {
        ZStack {
            Circle()
                .fill(borderColor)
                .frame(width: indicatorSize, height: indicatorSize)
            Circle()
                .fill(contentColor)
                .frame(width: contentSize, height: contentSize)
            Text("\(progress)%")
                .foregroundColor(textColor)
                .font(CustomFont.bodyMedium)
        }
    }
}

#Preview {
    CircularProgressIndicator(progress: 96)
        .loadCustomFonts()
}
