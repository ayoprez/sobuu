import SwiftUI
import shared

struct AcceptanceLegalTextWithLinks: View {
    let termsAction: () -> Void
    let privacyAction: () -> Void
    
    let linkColor: Color = ColorToken.vermilion
    let textColor:Color = ColorToken.whiteBlue
    
    let textPart1: String = SharedRes.strings().authorization_auth_privacyConfirmation1.stringResource()
    let textPart2: String = SharedRes.strings().settings_main_terms.stringResource()
    let textPart3: String = SharedRes.strings().authorization_auth_privacyConfirmation2.stringResource()
    let textPart4: String = SharedRes.strings().settings_main_privacy.stringResource()
    
    init(
        termsAction: @escaping () -> Void,
        privacyAction: @escaping () -> Void
    ) {
        self.termsAction = termsAction
        self.privacyAction = privacyAction
    }
    
    var body: some View {
        HStack(spacing: 2) {
            Text(textPart1)
                .font(CustomFont.bodySmall)
                .foregroundColor(textColor)
            CustomTextButton(text: textPart2, color: linkColor, action: termsAction)
            Text(textPart3)
                .font(CustomFont.bodySmall)
                .foregroundColor(textColor)
            CustomTextButton(text: textPart4, color: linkColor, action: termsAction)
        }
    }
}

#Preview {
    AcceptanceLegalTextWithLinks(
        termsAction: {},
        privacyAction: {}
    )
}
