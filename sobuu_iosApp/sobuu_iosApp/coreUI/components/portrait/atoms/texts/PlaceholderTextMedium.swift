import SwiftUI

struct PlaceholderTextMedium: View {
    var placeholderText: String
    var isError: Bool
    
    var contentColor: Color
    
    init(placeholderText: String, isError: Bool) {
        self.placeholderText = placeholderText
        self.isError = isError
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.spanishGrey
        }
    }
    
    var body: some View {
        Text(placeholderText)
            .font(CustomFont.bodyMedium)
            .foregroundColor(contentColor)
    }
}

#Preview {
    PlaceholderTextMedium(placeholderText: "Email", isError: false)
        .loadCustomFonts()
}
