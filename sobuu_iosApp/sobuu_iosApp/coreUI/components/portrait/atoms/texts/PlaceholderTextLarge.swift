import SwiftUI

struct PlaceholderTextLarge: View {
    var placeholderText: String
    var isError: Bool
    
    var contentColor: Color
    
    init(placeholderText: String, isError: Bool) {
        self.placeholderText = placeholderText
        self.isError = isError
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.spanishGrey
        }
    }
    
    var body: some View {
        Text(placeholderText)
            .font(CustomFont.bodyLarge)
            .foregroundColor(contentColor)
    }
}

#Preview {
    PlaceholderTextLarge(placeholderText: "Email", isError: false)
        .loadCustomFonts()
}
