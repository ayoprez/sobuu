import SwiftUI
import shared

struct IconAndText<Content: View>: View {
    
    let text: String
    let icon: Content
    
    let iconSize:CGFloat = 12
    let spacing: CGFloat = 2
    
    var body: some View {
        HStack(spacing: spacing) {
            icon
                .font(.system(size: iconSize))
            Text(text)
                .padding(spacing)
                .lineLimit(1)
        }
    }
}

#Preview {
    IconAndText(
        text: "Person",
        icon: IconToken.person
    ).loadCustomFonts()
}
