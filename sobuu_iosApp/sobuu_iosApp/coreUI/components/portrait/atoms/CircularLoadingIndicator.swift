import SwiftUI

struct CircularLoadingIndicator: View {
    var body: some View {
        ProgressView()
            .scaleEffect(2)
            .progressViewStyle(CircularProgressViewStyle(tint: ColorToken.greenSheen))
            .edgesIgnoringSafeArea(.all)
            .background(ColorToken.whiteBlue)
    }
}

#Preview {
    CircularLoadingIndicator()
}
