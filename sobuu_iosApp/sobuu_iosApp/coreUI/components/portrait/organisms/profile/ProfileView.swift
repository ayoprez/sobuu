import SwiftUI

struct ProfileView: View {
    let isLoading: Bool
    let hasError: Bool
    let errorText: String?
    let userFirstname: String?
    let userLastname: String?
    let username: String?
    
    let errorTextColor = ColorToken.vermilion
    
    var body: some View {
        VStack {
            ProfileCard(
                firstName: userFirstname,
                lastName: userLastname,
                username: username
            )
            
            if (isLoading) {
                ProfileLoading()
            }
            
            if (hasError) {
                Text(errorText ?? "")
                    .font(CustomFont.bodyMedium)
                    .foregroundStyle(errorTextColor)
            }
        }
    }
}

#Preview {
    ProfileView(
        isLoading: false,
        hasError: false,
        errorText: "",
        userFirstname: "John",
        userLastname: "Connor",
        username: "JoCo"
    )
}
