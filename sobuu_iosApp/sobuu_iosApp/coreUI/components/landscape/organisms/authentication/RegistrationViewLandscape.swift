import SwiftUI
import shared

struct RegistrationViewLandscape: View {
    @State var username: String
    @State var email: String
    @State var password: String
    @State var passwordConfirmation: String
    @State var firstName: String
    @State var lastName: String
    @State var isUserError: Bool
    @State var isEmailError: Bool
    @State var isPasswordError: Bool
    @State var isPassConfirmError: Bool
    @State var errorText: String?
    @State var isLoading: Bool
    let onLoginClick: () -> Void
    let onForgotPasswordClick: () -> Void
    let onRegisterClick: () -> Void
    let onTermsClick: () -> Void
    let onPrivacyClick: () -> Void
    
    var body: some View {
        if isLoading {
            CircularLoadingIndicator()
        } else {
            ZStack {
                ColorToken.greenSheen.ignoresSafeArea()
                
                HStack {
                    VStack(spacing: 0) {
                        Spacer()
                        
                        TopRoundedOutlinedTextField(
                            text: username,
                            hintText: SharedRes.strings().authorization_auth_username.stringResource(),
                            icon: "person.fill",
                            isError: isUserError
                        )
                        SquareOutlinedTextField(
                            isSecuredField: false,
                            text: email,
                            hintText: SharedRes.strings().authorization_auth_email.stringResource(),
                            icon: "envelope.fill",
                            isError: isEmailError
                        )
                        SquareOutlinedTextField(
                            isSecuredField: true,
                            text: password,
                            hintText: SharedRes.strings().authorization_auth_password.stringResource(),
                            icon: "lock.fill",
                            isError: isPasswordError
                        )
                        BottomRoundedOutlinedTextField(
                            isSecuredField: true,
                            text: passwordConfirmation,
                            hintText: SharedRes.strings().authorization_auth_confirmPassword.stringResource(),
                            isError: isPassConfirmError
                        )
                        
                        Spacer()
                        
                        LegalButtons(
                            onTermsAndConditionsButtonClick: onTermsClick,
                            onPrivacyPolicyButtonClick: onPrivacyClick
                        )
                        
                        Spacer()
                    }
                    
                    VStack(spacing: 0) {
                        Spacer()
                        Spacer()
                        
                        TopRoundedOutlinedTextField(
                            text: firstName,
                            hintText: SharedRes.strings().authorization_auth_firstNameAccount.stringResource(),
                            icon: "person.fill",
                            isError: false
                        )
                        BottomRoundedOutlinedTextField(
                            isSecuredField: false,
                            text: lastName,
                            hintText: SharedRes.strings().authorization_auth_lastNameAccount.stringResource(),
                            isError: false
                        )
                        
                        LegalTextAcceptanceSwitch(
                            termsAction: {},
                            privacyAction: {}
                        )
                        .padding(.top, 30)
                        
                        
                        Spacer()
                        
                        if errorText != nil {
                            Text(errorText!)
                                .padding(EdgeInsets(top: 15, leading: 25, bottom: 0, trailing: 25))
                                .foregroundColor(ColorToken.vermilion)
                        }
                        Spacer()
                        
                        CreateAccountButton(didClickButton: onRegisterClick, isEnabled: true)
                            .padding(.horizontal, 5)
                        
                        
                        Spacer()
                        
                    }
                    .padding(.horizontal, 20)
                }
            }
        }
    }
}

#Preview {
    RegistrationViewLandscape(
        username: "",
        email: "",
        password: "",
        passwordConfirmation: "",
        firstName: "",
        lastName: "",
        isUserError: false,
        isEmailError: false,
        isPasswordError: false,
        isPassConfirmError: false,
        errorText: nil,
        isLoading: false,
        onLoginClick: {},
        onForgotPasswordClick: {},
        onRegisterClick: {},
        onTermsClick: {},
        onPrivacyClick: {}
    ).loadCustomFonts()
}
