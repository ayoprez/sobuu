import Foundation
import shared

public extension Date? {
    
    func toStringDateWithDayAndTime() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy, HH:mm"
        
        if(self == nil) {
            return dateFormatter.string(from: Date.now)
        }
        
        if (Calendar.current.isDateInToday(self!)) {
            return SharedRes.strings().general_today.stringResource()
        }
        
        return dateFormatter.string(from: self!)
        
        /*
        if(this.date == Clock.System.now().toLocalDateTime(TimeZone.UTC).date) {
            return general_today.stringResource(context = LocalContext.current)
        }

        val formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm")
        return this.toJavaLocalDateTime().format(formatter)
        */
    }
}
