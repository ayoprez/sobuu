//
//  RegistrationViewModel.swift
//  sobuu_iosApp
//
//  Created by Ayo on 25.02.24.
//  Copyright © 2024 orgName. All rights reserved.
//
/*
import Foundation
import shared

class RegistrationViewModel: ObservableObject {
    
    @Published var showLoading = false
    @Published var errorText: String? = nil
    
    private var authRepo: IAuthenticationRepository
    
    init() {
        authRepo = KoinHelper(context: NSObject()).authRepo
    }
        
    func registerUser () {
        showLoading = true
        
        
    }
    
    private func handleErrors(error: AuthenticationError?) -> String {
        guard let resultError = error else {
            return SharedRes.strings().authorization_errors_unknown.stringResource()
        }
        
        switch resultError {
        case .EmailAlreadyTaken():
            return SharedRes.strings().authorization_errors_emailAlreadyUsed.stringResource()
        case .EmptyCredentialsError():
            return SharedRes.strings().authorization_errors_emptyCredentials.stringResource()
        case .InvalidCredentials():
            return SharedRes.strings().authorization_errors_invalidCredentials.stringResource()
        case .InvalidSessionToken():
            return SharedRes.strings().authorization_errors_invalidSessionToken.stringResource()
        case .TimeOutError():
            return SharedRes.strings().authorization_errors_timeout.stringResource()
        case .ResetPassword():
            return SharedRes.strings().authorization_errors_resettingPassword.stringResource()
        case .PasswordTooShort():
            return SharedRes.strings().authorization_errors_shortPassword.stringResource()
        case .InvalidEmailError():
            return SharedRes.strings().authorization_errors_wrongEmailFormat.stringResource()
        case .UsernameAlreadyTaken():
            return SharedRes.strings().authorization_errors_usernameAlreadyTaken.stringResource()
        case .WrongEmailFormatError():
            return SharedRes.strings().authorization_errors_wrongEmailFormat.stringResource()
        case .RegistrationError():
            return SharedRes.strings().authorization_errors_registration.stringResource()
        default:
            return SharedRes.strings().authorization_errors_unknown.stringResource()
        }
    }
}
*/
