package com.sobuumedia.sobuu.authentication.reset_pass

import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_resetPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyResetPassEmail
import com.sobuumedia.sobuu.android.authentication.reset_pass.ResetPassViewModel
import com.sobuumedia.sobuu.android.authentication.reset_pass.ResetPasswordScreen
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import com.sobuumedia.sobuu.withContentDescription
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule


@RunWith(AndroidJUnit4::class)
class ResetPasswordScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<IAuthenticationRepository>()

    /**
     * TODO
     * - Take screenshots in other languages
     * - Test visibility of elements in tablet landscape
     */
    @Before
    fun setUp() {
        val mockViewModel = ResetPassViewModel(mockRepo)

        composeTestRule.setContent {
            SobuuAuthTheme {
                ResetPasswordScreen(
                    navigateToSentEmailScreen = {},
                    navigateBack = {},
                    viewModel = mockViewModel
                )
            }
        }
        Thread.sleep(120)
    }

    @Test
    fun takeScreenshot() {
        Screengrab.screenshot("reset_password")
    }

    @Test
    fun testEmailDisplayed() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmailDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).isDisplayed()
    }

    @Test
    fun testSendButtonDisplayed() {
        composeTestRule.onNode(authorization_auth_resetPassword.withContentDescription()).isDisplayed()
    }

    @Test
    fun testSendButtonDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_resetPassword.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmptyFieldError() {
        coEvery {mockRepo.resetPassword(any()) } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)

        composeTestRule.onNode(authorization_auth_resetPassword.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_emptyResetPassEmail.withContentDescription()).isDisplayed()
    }

    @Test
    fun testWrongEmailFormatError() {
        coEvery { mockRepo.resetPassword(any()) } returns AuthenticationResult.Error(
            AuthenticationError.WrongEmailFormatError)

        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performTextInput("testemail")

        Espresso.closeSoftKeyboard()

        composeTestRule.onNode(authorization_auth_resetPassword.withContentDescription()).performClick()

        composeTestRule.onNode(authorization_errors_emptyResetPassEmail.withContentDescription()).isDisplayed()
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }

        @ClassRule
        @JvmField
        val localeTestRule = LocaleTestRule()
    }
}