package com.sobuumedia.sobuu.authentication.login

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.android.onboarding.OnBoardingFeaturesScreen
import com.sobuumedia.sobuu.android.settings.settings.SettingsViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.AnalyticsInterface
import com.sobuumedia.sobuu.android.utils.CrashReportInterface
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.lang.Thread.sleep
import kotlin.test.Test

@RunWith(AndroidJUnit4::class)
internal class OnBoardingFeaturesScreenTests {

    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<ISettingsRepository>()
    private val mockCrashReport = mockk<CrashReportInterface>()
    private val mockAnalytics = mockk<AnalyticsInterface>()


    @Before
    fun setUp() {
        coEvery { mockRepo.setDisplayedConsentScreen(any()) } returns Unit
        coEvery { mockRepo.displayedFeatureScreen() } returns true
        coEvery { mockRepo.getAnalyticsEnabled() } returns false
        coEvery { mockRepo.getCrashReportsEnabled() } returns false
        val mockViewModel = SettingsViewModel(mockRepo, mockCrashReport, mockAnalytics)

        composeTestRule.setContent {
            SobuuAuthTheme {
                OnBoardingFeaturesScreen(
                    navigateToNext = {},
                    viewModel = mockViewModel
                )
            }
        }
        sleep(1200)
    }

    @Test
    fun takeScreenshot() {
        Screengrab.screenshot("Features")

        sleep(1200)
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}