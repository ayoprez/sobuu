//package com.sobuumedia.sobuu.comment
//
//import androidx.compose.ui.test.junit4.createComposeRule
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import com.sobuumedia.sobuu.android.comments.CommentsScreen
//import com.sobuumedia.sobuu.android.comments.CommentsViewModel
//import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
//import com.sobuumedia.sobuu.core.NapierLogs
//import com.sobuumedia.sobuu.fake_data.comment1
//import com.sobuumedia.sobuu.fake_data.comment2
//import com.sobuumedia.sobuu.fake_data.comment4
//import com.sobuumedia.sobuu.fake_data.comment5
//import com.sobuumedia.sobuu.features.book.repository.IBookRepository
//import com.sobuumedia.sobuu.features.comments.repository.ICommentRepository
//import io.mockk.mockk
//import org.junit.AfterClass
//import org.junit.Before
//import org.junit.BeforeClass
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//import tools.fastlane.screengrab.Screengrab
//import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
//import tools.fastlane.screengrab.locale.LocaleTestRule
//
//@RunWith(AndroidJUnit4::class)
//class CommentScreenInstrumentationTest {
//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    /**
//     * Used to translate the screen for the Fastlane screenshots
//     */
//    @get:Rule
//    val localeTestRule = LocaleTestRule()
//
//    private val mockBookRepo = mockk<IBookRepository>()
//    private val mockCommentRepo = mockk<ICommentRepository>()
//    private lateinit var mockCommentViewModel: CommentsViewModel
//
//    /**
//     *
//     */
//    @Before
//    fun setUp() {
//        mockCommentViewModel = CommentsViewModel(
//            commentsRepo = mockCommentRepo,
//            booksRepo = mockBookRepo,
//            logs = NapierLogs()
//        )
//
//        mockCommentViewModel.state = mockCommentViewModel.state.copy(
//            listOFComments = listOf(comment1, comment2, comment4, comment5),
//        )
//
//        composeTestRule.setContent {
//            SobuuTheme {
//                CommentsScreen(
//                    viewModel = mockCommentViewModel,
//                    navigateBackOrHome = {},
//                    bookId = "123",
//                    navigateToAddNewCommentScreen = { _, _ -> },
//                    logs = NapierLogs()
//                )
//            }
//        }
//
//        Thread.sleep(120)
//    }
//
//    @Test
//    fun takeScreenshotComment() {
//        Thread.sleep(500)
//        Screengrab.screenshot("comments")
//    }
//
//    companion object {
//        @JvmStatic
//        @BeforeClass
//        fun beforeAll() {
//            CleanStatusBar.enableWithDefaults()
//        }
//
//        @JvmStatic
//        @AfterClass
//        fun afterAll() {
//            CleanStatusBar.disable()
//        }
//    }
//}