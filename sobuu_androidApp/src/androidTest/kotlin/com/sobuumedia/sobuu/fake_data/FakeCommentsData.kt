package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.Comment
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime


val comment1 = Comment(
    id = "135",
    user = profile1,
    percentage = 47.4,
    parentCommentId = null,
    publishedDate = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
    pageNumber = 150,
    hasSpoilers = false,
    votesCounter = 0,
    text = "Test comment"
)

val comment2 = Comment(
    id = "246",
    user = profile1,
    percentage = 0.0,
    parentCommentId = null,
    publishedDate = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
    pageNumber = 0,
    hasSpoilers = false,
    votesCounter = 0,
    text = "This will be awesome"
)

val comment3 = Comment(
    id = "357",
    user = profile1,
    percentage = 50.5,
    parentCommentId = null,
    publishedDate = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
    pageNumber = 165,
    hasSpoilers = true,
    votesCounter = 0,
    text = "OMG"
)

val comment4 = Comment(
    id = "359",
    user = profile2,
    percentage = 50.5,
    parentCommentId = null,
    publishedDate = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
    pageNumber = 165,
    hasSpoilers = true,
    votesCounter = 0,
    text = "OMG"
)

val comment5 = Comment(
    id = "359",
    user = profile2,
    percentage = 50.5,
    parentCommentId = null,
    publishedDate = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
    pageNumber = 165,
    hasSpoilers = true,
    votesCounter = 0,
    text = "OMG"
)
