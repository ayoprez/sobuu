package com.sobuumedia.sobuu

import androidx.compose.ui.test.SemanticsMatcher
import androidx.compose.ui.test.hasContentDescription
import com.sobuumedia.sobuu.android.utils.stringResource
import dev.icerock.moko.resources.StringResource

fun String.withContentDescription(): SemanticsMatcher {
    return hasContentDescription(this)
}

fun StringResource.withContentDescription(): SemanticsMatcher {
    return hasContentDescription(this.stringResource())
}