package com.sobuumedia.sobuu.book

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingScreen
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.CrashReportInterface
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.bookProgressReadingAlmostFinished
import com.sobuumedia.sobuu.fake_data.comment4
import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.lang.Thread.sleep
import kotlin.test.Test


@RunWith(AndroidJUnit4::class)
internal class CurrentlyReadingBookScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<IBookRepository>()
    private val mockCrashReport = mockk<CrashReportInterface>()
    private val mockSobuuLogs = mockk<SobuuLogs>()


    @Before
    fun setUp() {
        val bookProgress = BookWithProgress(
            book = book1,
            bookProgress = bookProgressReadingAlmostFinished
        )

        coEvery { mockSobuuLogs.info(any(), any()) } returns Unit
        coEvery { mockRepo.getBookWithProgressByIdFromDB(any()) } returns BookResult.Success(
            bookProgress
        )
        coEvery { mockRepo.getCommentsInPercentage(any(), any()) } returns BookResult.Success(2)
        val mockViewModel = CurrentlyReadingViewModel(mockRepo, mockCrashReport)

        composeTestRule.setContent {
            SobuuAuthTheme {
                CurrentlyReadingScreen(
                    navigateToHomeScreen = {},
                    navigateToCommentsScreen = {},
                    navigateBack = {},
                    bookId = "",
                    viewModel = mockViewModel,
                    navigateToBookScreen = { _, _ -> },
                    logs = mockSobuuLogs
                )
            }
        }
        sleep(1200)
    }

    @Test
    fun takeScreenshot() {
        Screengrab.screenshot("Currently_Reading")

        sleep(1200)
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}