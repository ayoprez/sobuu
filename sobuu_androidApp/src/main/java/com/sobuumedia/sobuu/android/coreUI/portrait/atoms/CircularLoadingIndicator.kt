package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_login_loading
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun CircularLoadingIndicator() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(modifier = Modifier.semantics {
            contentDescription = contentDescription_login_loading.stringResource()
        })
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CircularLoadingIndicatorPreview() {
    SobuuTheme {
        CircularLoadingIndicator()
    }
}
