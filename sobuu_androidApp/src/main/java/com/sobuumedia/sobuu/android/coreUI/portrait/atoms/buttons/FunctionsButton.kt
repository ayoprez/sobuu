package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.Context
import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Rule
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_functionsButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.IconAndText
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun FunctionsButton(
    onClick: () -> Unit
) {
    val context: Context = LocalContext.current
    val textColor = MaterialTheme.colorScheme.secondary

    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            onClick()
        }) {
        IconAndText(
            text = settings_about_functionsButton.stringResource(context),
            textStyle = Typography.bodyMedium.copy(color = textColor),
            icon = Icons.AutoMirrored.Filled.Rule
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = false,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun FunctionsButtonPreview() {
    SobuuAuthTheme {
        FunctionsButton(onClick = {})
    }
}