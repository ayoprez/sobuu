package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.search_main_addBookInvitation
import com.sobuumedia.sobuu.SharedRes.strings.search_main_addBookManually
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchISBNButtonBlock(
    modifier: Modifier = Modifier,
    navigateToAddBookScreenManually: () -> Unit
) {
    val context = LocalContext.current
    val buttonTextColor = MaterialTheme.colorScheme.background
    val buttonColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.secondary

    val paddingSize = 12.dp

    Column(
        modifier = Modifier.then(modifier).fillMaxWidth(),
    ) {
        Text(
            text = search_main_addBookInvitation.stringResource(context = context),
            style = Typography.bodyLarge.copy(color = textColor),
            textAlign = TextAlign.Justify
        )
        ElevatedButton(
            onClick = navigateToAddBookScreenManually,
            colors = ButtonDefaults.buttonColors(buttonColor),
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = paddingSize)
        ) {
            Text(
                text = search_main_addBookManually.stringResource(context = context),
                style = Typography.bodyMedium.copy(color = buttonTextColor)
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchISBNButtonBlockPreview() {
    SobuuTheme {
        SearchISBNButtonBlock(
            modifier = Modifier.background(Color.White),
            navigateToAddBookScreenManually = {}
        )
    }
}