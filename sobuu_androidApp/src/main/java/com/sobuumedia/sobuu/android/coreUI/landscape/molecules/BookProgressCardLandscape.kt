package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.sobuumedia.sobuu.SharedRes.strings.home_main_finishedBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_gaveUpBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_startedBookOn
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.LinealProgressIndicator
import com.sobuumedia.sobuu.android.main.toStringDateWithDayAndTime
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun BookProgressCardLandscape(
    modifier: Modifier = Modifier,
    picture: String,
    progress: Double,
    startedToRead: LocalDateTime?,
    finishedToRead: LocalDateTime?,
    title: String,
    authors: List<String>,
    finished: Boolean,
    giveUp: Boolean,
    commentsInPage: Int,
    navigateToCommentsScreen: () -> Unit
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.secondary
    val textColor = MaterialTheme.colorScheme.background

    val startedTime = startedToRead?.toStringDateWithDayAndTime() ?: Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault())
        .toStringDateWithDayAndTime()
    val startedReadText =
        "${home_main_startedBookOn.stringResource(context = context)} $startedTime"

    val finishedTime = finishedToRead?.toStringDateWithDayAndTime() ?: ""
    val finishedReadText = "${
        if (finished) {
            home_main_finishedBookOn.stringResource(context = context)
        } else {
            home_main_gaveUpBookOn.stringResource(context = context)
        }
    } $finishedTime"

    Box(
        modifier = Modifier
            .then(modifier)
            .fillMaxHeight(0.9f)
            .fillMaxWidth(0.65f)
            .border(
                border = BorderStroke(2.dp, color = backgroundColor),
                shape = RoundedCornerShape(20.dp)
            )
            .clickable { navigateToCommentsScreen() }
            .clip(RoundedCornerShape(20.dp))
            .background(backgroundColor)
    ) {

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row {
                AsyncImage(
                    model = picture,
                    placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(20.dp)
                        .fillMaxHeight(0.7f),
                )
                Column(modifier = Modifier.padding(10.dp)) {
                    Text(
                        modifier = Modifier.padding(bottom = 10.dp),
                        text = title,
                        style = Typography.titleLarge.copy(color = textColor),
                        maxLines = 3,
                        overflow = TextOverflow.Ellipsis,
                    )
                    Text(
                        modifier = Modifier.padding(bottom = 10.dp),
                        text = authors.joinToString(", "),
                        style = Typography.bodyLarge.copy(color = textColor),
                    )
                    Spacer(modifier = Modifier.fillMaxHeight(0.2f))
                    Text(
                        text = startedReadText,
                        style = Typography.bodySmall.copy(color = textColor),
                    )
                    if (finishedToRead != null && (finished || giveUp)) {
                        Text(
                            text = finishedReadText,
                            style = Typography.bodySmall.copy(color = textColor),
                        )
                    }
                }
            }
            LinealProgressIndicator(
                progress = progress,
                commentsInPage = commentsInPage
            )
        }
    }
}

@Preview(
    showSystemUi = true,
    device = "spec:width=412dp,height=915dp,dpi=420,isRound=false,chinSize=0dp,orientation=landscape",
    showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO, widthDp = 720, heightDp = 360)
@Composable
fun BookProgressCardPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "The lord of the rings: The return of the king",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        BookProgressCardLandscape(
            picture = book.book.picture,
            progress = book.bookProgress.progressInPercentage,
            startedToRead = book.bookProgress.startedToRead,
            finishedToRead = book.bookProgress.finishedToRead,
            title = book.book.title,
            authors = book.book.authors,
            finished = book.bookProgress.finished,
            giveUp = book.bookProgress.giveUp,
            commentsInPage = 30,
            navigateToCommentsScreen = {}
        )
    }
}