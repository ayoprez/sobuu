package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextMedium
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun NoBorderHalfScreenTextInputField(
    modifier: Modifier = Modifier,
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    contentDescription: String = "",
    isError: Boolean = false,
    takeHalfScreen: Boolean = true
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val textStyle = Typography.bodyLarge.copy(color = elementColor)

    val fraction = if (takeHalfScreen) 0.5f else 1f

    TextField(
        value = fieldValue,
        onValueChange = onFieldValueChange,
        modifier = Modifier
            .fillMaxWidth(fraction)
            .background(color = backgroundColor)
            .semantics { this.contentDescription = contentDescription }
            .then(modifier),
        textStyle = textStyle,
        colors = OutlinedTextFieldDefaults.colors(
            focusedBorderColor = elementColor,
            unfocusedBorderColor = elementColor,
            errorBorderColor = errorColor,
        ),
        isError = isError,
        placeholder = {
            PlaceholderTextMedium(
                placeholderText = placeholderText,
                isError = isError
            )
        },
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Default,
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Words
        ),
        singleLine = true,
    )
}

@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun NoBorderHalfScreenTextInputFieldPreview() {
    NoBorderHalfScreenTextInputField(
        fieldValue = "",
        onFieldValueChange = ({}),
        placeholderText = "First name",
    )
}