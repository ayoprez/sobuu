package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.res.Configuration
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Info
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_progress_bookInfo
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun BookInfoButton(
    modifier: Modifier = Modifier,
    navigateToBookScreen: () -> Unit
) {
    val profileIconColor = MaterialTheme.colorScheme.primary
    val iconSize = 54.dp

    IconButton(
        modifier = Modifier
            .width(iconSize)
            .height(iconSize)
            .semantics {
                this.contentDescription = contentDescription_progress_bookInfo.stringResource()
            }
            .then(modifier),
        onClick = {
            navigateToBookScreen()
        }
    ) {
        Icon(
            modifier = Modifier
                .width(iconSize)
                .height(iconSize),
            imageVector = Icons.Sharp.Info,
            contentDescription = Icons.Sharp.Info.name,
            tint = profileIconColor,
        )
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BookInfoButtonPreview() {
    SobuuTheme {
        BookInfoButton(
            navigateToBookScreen = {}
        )
    }
}