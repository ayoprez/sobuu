package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Comment
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CommentsInPageIndicator(
    modifier: Modifier = Modifier,
    commentsInPage: String
) {

    val backgroundColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.secondary
    val iconColor = MaterialTheme.colorScheme.secondary
    val iconHeight = 20.dp
    val iconWidth = 50.dp

    Row {
        Box(
            modifier = Modifier
                .width(iconWidth)
                .height(iconHeight)
                .clip(RoundedCornerShape(40.dp, 40.dp, 0.dp, 0.dp))
                .background(backgroundColor)
                .then(modifier),
            contentAlignment = Alignment.Center
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Icon(
                    modifier = Modifier.width(12.dp),
                    imageVector = Icons.AutoMirrored.Default.Comment,
                    contentDescription = Icons.AutoMirrored.Default.Comment.name,
                    tint = iconColor
                )
                Spacer(modifier = Modifier.padding(3.dp))
                Text(
                    text = commentsInPage.trim(),
                    style = Typography.bodySmall.copy(color = textColor)
                )
            }
        }

        Spacer(modifier = Modifier.padding(end = 9.dp))
    }
}

@Preview(
    showSystemUi = false,
    showBackground = false,
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun CurrentlyReadingViewDEPreview() {
    SobuuTheme {
        CommentsInPageIndicator(commentsInPage = "300")
    }
}

