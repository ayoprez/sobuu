package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.android.custom_widgets.IconAndText
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun EmailButton(text: String) {
    val context: Context = LocalContext.current

    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            val intent = Intent(Intent.ACTION_VIEW)
            val data = Uri.parse("mailto:$text")
            intent.data = data
            startActivity(context, intent, null)
        }) {
        IconAndText(
            text = text,
            fontSize = 14.sp,
            customIcon = {
                Image(
                    modifier = Modifier.size(24.dp, 24.dp),
                    imageVector = Icons.Filled.Mail,
                    contentDescription = authorization_auth_email.stringResource(context = context),
                    colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.background)
                )
            }
        )
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun EmailButtonPreview() {
    SobuuAuthTheme {
        EmailButton(text = "testemail@testdomain.com")
    }
}