package com.sobuumedia.sobuu.android.main

sealed class SearchUIEvent {
    data class SearchTermChanged(val value: String, val lang: String): SearchUIEvent()
    object cleanSearchTerm: SearchUIEvent()
    object searchTerm: SearchUIEvent()
    object removeErrorState: SearchUIEvent()
    object clickOnMoreInfo: SearchUIEvent()
}