package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.onboarding

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.changelog_1_2_0
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_newVersion_button
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_newVersion_title
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RoundPrimaryColorButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun OnBoardingNewVersionViewLandscape(
    modifier: Modifier = Modifier,
    navigateToNext: () -> Unit,
    currentVersion: String
) {
    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(horizontal = 20.dp)
            .then(modifier),
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 22.dp),
            text = onBoarding_newVersion_title.stringResource(context = context, currentVersion),
            style = Typography.titleMedium.copy(textColor),
            textAlign = TextAlign.Center
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.75f)
                .verticalScroll(rememberScrollState()),
            text = changelog_1_2_0.stringResource(context = context),
            style = Typography.bodyMedium.copy(color = textColor)
        )

        RoundPrimaryColorButton(
            modifier = Modifier.padding(start = 150.dp, end = 150.dp, bottom = 20.dp),
            onClick = navigateToNext,
            text = onBoarding_newVersion_button.stringResource(context = context),
            isEnabled = true
        )
    }
}

@Preview(
    showBackground = true,
    locale = "en",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun OnBoardingNewVersionLandscapeDialogPreview() {
    SobuuAuthTheme {
        OnBoardingNewVersionViewLandscape(
            navigateToNext = {},
            currentVersion = "2.0.0"
        )
    }
}

@Preview(
    showBackground = true,
    device = "id:pixel_2",
    locale = "de",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun OnBoardingNewVersionLandscapeDialogSmallPreview() {
    SobuuAuthTheme {
        OnBoardingNewVersionViewLandscape(
            navigateToNext = {},
            currentVersion = "2.0.0"
        )
    }
}