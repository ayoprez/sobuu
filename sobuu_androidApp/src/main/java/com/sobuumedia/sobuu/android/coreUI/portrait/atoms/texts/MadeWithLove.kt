package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_inGermany
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_love
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_madeWithLove
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.HeartShape
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun MadeWithLove() {
    val context = LocalContext.current
    val brush = Brush.verticalGradient(listOf(Color.Black, Color.Red, Color.Yellow))
    val textColor = MaterialTheme.colorScheme.secondary

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {

        Text(
            modifier = Modifier.padding(5.dp),
            text = settings_about_madeWithLove.stringResource(context = context),
            style = Typography.bodyLarge.copy(color = textColor)
        )
        Icon(
            modifier = Modifier
                .padding(top = 5.dp, bottom = 5.dp)
                .clip(HeartShape())
                .graphicsLayer(alpha = 0.99f)
                .drawWithCache {
                    onDrawWithContent {
                        drawContent()
                        drawRect(brush, blendMode = BlendMode.Difference)
                    }
                },
            imageVector = Icons.Filled.Favorite,
            contentDescription = settings_about_love.stringResource(context = context),
            tint = Color.Transparent
        )
        Text(
            modifier = Modifier.padding(5.dp),
            text = settings_about_inGermany.stringResource(context = context),
            style = Typography.bodyLarge.copy(color = textColor)
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun MadeWithLovePreview() {
    SobuuAuthTheme {
        Box(modifier = Modifier.background(MaterialTheme.colorScheme.background)) {
            MadeWithLove()
        }
    }
}