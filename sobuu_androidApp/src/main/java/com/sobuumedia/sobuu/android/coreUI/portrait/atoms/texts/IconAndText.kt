package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddAlarm
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun IconAndText(
    modifier: Modifier = Modifier,
    text: String,
    textMaxLines: Int = 1,
    textStyle: TextStyle,
    icon: ImageVector? = null,
    iconPainter: Painter? = null,
    customIcon: @Composable () -> Unit = {},
    iconColor: Color = MaterialTheme.colorScheme.secondary,
) {
    val iconSize = 16.dp
    val spacing = 5.dp

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        if (icon == null && iconPainter != null) {
            Icon(
                painter = iconPainter,
                modifier = Modifier.size(iconSize),
                contentDescription = "",
                tint = iconColor,
            )
        } else if (icon != null && iconPainter == null) {
            Icon(
                imageVector = icon,
                modifier = Modifier.size(iconSize),
                contentDescription = icon.name,
                tint = iconColor,
            )
        } else {
            customIcon()
        }

        Text(
            modifier = Modifier.padding(start = spacing),
            text = text,
            style = textStyle,
            maxLines = textMaxLines
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun IconAndTextPreview() {
    SobuuTheme {
        IconAndText(
            modifier = Modifier.background(Color.White),
            text = "Test",
            textMaxLines = 1,
            textStyle = Typography.bodyMedium.copy(color = MaterialTheme.colorScheme.secondary),
            icon = Icons.Filled.AddAlarm,
            iconPainter = null,
            customIcon = {},
            iconColor = MaterialTheme.colorScheme.secondary,
        )
    }
}