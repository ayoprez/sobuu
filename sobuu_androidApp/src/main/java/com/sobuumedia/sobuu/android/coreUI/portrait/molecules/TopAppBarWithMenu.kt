package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.PopupProperties
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Solway

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopAppBarWithMenu(
    modifier: Modifier = Modifier,
    navigateBackOrHome: () -> Unit,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    title: String? = null,
    titleSize: TextUnit = 38.sp,
    titleColor: Color = MaterialTheme.colorScheme.secondary,
    listItems: List<MenuItemData> = listOf(),
    showCollapseMenu: Boolean = true,
) {
    var expanded by remember { mutableStateOf(false) }

    CenterAlignedTopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .then(modifier),
        title = {
            Text(
                text = title ?: "",
                style = TextStyle(
                    color = titleColor,
                    fontSize = titleSize,
                    fontFamily = Solway,
                    fontWeight = FontWeight.Medium,
                ),
                textAlign = TextAlign.Center,
            )
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        actions = {
            if (showCollapseMenu) {
                IconButton(onClick = {
                    expanded = true
                }) {
                    Icon(
                        imageVector = Icons.Default.MoreVert,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary
                    )
                }

                DropdownMenu(
                    modifier = androidx.compose.ui.Modifier
                        .background(backgroundColor)
                        .width(width = 200.dp),
                    expanded = expanded,
                    onDismissRequest = {
                        expanded = false
                    },
                    offset = DpOffset(x = (-102).dp, y = (-64).dp),
                    properties = PopupProperties()
                ) {
                    listItems.forEach { menuItemData ->
                        if(!menuItemData.displayIt) {
                            return@forEach
                        }
                        DropdownMenuItem(
                            onClick = {
                                menuItemData.action()
                                expanded = false
                            },
                            enabled = true,
                            text = {
                                Text(
                                    text = menuItemData.text,
                                    fontWeight = FontWeight.Medium,
                                    fontSize = 16.sp,
                                    color = MaterialTheme.colorScheme.secondary
                                )
                            },
                            leadingIcon = {
                                Icon(
                                    imageVector = menuItemData.icon,
                                    contentDescription = menuItemData.text,
                                    tint = MaterialTheme.colorScheme.secondary,
                                )
                            }
                        )
                    }
                }
            } else {
                if(listItems.isNotEmpty()) {
                    val menuItem = listItems[0]

                    IconButton(
                        onClick = {
                            menuItem.action.invoke()
                        },
                    ) {
                        Icon(
                            imageVector = menuItem.icon,
                            contentDescription = menuItem.text,
                            tint = MaterialTheme.colorScheme.secondary,
                        )
                    }
                }
            }
        },
        navigationIcon = {
            IconButton(onClick = {
                navigateBackOrHome()
            }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    )
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun TopAppBarWithMenuPreview() {
    SobuuTheme {
        TopAppBarWithMenu(
            navigateBackOrHome = {},
            title = "Sobuu"
        )
    }
}