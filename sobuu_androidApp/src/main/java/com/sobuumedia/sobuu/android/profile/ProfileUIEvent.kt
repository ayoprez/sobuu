package com.sobuumedia.sobuu.android.profile

sealed class ProfileUIEvent {
    data class ChangeEditMode(val editMode: Boolean): ProfileUIEvent()
    data class FirstNameChanged(val firstname: String): ProfileUIEvent()
    data class LastNameChanged(val lastname: String): ProfileUIEvent()
    object startScreen: ProfileUIEvent()
    object saveEditedUserData: ProfileUIEvent()
    object logout: ProfileUIEvent()
}
