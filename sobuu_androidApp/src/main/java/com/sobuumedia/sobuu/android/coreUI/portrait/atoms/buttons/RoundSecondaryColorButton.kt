package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun RoundSecondaryColorButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    isEnabled: Boolean,
    text: String
) {
    val textColor = MaterialTheme.colorScheme.background
    val contentColor = MaterialTheme.colorScheme.secondary
    val disabledColor = MaterialTheme.colorScheme.tertiary

    FilledTonalButton(
        modifier = Modifier.fillMaxWidth().then(modifier),
        onClick = onClick,
        enabled = isEnabled,
        colors = ButtonDefaults.buttonColors(
            containerColor = contentColor,
            contentColor = textColor,
            disabledContainerColor = disabledColor,
            disabledContentColor = textColor,
        )
    ) {
        Text(
            text = text,
            modifier = Modifier.semantics { this.contentDescription = text },
            style = Typography.bodyMedium.copy(color = textColor),
            textAlign = TextAlign.Center,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun RoundSecondaryColorButtonPreview() {
    SobuuTheme {
        RoundSecondaryColorButton(
            onClick = {},
            isEnabled = true,
            text = "Secondary color button"
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun RoundSecondaryColorButtonDisabledPreview() {
    SobuuTheme {
        RoundSecondaryColorButton(
            onClick = {},
            isEnabled = false,
            text = "Secondary color button"
        )
    }
}