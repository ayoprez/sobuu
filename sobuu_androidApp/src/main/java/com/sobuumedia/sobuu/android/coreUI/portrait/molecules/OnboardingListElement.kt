package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Check
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun OnboardingListElement(
    modifier: Modifier = Modifier,
    title: String,
    description: String,
    icon: ImageVector,
    iconColor: Color
) {

    val textColor = MaterialTheme.colorScheme.secondary
    val textSecondaryColor = MaterialTheme.colorScheme.tertiary

    Column(
        modifier = Modifier.fillMaxWidth().padding(horizontal = 5.dp, vertical = 5.dp).then(modifier)
    ) {
        Row {
            Icon(
                modifier = Modifier.padding(5.dp).width(20.dp),
                imageVector = icon,
                contentDescription = icon.name,
                tint = iconColor
            )
            Text(
                modifier = Modifier.padding(4.dp),
                text = title,
                style = Typography.bodyMedium.copy(color = textColor)
            )
        }
        Text(
            modifier = Modifier.padding(horizontal = 5.dp),
            text = description,
            style = Typography.bodySmall.copy(color = textSecondaryColor)
        )
    }
}

@Preview(showSystemUi = true, showBackground = true, backgroundColor = -1, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun OnboardingListElementPreview() {
    SobuuAuthTheme {
        OnboardingListElement(
            title = "Track your reading",
            description = "Easily log the pages you've read and see your progress at a glance. Set reading goals and watch as you move closer to completing your book.",
            icon = Icons.Sharp.Check,
            iconColor = MaterialTheme.colorScheme.primary
        )
    }
}