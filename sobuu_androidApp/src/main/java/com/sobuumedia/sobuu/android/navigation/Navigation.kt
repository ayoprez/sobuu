package com.sobuumedia.sobuu.android.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.sobuumedia.sobuu.android.add_book.AddBookScreen
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.authentication.LongTextScreen
import com.sobuumedia.sobuu.android.authentication.SentEmailScreen
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.authentication.login.LoginScreen
import com.sobuumedia.sobuu.android.authentication.register.RegistrationScreen
import com.sobuumedia.sobuu.android.authentication.reset_pass.ResetPasswordScreen
import com.sobuumedia.sobuu.android.authentication.splash.SplashScreen
import com.sobuumedia.sobuu.android.book.BookCoverScreen
import com.sobuumedia.sobuu.android.book.BookScreen
import com.sobuumedia.sobuu.android.comments.AddNewCommentScreen
import com.sobuumedia.sobuu.android.comments.CommentsScreen
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingScreen
import com.sobuumedia.sobuu.android.main.HomeScreen
import com.sobuumedia.sobuu.android.onboarding.OnBoardingConsentScreen
import com.sobuumedia.sobuu.android.onboarding.OnBoardingFeaturesScreen
import com.sobuumedia.sobuu.android.onboarding.OnBoardingNewVersionScreen
import com.sobuumedia.sobuu.android.profile.ProfileScreen
import com.sobuumedia.sobuu.android.settings.settings.AboutTheAppScreen
import com.sobuumedia.sobuu.android.settings.settings.SettingsScreen
import com.sobuumedia.sobuu.android.utils.toEmailType
import com.sobuumedia.sobuu.android.utils.toTextType
import com.sobuumedia.sobuu.core.SobuuLogs
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

const val TAG = "Navigation"

@Composable
fun Navigation(navController: NavHostController, logs: SobuuLogs) {

    fun sobuuLogI(log: String) = logs.info(tag = TAG, log = log)

    NavHost(navController = navController, startDestination = Screen.SplashScreen.route) {
        // Splash screen
        composable(route = Screen.SplashScreen.route) {
            SplashScreen(
                navigateToHome = {
                    navController.navigate(Screen.HomeScreen.route) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToLogin = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToOnBoardingConsent = {
                    navController.navigate(Screen.OnBoardingConsentScreen.route) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToOnBoardingFeature = {
                    navController.navigate(Screen.OnBoardingFeaturesScreen.withArgs(false)) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToOnBoardingNewVersion = {
                    navController.navigate(Screen.OnBoardingNewVersionScreen.withArgs(false)) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }

        // Consent screen
        composable(route = Screen.OnBoardingConsentScreen.route) {
            OnBoardingConsentScreen(navigateToNext = { displayedFeature ->
                if (!displayedFeature) {
                    navController.navigate(Screen.OnBoardingFeaturesScreen.withArgs(false)) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                } else {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.SplashScreen.route) {
                            inclusive = true
                        }
                    }
                }
            })
        }

        // Features screen
        composable(
            route = Screen.OnBoardingFeaturesScreen.route + "/{navigate_back}",
            arguments = listOf(
                navArgument("navigate_back") {
                    type = NavType.BoolType
                    defaultValue = false
                    nullable = false
                }
            )
        ) {
            val navigateBack = it.arguments?.getBoolean("navigate_back") ?: false

            OnBoardingFeaturesScreen(
                navigateToNext = { displayedChangelog ->
                    if (navigateBack) {
                        navController.navigateUp()
                    } else if (!displayedChangelog) {
                        navController.navigate(Screen.OnBoardingNewVersionScreen.withArgs(false)) {
                            popUpTo(Screen.SplashScreen.route) {
                                inclusive = true
                            }
                        }
                    } else {
                        navController.navigate(Screen.LoginScreen.route) {
                            popUpTo(Screen.SplashScreen.route) {
                                inclusive = true
                            }
                        }
                    }
                }
            )
        }

        // New Version screen
        composable(
            route = Screen.OnBoardingNewVersionScreen.route + "/{button_back}",
            arguments = listOf(
                navArgument("button_back") {
                    type = NavType.BoolType
                    defaultValue = false
                    nullable = false
                }
            )
        ) {
            val navigateBack = it.arguments?.getBoolean("button_back") ?: false

            OnBoardingNewVersionScreen(
                navigateToNext = {
                    if (navigateBack) {
                        navController.navigateUp()
                    } else {
                        navController.navigate(Screen.LoginScreen.route) {
                            popUpTo(Screen.SplashScreen.route) {
                                inclusive = true
                            }
                        }
                    }
                },
            )
        }

        // Login screen
        composable(route = Screen.LoginScreen.route) {
            sobuuLogI("Navigate to Login Screen")
            LoginScreen(
                navigateToHomeScreen = {
                    navController.navigate(Screen.HomeScreen.route) {
                        popUpTo(Screen.LoginScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToForgotPasswordScreen = {
                    navController.navigate(Screen.ForgotPasswordScreen.route)
                },
                navigateToRegistrationScreen = {
                    navController.navigate(Screen.RegistrationScreen.route)
                },
                navigateToLegalTextScreen = {
                    navController.navigate(Screen.LongTextScreen.withArgs(it.name))
                }
            )
        }

        // Legal text screen
        composable(
            route = Screen.LongTextScreen.route + "/{text_type}",
            arguments = listOf(
                navArgument("text_type") {
                    type = NavType.StringType
                    defaultValue = TextType.LICENSES.name
                    nullable = false
                }
            )
        ) {
            LongTextScreen(
                navigateBack = { navController.navigateUp() },
                textType = it.arguments?.getString("text_type").toTextType()
            )
        }

        // Registration screen
        composable(route = Screen.RegistrationScreen.route) {
            RegistrationScreen(
                navigateToSentEmailScreen = {
                    navController.navigate(Screen.SentEmailScreen.withArgs(it.name))
                },
                navigateBack = {
                    navController.navigateUp()
                },
                navigateToLegalText = {
                    navController.navigate(Screen.LongTextScreen.withArgs(it.name))
                }
            )
        }

        // Sent email screen
        composable(
            route = Screen.SentEmailScreen.route + "/{email_type}",
            arguments = listOf(
                navArgument("email_type") {
                    type = NavType.StringType
                    defaultValue = EmailType.VERIFICATION.name
                    nullable = false
                }
            )
        ) {
            SentEmailScreen(
                navToLogin = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.LoginScreen.route) {
                            inclusive = true
                        }
                    }
                },
                emailType = it.arguments?.getString("email_type").toEmailType()
            )
        }

        // Reset password screen
        composable(
            route = Screen.ForgotPasswordScreen.route,
        ) {
            ResetPasswordScreen(
                navigateToSentEmailScreen = {
                    navController.navigate(Screen.SentEmailScreen.withArgs(it.name))
                },
                navigateBack = { navController.navigateUp() }
            )
        }

        // Home screen
        composable(
            route = Screen.HomeScreen.route,
        ) {
            sobuuLogI("Navigate to home screen")
            HomeScreen(
                navigateToLoginScreen = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(navController.graph.id) { inclusive = true }
                    }
                },
                navigateToProfileScreen = {
                    navController.navigate(Screen.ProfileScreen.route)
                },
                navigateToCurrentlyReadingBook = {
                    navController.navigate(Screen.CurrentlyReadingBookScreen.withArgs(it))
                },
                navigateToBookScreen = {
                    navController.navigate(Screen.BookScreen.withArgs("false", it, "true"))
                },
                navigateToAddBookScreen = {
                    navController.navigate(Screen.AddBookScreen.withArgs(it))
                },
                logs = logs
            )
        }

        // Current reading book screen
        composable(
            route = Screen.CurrentlyReadingBookScreen.route + "/{book_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            val bookId = it.arguments?.getString("book_id") ?: ""
            CurrentlyReadingScreen(
                navigateToHomeScreen = {
                    logs.info(TAG, "Navigate to home screen from currently reading book screen")
                    navController.navigate(Screen.HomeScreen.route) {
                        popUpTo(navController.graph.id) { inclusive = true }
                    }
                },
                navigateToCommentsScreen = { idBook ->
                    navController.navigate(
                        Screen.CommentsScreen.withArgs(idBook)
                    )
                },
                navigateBack = { navController.navigateUp() },
                navigateToBookScreen = { idBook, displayStartReadingBookButton ->
                    navController.navigate(
                        Screen.BookScreen.withArgs(
                            "false",
                            idBook,
                            displayStartReadingBookButton.toString()
                        )
                    )
                },
                bookId = bookId,
                logs = logs
            )
        }

        // Comments screen
        composable(
            route = Screen.CommentsScreen.route + "/{book_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            CommentsScreen(
                navigateBackOrHome = {
                    if (!navController.navigateUp()) {
                        logs.info(TAG, "Navigate to home screen from comments screen")
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToAddNewCommentScreen = { id, repliedCommentId ->
                    navController.navigate(
                        Screen.AddNewCommentScreen.withArgs(
                            id, repliedCommentId
                        )
                    )
                },
                bookId = it.arguments?.getString("book_id") ?: "",
                logs = logs
            )
        }

        // Add new comment screen
        composable(
            route = Screen.AddNewCommentScreen.route + "/{book_id}/{parent_comment_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                },
                navArgument("parent_comment_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                },
            )
        ) {
            AddNewCommentScreen(
                navigateBackOrHome = {
                    if (!navController.navigateUp()) {
                        logs.info(TAG, "Navigate to home screen from add new comment screen")
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigatePopBackStack = {
                    navController.popBackStack()
                },
                bookId = it.arguments?.getString("book_id") ?: "",
                parentCommentId = it.arguments?.getString("parent_comment_id") ?: "",
                commentsInPage = it.arguments?.getInt("comments_in_page") ?: 0,
                logs = logs
            )
        }

        // Book screen
        composable(
            route = Screen.BookScreen.route + "/{opened_after_saved}/{book_id}/{display_start_read_button}",
            arguments = listOf(
                navArgument("opened_after_saved") {
                    type = NavType.StringType
                    defaultValue = "false"
                    nullable = false
                },
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                },
                navArgument("display_start_read_button") {
                    type = NavType.StringType
                    defaultValue = "true"
                    nullable = false
                }
            )
        ) {
            BookScreen(
                navigateBackOrHome = {
                    val openedAfterSaved =
                        it.arguments?.getString("opened_after_saved")?.equals("true") ?: false
                    sobuuLogI("OpenedAfterSaved: $openedAfterSaved")

                    if (openedAfterSaved) {
                        logs.info(TAG, "Navigate back or home screen from book screen")
                        navController.navigate(Screen.HomeScreen.route)
                    } else {
                        logs.info(TAG, "Navigate back with popBackStack")
                        navController.popBackStack()
                    }
                },
                navigateToHomeScreen = {
                    logs.info(TAG, "Navigate to home screen from book screen")
                    navController.navigate(Screen.HomeScreen.route)
                },
                navigateToBookCoverScreen = { coverUrl ->
                    val encodedUrl = URLEncoder.encode(coverUrl, StandardCharsets.UTF_8.toString())
                    navController.navigate(Screen.BookCoverScreen.withArgs(encodedUrl))
                },
                bookId = it.arguments?.getString("book_id") ?: "",
                displayStartReadingButton = it.arguments?.getString("display_start_read_button")
                    ?.equals("true")
                    ?: true
            )
        }

        // Book cover screen
        composable(
            route = Screen.BookCoverScreen.route + "/{cover_url}",
            arguments = listOf(
                navArgument("cover_url") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            BookCoverScreen(
                nav = navController,
                cover = it.arguments?.getString("cover_url") ?: ""
            )
        }

        // Add book screen
        composable(
            route = Screen.AddBookScreen.route + "/{manually}",
            arguments = listOf(
                navArgument("manually") {
                    type = NavType.BoolType
                    defaultValue = false
                    nullable = false
                }
            )
        ) {
            AddBookScreen(
                navigateBackOrHome = {
                    if (!navController.navigateUp()) {
                        logs.info(TAG, "Navigate back or home screen from add book screen")
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToBookScreen = { openedAfterSaved, bookId ->
                    navController.navigate(
                        Screen.BookScreen.withArgs(
                            openedAfterSaved.toString(),
                            bookId,
                            "true"
                        )
                    ) {
                        popUpTo(Screen.BookScreen.route) {
                            inclusive = true
                        }
                    }
                },
                isManually = it.arguments?.getBoolean("manually") ?: false
            )
        }

        // Profile screen
        composable(route = Screen.ProfileScreen.route) {
            ProfileScreen(
                navigateBackOrHome = {
                    if (!navController.navigateUp()) {
                        logs.info(TAG, "Navigate back or home screen from profile screen")
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToLoginScreen = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.HomeScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToSettingsScreen = {
                    navController.navigate(Screen.SettingsScreen.route)
                },
                logs = logs
            )
        }

        // Settings screen
        composable(route = Screen.SettingsScreen.route) {
            SettingsScreen(
                navigateToAboutScreen = { navController.navigate(Screen.AboutScreen.route) },
                navigateBack = { navController.navigateUp() },
                navigateToPrivacyPolicy = {
                    navController.navigate(
                        Screen.LongTextScreen.withArgs(
                            TextType.PRIVACY_POLICY.name
                        )
                    )
                },
                navigateToTermsAndConditions = {
                    navController.navigate(
                        Screen.LongTextScreen.withArgs(
                            TextType.TERMS_AND_CONDITIONS.name
                        )
                    )
                },
                navigateToLicenses = {
                    navController.navigate(
                        Screen.LongTextScreen.withArgs(
                            TextType.LICENSES.name
                        )
                    )
                },
                navigateToChangelogs = {
                    navController.navigate(Screen.OnBoardingNewVersionScreen.withArgs(true))
                }
            )
        }

        // About the app screen
        composable(route = Screen.AboutScreen.route) {
            AboutTheAppScreen(
                navigateBack = { navController.navigateUp() },
                openOnBoardingFeatures = {
                    navController.navigate(Screen.OnBoardingFeaturesScreen.withArgs(true))
                }
            )
        }
    }
}