package com.sobuumedia.sobuu.android.add_book

import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.models.bo_models.Book
import java.io.File

sealed class AddBookUIEvent {
    data class SearchISBNChanged(val value: String): AddBookUIEvent()

    data class BookTitleChanged(val value: String): AddBookUIEvent()
    data class BookAuthorsChanged(val value: String): AddBookUIEvent()
    data class BookPictureChanged(val value: File): AddBookUIEvent()
    data class BookPublisherChanged(val value: String): AddBookUIEvent()
    data class BookPublishedDateChanged(val value: String): AddBookUIEvent()
    data class BookDescriptionChanged(val value: String): AddBookUIEvent()
    data class BookGenresChanged(val value: String): AddBookUIEvent()
    data class BookTotalPagesChanged(val value: String): AddBookUIEvent()
    data class BookTranslatedChanged(val value: Boolean): AddBookUIEvent()
    data class BookTranslatorChanged(val value: String): AddBookUIEvent()
    data class BookISBNChanged(val value: String): AddBookUIEvent()
    data class BookSerieChanged(val value: Boolean): AddBookUIEvent()
    data class BookSerieNameChanged(val value: String): AddBookUIEvent()
    data class BookSerieNumberChanged(val value: String): AddBookUIEvent()
    data class BookLanguageChanged(val value: String): AddBookUIEvent()
    data class SaveBookData(val book: Book): AddBookUIEvent()
    data class Error(val bookError: BookError): AddBookUIEvent()
    data class LogError(val log: String): AddBookUIEvent()
    data class LogInfo(val log: String): AddBookUIEvent()
    data class LogDebug(val log: String): AddBookUIEvent()
    object saveBookManuallyData: AddBookUIEvent()
    object cleanSearchISBN: AddBookUIEvent()
    object searchISBN: AddBookUIEvent()
    object removeErrorState: AddBookUIEvent()
    object closeErrorDialog: AddBookUIEvent()
    object emptyBookIdData: AddBookUIEvent()
}