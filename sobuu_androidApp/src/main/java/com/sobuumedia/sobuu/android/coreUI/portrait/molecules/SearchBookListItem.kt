package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Comment
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.sobuumedia.sobuu.SharedRes.strings.search_main_comment
import com.sobuumedia.sobuu.SharedRes.strings.search_main_comments
import com.sobuumedia.sobuu.SharedRes.strings.search_main_moreThan999
import com.sobuumedia.sobuu.SharedRes.strings.search_main_nobodyReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_onePersonReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_pluralPersonsReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_review
import com.sobuumedia.sobuu.SharedRes.strings.search_main_reviews
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userGaveUpBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userHasAlreadyReadBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userHasNotReadBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userIsReadingBook
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CurrentlyReadingIcon
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.IconAndText
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus

@Composable
fun SearchBookListItem(
    modifier: Modifier = Modifier,
    peopleReadingTheBook: Int = 0,
    title: String,
    author: String,
    totalPages: Int,
    totalReviews: Int = 0,
    totalComments: Int = 0,
    rate: Double = 0.0,
    description: String,
    cover: String,
    userHasReadTheBook: BookReadingStatus = BookReadingStatus.NOT_READ,
) {
    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.tertiary
    val dataTextColor = MaterialTheme.colorScheme.secondary
    val textColor = MaterialTheme.colorScheme.background

    val heightSize = 250.dp
    val verticalPaddingSize = 5.dp

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(heightSize)
            .padding(vertical = verticalPaddingSize)
            .border(
                border = BorderStroke(2.dp, color = backgroundColor),
                shape = RoundedCornerShape(10.dp)
            )
            .clip(RoundedCornerShape(10.dp))
            .then(modifier),
    ) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (bookCover, bottomBox, bookTitle, bookAuthors,
                bookDescription, bookPages, bookRead, peopleReading,
                bookComments, bookReviews, bookRating) = createRefs()
            val verticalMiddleGuideline = createGuidelineFromTop(0.3f)

            Text(
                text = when (userHasReadTheBook) {
                    BookReadingStatus.NOT_READ -> search_main_userHasNotReadBook.stringResource(
                        context = context
                    )

                    BookReadingStatus.READING -> search_main_userIsReadingBook.stringResource(
                        context = context
                    )

                    BookReadingStatus.FINISHED -> search_main_userHasAlreadyReadBook.stringResource(
                        context = context
                    )

                    BookReadingStatus.GIVE_UP -> search_main_userGaveUpBook.stringResource(
                        context = context
                    )
                },
                style = Typography.bodySmall.copy(color = dataTextColor),
                modifier = Modifier
                    .constrainAs(bookRead) {
                        top.linkTo(parent.top, margin = 5.dp)
                        bottom.linkTo(bookRating.top, margin = 4.dp)
                        start.linkTo(bookCover.end, margin = 4.dp)
                        end.linkTo(peopleReading.start, margin = 4.dp)
                    },
            )

            IconAndText(
                text = when {
                    peopleReadingTheBook == 0 -> search_main_nobodyReadingBook.stringResource(
                        context = context
                    )

                    peopleReadingTheBook == 1 -> "$peopleReadingTheBook ${
                        search_main_onePersonReadingBook.stringResource(
                            context = context
                        )
                    }"

                    peopleReadingTheBook > 999 -> "${
                        search_main_moreThan999.stringResource(
                            context = context
                        )
                    } ${
                        search_main_pluralPersonsReadingBook.stringResource(
                            context = context
                        )
                    }"

                    else -> "$peopleReadingTheBook ${
                        search_main_pluralPersonsReadingBook.stringResource(
                            context = context
                        )
                    }"
                },
                textStyle = Typography.bodySmall.copy(color = dataTextColor),
                customIcon = { CurrentlyReadingIcon() },
                modifier = Modifier.constrainAs(peopleReading) {
                    top.linkTo(bookRead.top)
                    bottom.linkTo(bookRead.bottom)
                    linkTo(
                        bookRead.end,
                        parent.end,
                        startMargin = 4.dp,
                        endMargin = 4.dp,
                        bias = 1F
                    )
                },
            )

            IconAndText(
                text = rate.toString(),
                textStyle = Typography.bodyMedium.copy(color = dataTextColor),
                icon = Icons.Filled.Star,
                modifier = Modifier
                    .constrainAs(bookRating) {
                        top.linkTo(bookRead.bottom)
                        bottom.linkTo(verticalMiddleGuideline)
                        start.linkTo(bookCover.end, 4.dp)
                        linkTo(
                            bookRead.bottom,
                            bottomBox.top,
                            topMargin = 10.dp,
                            bottomMargin = 10.dp,
                            bias = 1F
                        )
                    },
            )

            Text(
                text = when {
                    totalReviews == 1 -> "$totalReviews ${
                        search_main_review.stringResource(context = context)
                    }"

                    totalReviews > 999 -> "${
                        search_main_moreThan999.stringResource(context = context)
                    } ${
                        search_main_reviews.stringResource(context = context)
                    }"

                    else -> "$totalReviews ${
                        search_main_reviews.stringResource(context = context)
                    }"
                },
                style = Typography.bodyMedium.copy(color = dataTextColor),
                modifier = Modifier
                    .constrainAs(bookReviews) {
                        top.linkTo(bookRating.top)
                        bottom.linkTo(bookRating.bottom)
                        start.linkTo(bookRating.end, 2.dp)
                        end.linkTo(bookComments.start, 4.dp)
                    }
            )

            IconAndText(
                text = when {
                    totalComments == 1 -> "$totalComments ${
                        search_main_comment.stringResource(
                            context = context
                        )
                    }"

                    totalComments > 999 -> "${
                        search_main_moreThan999.stringResource(
                            context = context
                        )
                    } ${
                        search_main_comments.stringResource(context = context)
                    }"

                    else -> "$totalComments ${
                        search_main_comments.stringResource(
                            context = context
                        )
                    }"
                },
                textStyle = Typography.bodyMedium.copy(color = dataTextColor),
                icon = Icons.AutoMirrored.Filled.Comment,
                iconColor = dataTextColor,
                modifier = Modifier
                    .constrainAs(bookComments) {
                        top.linkTo(bookRating.top)
                        bottom.linkTo(bookRating.bottom)
                        start.linkTo(bookReviews.end)
                        end.linkTo(parent.end)
                        linkTo(
                            bookReviews.end,
                            parent.end,
                            startMargin = 4.dp,
                            endMargin = 4.dp,
                            bias = 1F
                        )
                    },
            )

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(15.dp))
                    .padding(1.dp)
                    .background(backgroundColor)
                    .constrainAs(bottomBox) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                        top.linkTo(verticalMiddleGuideline)
                        height = Dimension.fillToConstraints
                    },
            )

            Text(
                text = title,
                style = Typography.bodyLarge.copy(color = textColor),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.constrainAs(bookTitle) {
                    top.linkTo(bottomBox.top, 6.dp)
                    end.linkTo(bottomBox.end, 6.dp)
                    start.linkTo(bookCover.end, 6.dp)
                    width = Dimension.fillToConstraints
                }
            )

            Text(
                text = author,
                style = Typography.bodyMedium.copy(color = dataTextColor),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.constrainAs(bookAuthors) {
                    top.linkTo(bookTitle.bottom, 6.dp)
                    end.linkTo(bottomBox.end, 6.dp)
                    start.linkTo(bookCover.end, 6.dp)
                    width = Dimension.fillToConstraints
                }
            )

            Text(
                text = description,
                style = Typography.bodySmall.copy(color = textColor),
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.constrainAs(bookDescription) {
                    top.linkTo(bookAuthors.bottom, 4.dp)
                    end.linkTo(bottomBox.end, 6.dp)
                    start.linkTo(bookCover.end, 6.dp)
                    bottom.linkTo(bottomBox.bottom)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                }
            )

            Column(
                modifier = Modifier.constrainAs(bookCover) {
                    start.linkTo(parent.start, 10.dp)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom, 5.dp)
                    width = Dimension.percent(0.2f)
                    linkTo(
                        parent.top,
                        parent.bottom,
                        topMargin = 5.dp,
                        bottomMargin = 5.dp,
                        bias = 0.1f
                    )
                },
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                if (cover.isEmpty()) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_cover_placeholder),
                        contentDescription = null,
                        modifier = Modifier
                            .width(120.dp)
                            .height(200.dp),
                    )
                } else {
                    AsyncImage(
                        model = cover,
                        placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                        contentDescription = null,
                        modifier = Modifier
                            .width(74.dp)
                            .height(139.dp)
                    )
                }

                if (totalPages > 0) {
                    IconAndText(
                        text = totalPages.toString(),
                        textStyle = Typography.bodySmall.copy(color = dataTextColor),
                        iconColor = textColor,
                        iconPainter = painterResource(id = R.drawable.ic_book_open_page_variant),
                    )
                }
            }
        }
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    locale = "es"
)
@Composable
fun BookListItemPreview() {
    SobuuTheme {
        SearchBookListItem(
            peopleReadingTheBook = 5555,
            title = "El imperio final",
            author = "Brandon Sanderson",
            totalPages = 654,
            rate = 2.5,
            totalReviews = 542213,
            totalComments = 465132465,
            description = "Durante mil años han caído cenizas del cielo. Durante mil años nada ha florecido. " +
                    "Durante mil años los skaa han sido esclavizados y viven en la miseria, sumidos en un " +
                    "miedo inevitable. Durante mil años...",
            cover = "https://images-eu.ssl-images-amazon.com/images/I/51wrYVDxFNS._SY264_BO1,204,203,200_QL40_ML2_.jpg",
        )
    }
}