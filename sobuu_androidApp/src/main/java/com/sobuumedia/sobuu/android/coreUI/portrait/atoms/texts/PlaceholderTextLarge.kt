package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun PlaceholderTextLarge(
    placeholderText: String,
    isError: Boolean = false
) {
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    Text(
        modifier = Modifier.semantics { this.contentDescription = placeholderText },
        text = placeholderText,
        style = Typography.bodyLarge.copy(color = placeholderColor),
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun PlaceholderTextLargePreview() {
    PlaceholderTextLarge(
        placeholderText = "Email"
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun PlaceholderTextLargeErrorPreview() {
    PlaceholderTextLarge(
        placeholderText = "Email",
        isError = true
    )
}