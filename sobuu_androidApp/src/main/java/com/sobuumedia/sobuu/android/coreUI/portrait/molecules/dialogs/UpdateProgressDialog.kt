package com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_errorNotValidNumberInProgressDialog
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogPage
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogPercentage
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogTitle
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.SegmentText
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.SegmentedControl
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.UpdateProgressButton
import com.sobuumedia.sobuu.android.currently_reading.isNumber
import com.sobuumedia.sobuu.android.currently_reading.isPercentNumber
import com.sobuumedia.sobuu.android.currently_reading.toInt
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.getPage
import com.sobuumedia.sobuu.android.utils.getPercentage
import com.sobuumedia.sobuu.android.utils.limitToTwoDecimals
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs

private const val TAG = "UpdateProgressDialog"

enum class DialogUpdateProgressType(var typeText: String) {
    PAGE(""),
    PERCENTAGE("");

    fun getText(): String {
        return typeText
    }
}

@Composable
fun UpdateProgressDialog(
    modifier: Modifier = Modifier,
    onShowDialogState: MutableState<Boolean> = mutableStateOf(true),
    totalPages: Int,
    currentPage: Int?,
    currentPercentage: Double?,
    onChanged: (percentage: Double, page: Int) -> Unit,
    onButtonClick: () -> Unit,
    logs: SobuuLogs
) {
    DialogUpdateProgressType.PAGE.typeText =
        home_updateBookProgress_progressDialogPage.stringResource(LocalContext.current)
    DialogUpdateProgressType.PERCENTAGE.typeText =
        home_updateBookProgress_progressDialogPercentage.stringResource(LocalContext.current)

    val context = LocalContext.current
    val topTitlePadding = 10.dp
    val cornerRoundRadius = 8.dp
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val errorTextColor = MaterialTheme.colorScheme.error

    var displayInvalidNumberError by remember { mutableStateOf(false) }

    val segments = remember {
        listOf(DialogUpdateProgressType.PAGE, DialogUpdateProgressType.PERCENTAGE)
    }
    var type by remember {
        mutableStateOf(
            if (currentPage == null) segments.last() else segments.first()
        )
    }

    logs.info(TAG, "CurrentPage: $currentPage, CurrentPercentage: $currentPercentage")

    val page =
        if (currentPage == null || (currentPage < 0 && (currentPercentage != null && currentPercentage > 0.0))) {
            currentPercentage?.getPage(totalPages = totalPages)
        } else {
            currentPage
        }

    val percentage =
        if (currentPercentage == null || (currentPercentage <= 0.0 && (currentPage != null && currentPage > 10))) {
            currentPage?.getPercentage(totalPages = totalPages)
        } else {
            currentPercentage
        }

    logs.info(TAG, "Page: $page, Percentage: $percentage")

    // The dialog is loaded once with the page and the percentage null and a second time with the right data
    // To avoid storing the null data in the remember fields, don't assign them when they are null
    if (page != null && percentage != null) {
        val textPercentageState = remember {
            mutableStateOf(
                TextFieldValue(
                    text = percentage.limitToTwoDecimals(),
                    selection = TextRange(0)
                )
            )
        }

        val textPageState = remember {
            mutableStateOf(
                TextFieldValue(
                    text = page.toString(),
                    selection = TextRange(0)
                )
            )
        }

        if (onShowDialogState.value) {
            Dialog(
                onDismissRequest = { onShowDialogState.value = false },
                DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .background(
                            backgroundColor,
                            shape = RoundedCornerShape(cornerRoundRadius)
                        )
                        .padding(topTitlePadding)
                        .then(modifier),
                ) {
                    Text(
                        modifier = Modifier.padding(vertical = topTitlePadding),
                        text = home_updateBookProgress_progressDialogTitle.stringResource(context = context),
                        style = Typography.titleSmall.copy(color = textColor)
                    )

                    SegmentedControl(
                        modifier = Modifier.padding(
                            vertical = 5.dp,
                            horizontal = 20.dp
                        ),
                        segments = segments,
                        selectedSegment = type,
                        onSegmentSelected = { type = it },
                    ) {
                        SegmentText(
                            it.getText(),
                            style = Typography.bodyMedium.copy(color = textColor)
                        )
                    }

                    if (type == DialogUpdateProgressType.PAGE) {
                        PageText(
                            pageNumber = textPageState.value.text,
                            onValueChanged = { value ->
                                // If page field is empty, percentage field should be also empty, and
                                // the button should not be enabled
                                displayInvalidNumberError = false
                                textPageState.value = textPageState.value.copy(text = value)
                                textPercentageState.value = textPercentageState.value.copy(
                                    text = if (value.isBlank())
                                        ""
                                    else
                                        value.toInt()?.getPercentage(totalPages)
                                            ?.limitToTwoDecimals().toString()
                                )
                                if (value.isNumber() && value.toInt() != null && value.isNotBlank()) {
                                    onChanged(
                                        // Percentage
                                        value.toInt()?.getPercentage(totalPages)
                                            ?.limitToTwoDecimals()?.toDouble()!!,
                                        // Page
                                        value.toInt()!!
                                    )
                                } else {
                                    displayInvalidNumberError = true
                                }
                            }
                        )
                    } else {
                        PercentageText(
                            percentageNumber = textPercentageState.value.text,
                            onValueChanged = {
                                // TODO Here never can be the null word in the input field. If it's null, there should be 0.0 displayed

                                val changedText = if (it.contains(',')) {
                                    textPercentageState.value.copy(text = it.replace(',', '.'))
                                } else if (it.isBlank() || it == "-") {
                                    textPercentageState.value.copy(text = "0")
                                } else if (textPercentageState.value.text == "0") {
                                    textPercentageState.value.copy(text = it.replace("0", ""))
                                } else {
                                    textPercentageState.value.copy(text = it)
                                }
                                displayInvalidNumberError = false
                                textPercentageState.value = changedText
                                textPageState.value = changedText.copy(
                                    text = if (changedText.text.isBlank())
                                    // If it's blank, set as empty string
                                        ""
                                    else if (changedText.text.isPercentNumber())
                                    // If it's a number, set as a double
                                        changedText.text.toDouble().getPage(totalPages)
                                            .toString()
                                    else
                                    // If it's not empty but it's not a number (a letter, for example)
                                    // then, just display it
                                        changedText.text
                                )
                                if (changedText.text.isPercentNumber() && changedText.text.isNotBlank()) {
                                    onChanged(
                                        // Percentage
                                        changedText.text.toDouble().limitToTwoDecimals().toDouble(),
                                        // Page
                                        changedText.text.toDouble().getPage(totalPages)
                                    )
                                } else {
                                    displayInvalidNumberError = true
                                }
                            }
                        )
                    }

                    if (displayInvalidNumberError) {
                        Text(
                            modifier = Modifier.padding(10.dp),
                            text = home_updateBookProgress_errorNotValidNumberInProgressDialog.stringResource(
                                context = context
                            ),
                            style = Typography.bodySmall.copy(color = errorTextColor),
                        )
                    }

                    UpdateProgressButton(
                        modifier = Modifier.padding(
                            start = topTitlePadding,
                            end = topTitlePadding,
                            bottom = topTitlePadding,
                            top = if (displayInvalidNumberError) 0.dp else 50.dp
                        ),
                        onClick = {
                            onButtonClick.invoke()
                            onShowDialogState.value = false
                        },
                        isEnabled = !displayInvalidNumberError,
                    )
                }
            }
        }
    }
}

@Composable
private fun PageText(
    modifier: Modifier = Modifier,
    pageNumber: String,
    onValueChanged: (String) -> Unit,
) {
    val textColor = MaterialTheme.colorScheme.secondary

    BasicTextField(
        modifier = Modifier
            .width(IntrinsicSize.Min)
            .padding(10.dp)
            .then(modifier),
        value = pageNumber,
        maxLines = 1,
        onValueChange = { onValueChanged(it) },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        textStyle = Typography.headlineMedium.copy(color = textColor)
    )
}

@Composable
private fun PercentageText(
    modifier: Modifier = Modifier,
    percentageNumber: String,
    onValueChanged: (String) -> Unit,
) {
    val textColor = MaterialTheme.colorScheme.secondary
    val padding = 10.dp

    Row(
        modifier = Modifier.padding(padding),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        BasicTextField(
            modifier = Modifier
                .width(IntrinsicSize.Min),
            value = percentageNumber,
            maxLines = 1,
            onValueChange = { onValueChanged(it) },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number
            ),
            textStyle = Typography.headlineMedium.copy(color = textColor)
        )
        Text(
            modifier = Modifier.padding(start = padding),
            text = "%",
            style = Typography.headlineMedium.copy(color = textColor, fontWeight = FontWeight.Bold)
        )
    }
}

@Preview(group = "Done")
@Composable
fun DialogUpdateProgressPreview() {
    SobuuTheme {
        UpdateProgressDialog(
            totalPages = 200,
            currentPage = null,
            currentPercentage = 100.0,
            onButtonClick = {},
            onChanged = { _, _ -> },
            logs = NapierLogs()
        )
    }
}
