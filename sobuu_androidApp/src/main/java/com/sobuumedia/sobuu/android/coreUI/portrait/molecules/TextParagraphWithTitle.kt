package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun TextParagraphWithTitle(title: String, text: String) {

    val textColor = MaterialTheme.colorScheme.secondary

    Column {
        Text(
            modifier = Modifier.padding(5.dp),
            text = title,
            style = Typography.bodyMedium.copy(color = textColor, fontWeight = FontWeight.Bold),
        )

        Text(
            modifier = Modifier.padding(5.dp),
            text = text,
            style = Typography.bodyMedium.copy(color = textColor),
            maxLines = 10
        )
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun OnBoardingNewVersionDialogSmallPreview() {
    SobuuAuthTheme {
        Box(modifier = Modifier.background(MaterialTheme.colorScheme.background)) {
            TextParagraphWithTitle(
                title = "The idea",
                text = "The idea behind of the app is not only offer a tool let you manage your readings from your phone. There are others that offer you that. Sobuu wants to be the place where you talk about books, know about books and share your love for books"
            )
        }
    }
}