package com.sobuumedia.sobuu.android.onboarding

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.onboarding.OnBoardingNewVersionViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.onboarding.OnBoardingNewVersionView
import com.sobuumedia.sobuu.android.settings.settings.SettingsUIEvent
import com.sobuumedia.sobuu.android.settings.settings.SettingsViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun OnBoardingNewVersionScreen(
    navigateToNext: () -> Unit,
    viewModel: SettingsViewModel = koinViewModel()
) {
    val state = viewModel.uiState.collectAsState().value
    val configuration = LocalConfiguration.current

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            Box(modifier = Modifier.padding(it)) {
                when (configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        OnBoardingNewVersionViewLandscape(
                            navigateToNext = {
                                viewModel.onEvent(SettingsUIEvent.updateDisplayedNewVersionScreen)
                                navigateToNext()
                            },
                            currentVersion = state.currentVersionName
                        )
                    }

                    else -> {
                        OnBoardingNewVersionView(
                            navigateToNext = {
                                viewModel.onEvent(SettingsUIEvent.updateDisplayedNewVersionScreen)
                                navigateToNext()
                            },
                            currentVersion = state.currentVersionName
                        )
                    }
                }
            }
        }
    )
}