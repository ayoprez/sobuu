package com.sobuumedia.sobuu.android.book

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.custom_widgets.SimpleTransparentTopAppBar

@Composable
fun BookCoverScreen(
    nav: NavController? = null,
    cover: String,
) {
    Scaffold(
        topBar = {
            SimpleTransparentTopAppBar(
                nav = nav,
            )
        },
        content = {
            Box(
                modifier= Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .fillMaxSize()
            ) {
                AsyncImage(
                    model = cover,
                    placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(it)
                        .fillMaxSize(),
                )
            }
        }
    )
}