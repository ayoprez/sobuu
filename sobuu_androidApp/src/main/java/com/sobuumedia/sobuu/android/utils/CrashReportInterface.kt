package com.sobuumedia.sobuu.android.utils

interface CrashReportInterface {
    fun startComponent(componentEnabled: Boolean)
    fun reportWithCustomData(title: String, errorMessage: String)
    fun reportException(e: Exception)
}