package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_alreadyRead
import com.sobuumedia.sobuu.SharedRes.strings.home_main_giveUp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_reading
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.BookStatusType
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun BookSectionsButtonBar(
    modifier: Modifier = Modifier,
    displayCurrentlyReading: () -> Unit,
    displayFinished: () -> Unit,
    displayGaveUp: () -> Unit,
    currentSection: BookStatusType
) {
    val context = LocalContext.current

    val selectedTextColor = MaterialTheme.colorScheme.secondary
    val unselectedTextColor = MaterialTheme.colorScheme.tertiary

    val selectedTextStyle = Typography.bodyLarge
    val unselectedTextStyle = Typography.bodySmall

    Row(
        modifier = Modifier
            .padding(4.dp)
            .then(modifier),
        horizontalArrangement = Arrangement.SpaceAround,
    ) {
        TextButton(
            modifier = Modifier.semantics {
                this.contentDescription = home_main_reading.stringResource()
            },
            onClick = displayCurrentlyReading
        ) {
            Text(
                text = home_main_reading.stringResource(context = context),
                style = if (currentSection == BookStatusType.CURRENTLY_READING) {
                    selectedTextStyle.copy(color = selectedTextColor)
                } else {
                    unselectedTextStyle.copy(color = unselectedTextColor)
                }
            )
        }
        TextButton(
            modifier = Modifier.semantics {
                this.contentDescription = home_main_alreadyRead.stringResource()
            },
            onClick = displayFinished
        ) {
            Text(
                text = home_main_alreadyRead.stringResource(context = context),
                style = if (currentSection == BookStatusType.ALREADY_READ) {
                    selectedTextStyle.copy(color = selectedTextColor)
                } else {
                    unselectedTextStyle.copy(color = unselectedTextColor)
                }
            )
        }
        TextButton(
            modifier = Modifier.semantics {
                this.contentDescription = home_main_giveUp.stringResource()
            },
            onClick = displayGaveUp
        ) {
            Text(
                text = home_main_giveUp.stringResource(context = context),
                style = if (currentSection == BookStatusType.GIVE_UP) {
                    selectedTextStyle.copy(color = selectedTextColor)
                } else {
                    unselectedTextStyle.copy(color = unselectedTextColor)
                }
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = false, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BookSectionsButtonBarPreview() {
    SobuuTheme {
        BookSectionsButtonBar(
            displayCurrentlyReading = {},
            displayFinished = {},
            displayGaveUp = {},
            currentSection = BookStatusType.ALREADY_READ
        )
    }
}