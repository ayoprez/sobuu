package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.search_main_addBookByISBN
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchFurtherBlock(
    navigateToAddBookScreenByISBN: () -> Unit
) {
    val context = LocalContext.current
    val buttonColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.background
    val paddingSize = 12.dp

    ElevatedButton(
        onClick = navigateToAddBookScreenByISBN,
        colors = ButtonDefaults.buttonColors(buttonColor),
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = paddingSize)
    ) {
        Text(
            text = search_main_addBookByISBN.stringResource(context = context),
            style = Typography.bodyMedium.copy(color = textColor)
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchFurtherBlockPreview() {
    SobuuTheme {
        SearchFurtherBlock(
            navigateToAddBookScreenByISBN = {}
        )
    }
}