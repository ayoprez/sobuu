package com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.TripOrigin
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_1
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_2
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_3
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SettingsAnalyticsExplanationDialog(
    onDismissAnalyticsDialog: () -> Unit
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val dialogPadding = 20.dp
    val textStyle = Typography.bodyMedium.copy(color = textColor)

    Dialog(
        onDismissRequest = onDismissAnalyticsDialog,
        DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
                .background(backgroundColor, shape = RoundedCornerShape(8.dp))
                .padding(dialogPadding),
        ) {
            Text(
                text = settings_main_analytics_explanation.stringResource(context = context),
                style = textStyle
            )

            BulletPointText {
                Text(
                    text = settings_main_analytics_explanation_data_1.stringResource(context = context),
                    style = textStyle
                )
            }
            BulletPointText {
                Text(
                    text = settings_main_analytics_explanation_data_2.stringResource(context = context),
                    style = textStyle
                )
            }
            BulletPointText {
                Text(
                    text = settings_main_analytics_explanation_data_3.stringResource(context = context),
                    style = textStyle
                )
            }
        }
    }
}

@Composable
fun BulletPointText(
    text: @Composable () -> Unit
) {
    Row(modifier = Modifier.padding(start = 5.dp, top = 4.dp)) {
        Icon(
            modifier = Modifier.padding(5.dp),
            imageVector = Icons.Filled.TripOrigin,
            contentDescription = ""
        )
        text()
    }
}


@Preview(showBackground = true)
@Composable
fun SettingsPreview() {
    SobuuTheme {
        SettingsAnalyticsExplanationDialog(
            onDismissAnalyticsDialog = {}
        )
    }
}

@Preview(showBackground = true, locale = "es")
@Composable
fun SettingsESPreview() {
    SobuuTheme {
        SettingsAnalyticsExplanationDialog(
            onDismissAnalyticsDialog = {}
        )
    }
}

@Preview(showBackground = true, locale = "de")
@Composable
fun SettingsDEPreview() {
    SobuuTheme {
        SettingsAnalyticsExplanationDialog(
            onDismissAnalyticsDialog = {}
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360,
    locale = "de"
)
@Composable
fun SettingsLandscapeDEPreview() {
    SobuuTheme {
        SettingsAnalyticsExplanationDialog(
            onDismissAnalyticsDialog = {}
        )
    }
}