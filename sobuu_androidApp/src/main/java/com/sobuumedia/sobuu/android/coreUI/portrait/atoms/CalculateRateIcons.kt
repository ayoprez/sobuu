package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun CalculateRateIcons(
    modifier: Modifier = Modifier,
    rate: Double,
) {
    val selectedColor = MaterialTheme.colorScheme.secondary
    val unselectedColor = MaterialTheme.colorScheme.background
    val iconSize = 18.dp
    val paddingSize = 2.dp

    RatingBar(
        modifier = Modifier.then(modifier),
        value = rate.toFloat(),
        config = RatingBarConfig()
            .activeColor(selectedColor)
            .hideInactiveStars(false)
            .inactiveColor(unselectedColor)
            .stepSize(StepSize.HALF)
            .numStars(5)
            .isIndicator(true)
            .size(iconSize)
            .padding(paddingSize)
            .style(RatingBarStyle.HighLighted),
        onValueChange = {},
        onRatingChanged = {}
    )
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun RatingCalculationPreview() {
    SobuuTheme {
        CalculateRateIcons(rate = 2.5)
    }
}