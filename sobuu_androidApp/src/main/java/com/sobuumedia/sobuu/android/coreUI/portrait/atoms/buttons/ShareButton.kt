package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_share
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialButtonTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaShareText
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.IconAndText
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun ShareButton() {
    val context: Context = LocalContext.current
    val textColor = MaterialTheme.colorScheme.secondary

    Button(
        modifier = Modifier.padding(start = 10.dp, top = 10.dp, bottom = 10.dp, end = 5.dp),
        onClick = {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, settings_about_socialMediaShareText.stringResource())
            startActivity(
                context,
                Intent.createChooser(intent, settings_about_socialButtonTitle.stringResource()),
                null
            )
        }) {
        IconAndText(
            text = settings_about_share.stringResource(context = context),
            textStyle = Typography.bodyMedium.copy(color = textColor),
            icon = Icons.Filled.Share
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = false,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun ShareButtonPreview() {
    SobuuAuthTheme {
        ShareButton()
    }
}