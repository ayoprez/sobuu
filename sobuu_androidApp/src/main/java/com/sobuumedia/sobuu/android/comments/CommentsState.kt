package com.sobuumedia.sobuu.android.comments

import com.sobuumedia.sobuu.features.comments.remote.CommentError
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment

data class CommentsState(
    val isLoading: Boolean = false,
    val applyFilter: Boolean = false,
    val filterType: CommentFilterType? = null,
    val error: CommentError? = null,
    var listOFComments: List<Comment> = emptyList(),
    val newComment: String = "",
    val book: BookWithProgress? = null,
    val spoilers: Boolean = false,
    val navigateBack: Boolean = false,
    val displayReportReasonsDialog: Pair<Boolean, String> = Pair(false, ""),
    val displaySuccessDialog: Boolean = false,
    val displayErrorDialog: Boolean = false,
)