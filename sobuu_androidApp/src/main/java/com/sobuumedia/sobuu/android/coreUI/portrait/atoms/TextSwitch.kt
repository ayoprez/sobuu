package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.general_no
import com.sobuumedia.sobuu.SharedRes.strings.general_yes
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun TextSwitch(
    modifier: Modifier = Modifier,
    isChecked: Boolean,
    onChangedValue: (Boolean) -> Unit
) {
    val context = LocalContext.current

    val checkedBorderColor = MaterialTheme.colorScheme.secondary
    val checkedThumbColor = MaterialTheme.colorScheme.secondary
    val checkedBackgroundColor = MaterialTheme.colorScheme.background
    val uncheckedBorderColor = MaterialTheme.colorScheme.tertiary
    val uncheckedThumbColor = MaterialTheme.colorScheme.tertiary
    val uncheckedBackgroundColor = MaterialTheme.colorScheme.error
    val textSecondaryColor = MaterialTheme.colorScheme.background

    Switch(
        modifier = modifier,
        colors = SwitchDefaults.colors(
            checkedBorderColor = checkedBorderColor,
            checkedThumbColor = checkedThumbColor,
            checkedTrackColor = checkedBackgroundColor,
            uncheckedBorderColor = uncheckedBorderColor,
            uncheckedThumbColor = uncheckedThumbColor,
            uncheckedTrackColor = uncheckedBackgroundColor
        ),
        thumbContent = {
            Text(
                text = if(isChecked) general_yes.stringResource(context = context) else general_no.stringResource(context = context),
                style = Typography.labelSmall.copy(color = textSecondaryColor)
            )
        },
        checked = isChecked,
        onCheckedChange = onChangedValue
    )
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    backgroundColor = -1,
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun OnboardingConsentElementPreview() {
    SobuuAuthTheme {
        TextSwitch(
            isChecked = true,
            onChangedValue = {}
        )
    }
}