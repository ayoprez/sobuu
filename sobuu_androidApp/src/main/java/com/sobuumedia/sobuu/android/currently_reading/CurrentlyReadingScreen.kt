package com.sobuumedia.sobuu.android.currently_reading

import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.home_main_reading
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.current_reading.CurrentlyReadingViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.LinealProgressIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.BookProgressCard
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SobuuTopAppBar
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs.FinishBookDialog
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs.GiveUpBookDialog
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs.UpdateProgressDialog
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.current_reading.CurrentlyReadingView
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.SobuuLogs
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.koin.androidx.compose.koinViewModel

private const val TAG = "CurrentlyReadingScreen"

@Composable
fun CurrentlyReadingScreen(
    navigateToHomeScreen: () -> Unit,
    navigateToCommentsScreen: (idBook: String) -> Unit,
    navigateBack: () -> Unit,
    bookId: String,
    viewModel: CurrentlyReadingViewModel = koinViewModel(),
    navigateToBookScreen: (bookId: String, displayStartReadingButton: Boolean) -> Unit,
    logs: SobuuLogs
) {
    val context = LocalContext.current
    val configuration = LocalConfiguration.current
    val state = viewModel.state
    val bookInfo = viewModel.bookData?.book
    val progressInfo = viewModel.bookData?.bookProgress

    val displayUpdateProgressDialog = remember { mutableStateOf(false) }
    val displayGiveUpDialog = remember { mutableStateOf(false) }
    val displayFinishDialog = remember { mutableStateOf(false) }

    LaunchedEffect(viewModel, context) {
        logs.info(TAG, "----Start Screen for book $bookId")
        viewModel.onEvent(CurrentlyReadingUIEvent.StartScreen(bookId = bookId))
    }

    if (state.gaveUp || state.finished) {
        logs.info(
            TAG,
            "State is giveUp (${state.gaveUp}) or finished (${state.finished}) in currently reading screen"
        )
        navigateToHomeScreen()
        viewModel.onEvent(CurrentlyReadingUIEvent.setVariablesToFalse)
    }

    if (displayGiveUpDialog.value && bookInfo != null) {
        GiveUpBookDialog(
            bookTitle = bookInfo.title,
            showDialogState = displayGiveUpDialog,
            bookId = bookInfo.id,
            onGiveUpBook = { id ->
                viewModel.onEvent(CurrentlyReadingUIEvent.GiveUpBook(id))
            },
        )
    }

    if (displayFinishDialog.value && bookInfo != null) {
        FinishBookDialog(
            bookTitle = bookInfo.title,
            showDialogState = displayFinishDialog,
            bookId = bookInfo.id,
            logger = logs,
            finishBook = { id, rating, review ->
                viewModel.onEvent(
                    CurrentlyReadingUIEvent.FinishBook(
                        bookId = id,
                        rate = rating,
                        ratingText = review
                    )
                )
            }
        )
    }

    if(displayUpdateProgressDialog.value && bookInfo != null) {
        UpdateProgressDialog(
            onShowDialogState = displayUpdateProgressDialog,
            totalPages = bookInfo.totalPages,
            currentPage = progressInfo?.page,
            currentPercentage = progressInfo?.percentage,
            onButtonClick = {
                viewModel.onEvent(
                    CurrentlyReadingUIEvent.UpdateProgress(
                        bookId = bookInfo.id
                    )
                )
            },
            onChanged = { percentage, page ->
                viewModel.onEvent(
                    CurrentlyReadingUIEvent.UpdateProgressChanged(
                        page = page,
                        percentage = percentage
                    )
                )
            },
            logs = logs
        )
    }

    Scaffold(
        topBar = {
            SobuuTopAppBar(
                navigateBack = { navigateBack() },
                title = home_main_reading.stringResource(context = context)
            )
        },
        content = { padding ->
            when(configuration.orientation) {

                Configuration.ORIENTATION_LANDSCAPE -> {
                    CurrentlyReadingViewLandscape(
                        modifier = Modifier.padding(padding),
                        bookId = bookInfo?.id ?: "",
                        picture = bookInfo?.picture ?: "",
                        progress = progressInfo?.progressInPercentage ?: 0.0,
                        startedToRead = progressInfo?.startedToRead,
                        finishedToRead = progressInfo?.finishedToRead,
                        title = bookInfo?.title ?: "",
                        authors = bookInfo?.authors ?: emptyList(),
                        finished = progressInfo?.finished ?: false,
                        giveUp = progressInfo?.giveUp ?: false,
                        errorText = state.error?.errorMessage,
                        isLoading = state.isLoading,
                        navigationToBookScreen = navigateToBookScreen,
                        navigationToCommentsScreen = navigateToCommentsScreen,
                        openGiveUpDialog = { displayGiveUpDialog.value = true },
                        openFinishDialog = { displayFinishDialog.value = true },
                        openUpdateProgressDialog = { displayUpdateProgressDialog.value = true },
                        commentsInPage = state.commentsInPage,
                        logs = logs
                    )
                }
                else -> {
                    CurrentlyReadingView(
                        modifier = Modifier.padding(padding),
                        bookId = bookInfo?.id ?: "",
                        picture = bookInfo?.picture ?: "",
                        progress = progressInfo?.progressInPercentage ?: 0.0,
                        startedToRead = progressInfo?.startedToRead,
                        finishedToRead = progressInfo?.finishedToRead,
                        title = bookInfo?.title ?: "",
                        authors = bookInfo?.authors ?: emptyList(),
                        finished = progressInfo?.finished ?: false,
                        giveUp = progressInfo?.giveUp ?: false,
                        errorText = state.error?.errorMessage,
                        isLoading = state.isLoading,
                        navigationToBookScreen = navigateToBookScreen,
                        navigationToCommentsScreen = navigateToCommentsScreen,
                        openGiveUpDialog = { displayGiveUpDialog.value = true },
                        openFinishDialog = { displayFinishDialog.value = true },
                        openUpdateProgressDialog = { displayUpdateProgressDialog.value = true },
                        commentsInPage = state.commentsInPage,
                        logs = logs
                    )
                }
            }
        }
    )
}

@Preview(group = "Done")
@Composable
fun BookProgressCardPreview() {
    SobuuTheme {
        BookProgressCard(
            picture = "",
            progress = 23.0,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
            title = "1984",
            authors = listOf("George Orwell"),
            finished = false,
            giveUp = false,
            finishedToRead = null,
            commentsInPage = 30,
            navigateToCommentsScreen = {}
        )
    }
}

@Preview(group = "Done")
@Composable
fun LinealProgressIndicatorPreview() {
    SobuuTheme {
        LinealProgressIndicator(progress = 68.77, commentsInPage = 20)
    }
}

fun String.isNumber(): Boolean {
    return this.toIntOrNull() != null
}

fun String.isPercentNumber(): Boolean {
    return this.toDoubleOrNull() != null
}

fun String.toInt(): Int? {
    return this.toIntOrNull()
}