package com.sobuumedia.sobuu.android.book

import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.models.bo_models.Book

data class BookState (
    val isLoading: Boolean = false,
    val error: BookError? = null,
    val book: Book? = null,
    val displayStartedToRead: Boolean = false,
    val displayReportedMissingDataMessage: Boolean = false,
    val bookStartedReadingTitle: String? = null,
)