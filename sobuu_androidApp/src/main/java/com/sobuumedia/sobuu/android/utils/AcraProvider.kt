package com.sobuumedia.sobuu.android.utils

import android.app.Application
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_crashDialogCommentPrompt
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_crashDialogNegativeButtonText
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_crashDialogPositiveButtonText
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_crashDialogText
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_crashDialogTitle
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.core.SobuuLogs
import org.acra.ACRA
import org.acra.ReportField
import org.acra.config.dialog
import org.acra.config.mailSender
import org.acra.data.StringFormat
import org.acra.ktx.initAcra

private const val TAG = "AcraProvider"

/**
 * Class that provide the necessary logic to send the crash reports using ACRA
 */
class AcraProvider(
    private val logger: SobuuLogs,
    private val application: Application
) : CrashReportInterface {
    // Start the component, choosing if it's enabled or not
    override fun startComponent(componentEnabled: Boolean) {
        logger.info(TAG, "Start Acra reports component. Is enabled? $componentEnabled")

        ACRA.DEV_LOGGING = BuildConfig.DEBUG

        ACRA.errorReporter.setEnabled(componentEnabled)

        application.initAcra {
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON
            reportContent = listOf(
                ReportField.ANDROID_VERSION,
                ReportField.PHONE_MODEL,
                ReportField.CUSTOM_DATA,
                ReportField.STACK_TRACE,
                ReportField.LOGCAT
            )

            dialog {
                enabled = componentEnabled
                text = general_errors_crashDialogText.stringResource()
                title = general_errors_crashDialogTitle.stringResource()
                positiveButtonText = general_errors_crashDialogPositiveButtonText.stringResource()
                negativeButtonText = general_errors_crashDialogNegativeButtonText.stringResource()
                commentPrompt = general_errors_crashDialogCommentPrompt.stringResource()
                resTheme = R.style.AppTheme
            }

            mailSender {
                enabled = componentEnabled
                mailTo = "error@getsobuu.com"
            }
        }
    }

    // Report a crash with the some custom data
    override fun reportWithCustomData(title: String, errorMessage: String) {
        ACRA.errorReporter.putCustomData(title, errorMessage)
    }

    // Report a crash with the exception that made the crash
    override fun reportException(e: Exception) {
        ACRA.errorReporter.handleException(e)
    }
}