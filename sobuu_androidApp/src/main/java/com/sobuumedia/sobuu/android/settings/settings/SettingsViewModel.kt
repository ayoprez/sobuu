package com.sobuumedia.sobuu.android.settings.settings

import android.content.pm.PackageManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.android.utils.AnalyticsInterface
import com.sobuumedia.sobuu.android.utils.CrashReportInterface
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SettingsViewModel(
    private val settingsRepo: ISettingsRepository,
    private val crashReport: CrashReportInterface,
    private val analytics: AnalyticsInterface
) : ViewModel() {

    private val _uiState = MutableStateFlow(SettingsState())
    val uiState: StateFlow<SettingsState> = _uiState.asStateFlow()

    init {
        fetchConsentEnable()
        fetchCurrentAppVersion()
    }

    fun onEvent(event: SettingsUIEvent) {
        when (event) {
            is SettingsUIEvent.SetThemeMode -> setDisplayMode(event.value)
            is SettingsUIEvent.displayThemeModeDialog -> {
                _uiState.value = if (uiState.value.displayThemeDialog) {
                    SettingsState(displayThemeDialog = !uiState.value.displayThemeDialog)
                } else {
                    getDisplayMode()
                    SettingsState(displayThemeDialog = true)
                }
            }

            is SettingsUIEvent.getThemeMode -> getDisplayMode()
            is SettingsUIEvent.displayAnalyticsDialog -> {
                _uiState.value =
                    SettingsState(displayAnalyticsExplanationDialog = !uiState.value.displayAnalyticsExplanationDialog)
            }
            is SettingsUIEvent.updateDisplayedNewVersionScreen -> {
                markVersionDialogAsShown()
            }

            is SettingsUIEvent.displayNewVersionDialog -> {
                shouldShowVersionDialog()
            }

            is SettingsUIEvent.getAnalyticsEnabled -> getAnalyticsEnabled()

            is SettingsUIEvent.AnalyticsConsentChanged -> updateAnalyticsConsentValue(event.value)

            is SettingsUIEvent.CrashReportConsentChanged -> updateCrashReportConsentValue(event.value)

            is SettingsUIEvent.UpdateDisplayedConsentScreen -> updateDisplayedContentScreenValue(
                event.displayed
            )

            is SettingsUIEvent.UpdateDisplayedFeatureScreen -> updateDisplayedFeatureScreenValue(
                event.displayed
            )

            is SettingsUIEvent.startAnalytics -> startAnalytics()

            is SettingsUIEvent.startCrashReports -> startCrashReports()

            is SettingsUIEvent.openFeaturesScreenFromSettings -> {
                _uiState.value = uiState.value.copy(isOpenedFromSettings = true)
            }
        }
    }

    private fun getDisplayMode() {
        viewModelScope.launch {
            val theme = settingsRepo.getAppTheme()

            _uiState.value = uiState.value.copy(currentTheme = theme)
        }
    }

    private fun getDisplayModeAndOpenDialog() {
        viewModelScope.launch {
            val theme = settingsRepo.getAppTheme()

            _uiState.value = uiState.value.copy(currentTheme = theme)
        }
    }

    private fun setDisplayMode(selectedMode: ThemeOptions) {
        viewModelScope.launch {
            _uiState.value = uiState.value.copy(
                isLoading = true,
                displayThemeDialog = false
            )

            settingsRepo.setAppTheme(selectedMode)

            _uiState.value = uiState.value.copy(isLoading = false, currentTheme = selectedMode)
        }
    }

    /**
     * Check if the display features screen should be displayed
     */
    private fun fetchConsentEnable() {
        viewModelScope.launch {
            val result = settingsRepo.displayedFeatureScreen()
            val analyticsEnabled = settingsRepo.getAnalyticsEnabled()
            val reportsEnabled = settingsRepo.getCrashReportsEnabled()

            _uiState.value = uiState.value.copy(
                displayFeature = result,
                currentAnalyticsEnabled = analyticsEnabled,
                currentCrashReportsEnabled = reportsEnabled
            )
        }
    }

    private fun getAnalyticsEnabled() {
        viewModelScope.launch {
            val enabled = settingsRepo.getAnalyticsEnabled()

            _uiState.value = uiState.value.copy(currentAnalyticsEnabled = enabled)
        }
    }

    private fun getCrashReportsEnabled() {
        viewModelScope.launch {
            val enabled = settingsRepo.getCrashReportsEnabled()

            _uiState.value = uiState.value.copy(currentCrashReportsEnabled = enabled)
        }
    }

    fun isPackageInstalled(packageName: String, packageManager: PackageManager): Boolean {
        return try {
            packageManager.getPackageInfo(packageName, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    /**
     * Update the consent that allows the app to collect crash reports info.
     * With True, the app collects
     * With False, the app doesn't collect
     */
    private fun updateCrashReportConsentValue(newValue: Boolean) {
        viewModelScope.launch {
            _uiState.value = uiState.value.copy(
                isLoading = true,
                displayAnalyticsExplanationDialog = false
            )
            settingsRepo.setCrashReportsEnabled(newValue)

            _uiState.value =
                uiState.value.copy(isLoading = false, currentCrashReportsEnabled = newValue)
            startCrashReports()
        }
    }

    /**
     * Update the consent that allows the app to collect analytics info.
     * With True, the app collects
     * With False, the app doesn't collect
     */
    private fun updateAnalyticsConsentValue(newValue: Boolean) {
        viewModelScope.launch {
            _uiState.value = uiState.value.copy(
                isLoading = true,
                displayAnalyticsExplanationDialog = false
            )
            settingsRepo.setAnalyticsEnabled(newValue)
            _uiState.value =
                uiState.value.copy(isLoading = false, currentAnalyticsEnabled = newValue)
            startAnalytics()
        }
    }

    /**
     * Update in the local database if the screen to ask the user for consent
     * should be displayed when starting the app
     */
    private fun updateDisplayedContentScreenValue(newValue: Boolean) {
        viewModelScope.launch {
            settingsRepo.setDisplayedConsentScreen(newValue)
        }
    }

    /**
     * Update in the local database if the screen to ask the tell the user
     * about the features of the app should be displayed when starting the app
     */
    private fun updateDisplayedFeatureScreenValue(newValue: Boolean) {
        viewModelScope.launch {
            settingsRepo.setDisplayedFeatureScreen(newValue)
        }
    }

    /**
     * Depending on the consent of the user, start or not the crash reports
     */
    private fun startCrashReports() {
        val crashReportEnabled = uiState.value.currentCrashReportsEnabled

        crashReport.startComponent(crashReportEnabled)
    }

    /**
     * Depending on the consent of the user, start or not the analytics
     */
    private fun startAnalytics() {
        val analyticsEnabled = uiState.value.currentAnalyticsEnabled

        analytics.startComponent(analyticsEnabled)
    }

    private fun shouldShowVersionDialog() {
        viewModelScope.launch {
            _uiState.value = uiState.value.copy(
                isLoading = false,
                displayNewVersionScreen = getCurrentAppVersionCode() > settingsRepo.getStoredVersionCode()
            )
        }
    }

    private fun markVersionDialogAsShown() {
        viewModelScope.launch {
            settingsRepo.storeVersionCode(getCurrentAppVersionCode())
        }
    }

    private fun fetchCurrentAppVersion() {
        _uiState.value = uiState.value.copy(currentVersionName = getCurrentAppVersion())
    }

    private fun storeVersionCode(versionCode: Int) {
        viewModelScope.launch {
            settingsRepo.storeVersionCode(versionCode)
        }
    }

    private fun getCurrentAppVersion(): String {
        return BuildConfig.VERSION_NAME
    }

    private fun getCurrentAppVersionCode(): Int {
        return BuildConfig.VERSION_CODE
    }
}