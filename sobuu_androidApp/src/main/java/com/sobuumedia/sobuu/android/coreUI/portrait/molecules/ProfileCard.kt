package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.NoBorderHalfScreenTextInputField
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@SuppressLint("UnrememberedMutableState")
@Composable
fun ProfileCard(
    modifier: Modifier = Modifier,
    firstName: String?,
    lastName: String?,
    firstNameOnValueChange: (String) -> Unit,
    lastNameOnValueChange: (String) -> Unit,
    username: String?,
    isEditMode: Boolean
) {
    val lastnameState by mutableStateOf(lastName ?: "")
    val firstnameState by mutableStateOf(firstName ?: "")
    val textColor = MaterialTheme.colorScheme.secondary
    val topPadding = 20.dp
    val bottomPadding = 30.dp
    val startPadding = 30.dp
    val editPadding = 2.dp

    val name: String = if(!firstName.isNullOrEmpty() && !lastName.isNullOrEmpty())
        "$firstName $lastName"
    else if(username != null)
        "$username"
    else
        ""

    if(isEditMode) {
        Row(modifier = Modifier.fillMaxWidth()) {
            NoBorderHalfScreenTextInputField(
                modifier = Modifier
                    .padding(editPadding),
                fieldValue = firstnameState,
                onFieldValueChange = firstNameOnValueChange,
                placeholderText = SharedRes.strings.authorization_auth_firstNameAccount.stringResource(context = LocalContext.current),
            )
            NoBorderHalfScreenTextInputField(
                modifier = Modifier
                    .padding(editPadding),
                takeHalfScreen = false,
                fieldValue = lastnameState,
                onFieldValueChange = lastNameOnValueChange,
                placeholderText = SharedRes.strings.authorization_auth_lastNameAccount.stringResource(context = LocalContext.current),
            )
        }
    } else {
        Text(
            modifier = modifier
                .padding(
                    start = startPadding,
                    top = topPadding,
                    bottom = bottomPadding
                )
                .then(modifier),
            text = name,
            style = Typography.titleSmall.copy(color = textColor)
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ProfileCardPreview() {
    SobuuTheme {
        ProfileCard(
            firstName = "Andy",
            lastName = "Warhol",
            username = "Anwar",
            firstNameOnValueChange = {},
            lastNameOnValueChange = {},
            isEditMode = true
        )
    }
}