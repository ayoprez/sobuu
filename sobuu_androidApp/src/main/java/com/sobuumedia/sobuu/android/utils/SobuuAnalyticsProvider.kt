package com.sobuumedia.sobuu.android.utils

import android.content.pm.PackageInfo
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.analytics.Analytics
import com.sobuumedia.sobuu.analytics.events.base.Event
import com.sobuumedia.sobuu.android.ContextProvider
import com.sobuumedia.sobuu.android.analytics.SobuuAnalytics
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SobuuAnalyticsProvider : AnalyticsInterface {

    private lateinit var analytics: Analytics

    override fun startComponent(analyticsEnabled: Boolean) {
        val context = ContextProvider.applicationContext
        if (analyticsEnabled) {
            // Analytics data
            val screenHeight = context.resources.configuration.screenHeightDp.dp
            val screenWidth = context.resources.configuration.screenWidthDp.dp

            val pInfo: PackageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            val version = pInfo.versionName ?: ""

            val resolution = "{ Height: $screenHeight, Width $screenWidth}"
            val operativeSystem = "Android"
            val osVersion = android.os.Build.VERSION.RELEASE
            val language = Locale.current.language

            analytics = SobuuAnalytics().startAnalytics(
                resolution = resolution,
                operativeSystem = operativeSystem,
                osVersion = osVersion,
                appVersion = version,
                language = language
            )
        }
    }

    override fun trackEvent(event: Event) {
        // If it's not initialized it's because it's not enabled, so we don't send analytics
        if (::analytics.isInitialized) {
            CoroutineScope(Dispatchers.Default).launch {
                analytics.track(event)
            }
        }
    }
}