package com.sobuumedia.sobuu.android.settings.settings

import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions

sealed class SettingsUIEvent {
    object displayThemeModeDialog: SettingsUIEvent()
    object displayAnalyticsDialog: SettingsUIEvent()
    object displayNewVersionDialog : SettingsUIEvent()
    object getThemeMode: SettingsUIEvent()
    object getAnalyticsEnabled: SettingsUIEvent()
    object openFeaturesScreenFromSettings : SettingsUIEvent()
    object startAnalytics : SettingsUIEvent()
    object startCrashReports : SettingsUIEvent()
    object updateDisplayedNewVersionScreen : SettingsUIEvent()

    data class SetThemeMode(val value: ThemeOptions): SettingsUIEvent()
    data class AnalyticsConsentChanged(val value: Boolean) : SettingsUIEvent()
    data class CrashReportConsentChanged(val value: Boolean) : SettingsUIEvent()
    data class UpdateDisplayedConsentScreen(val displayed: Boolean) : SettingsUIEvent()
    data class UpdateDisplayedFeatureScreen(val displayed: Boolean) : SettingsUIEvent()
}