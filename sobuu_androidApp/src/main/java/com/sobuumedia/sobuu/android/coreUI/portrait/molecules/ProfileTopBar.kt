package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Logout
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun ProfileTopBar(
    modifier: Modifier = Modifier,
    navigateBackOrHome: () -> Unit,
    navigateToSettingsScreen: () -> Unit,
    editUserData: () -> Unit,
    saveData: () -> Unit,
    logout: () -> Unit,
    isEditMode: Boolean
) {
    val context = LocalContext.current
    val titleSize = 24.sp
    val backgroundColor: Color = MaterialTheme.colorScheme.background
    val titleColor: Color = MaterialTheme.colorScheme.secondary

    TopAppBarWithMenu(
        modifier = modifier,
        navigateBackOrHome = navigateBackOrHome,
        title = SharedRes.strings.profile_main_title.stringResource(context = context),
        titleSize = titleSize,
        titleColor = titleColor,
        backgroundColor = backgroundColor,
        showCollapseMenu = !isEditMode,
        listItems = if(isEditMode) {
            /**
             * If is in EditMode, don't show the list of options and instead,
             * display an accept icon/button
             */
            listOf(
                MenuItemData(
                    text = "",
                    icon = Icons.Filled.Check,
                    action = saveData
                ),
            )
        } else {
            listOf(
                MenuItemData(
                    text = SharedRes.strings.profile_main_edit.stringResource(context = context),
                    icon = Icons.Filled.Edit,
                    action = editUserData
                ),
                MenuItemData(
                    text = SharedRes.strings.settings_main_title.stringResource(context = context),
                    icon = Icons.Filled.Settings,
                    action = navigateToSettingsScreen
                ),
                MenuItemData(
                    text = SharedRes.strings.profile_main_logout.stringResource(context = context),
                    icon = Icons.AutoMirrored.Filled.Logout,
                    action = logout,
                ),
            )
        },
    )
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ProfileTopBarPreview() {
    SobuuTheme {
        ProfileTopBar(
            navigateBackOrHome = {},
            navigateToSettingsScreen = {},
            editUserData = {},
            logout = {},
            saveData = {},
            isEditMode = true
        )
    }
}