package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.landscape.atoms.textinputfields.SearchBarLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ProfileIconButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun TopBarWithSearchAndProfileLandscape(
    navigateToProfileScreen: () -> Unit,
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearTextButtonClick: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val horizontalPaddingSeize = 10.dp
    val maxHeight = 72.dp

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(maxHeight)
            .background(backgroundColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(maxHeight)
                .padding(horizontalPaddingSeize)
        ) {
            SearchBarLandscape(
                modifier = Modifier.weight(6f),
                searchFieldValue = searchFieldValue,
                onSearchFieldValueChange = onSearchFieldValueChange,
                onSearchButtonClick = onSearchButtonClick,
                clearText = clearTextButtonClick,
                onSearchFieldFocusChange = onSearchFieldFocusChange
            )

            ProfileIconButton(
                modifier = Modifier.weight(1f),
                navigateToProfileScreen = navigateToProfileScreen
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO, widthDp = 720, heightDp = 360)
@Composable
fun TopBarWithSearchAndProfileLandscapePreview() {
    SobuuTheme {
        TopBarWithSearchAndProfileLandscape(
            {},
            searchFieldValue = "",
            onSearchFieldValueChange = {},
            onSearchButtonClick = {},
            clearTextButtonClick = {},
            onSearchFieldFocusChange = {}
        )
    }
}