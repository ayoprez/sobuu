package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.search_main_noBookMessage
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchWarningTextWithEmail(
    modifier: Modifier = Modifier,
    onEmailClick: () -> Unit,
    bookRequestEmail: String
) {

    val textColor = MaterialTheme.colorScheme.secondary
    val paddingSize = 12.dp

    Column(
        modifier = Modifier.then(modifier)
    ) {
        Text(
            text = search_main_noBookMessage.stringResource(context = LocalContext.current),
            style = Typography.bodyLarge.copy(color = textColor),
            textAlign = TextAlign.Justify
        )

        Text(
            modifier = Modifier.clickable { onEmailClick() }.padding(vertical = paddingSize),
            text = bookRequestEmail,
            style = Typography.bodyLarge.copy(color = textColor, fontWeight = FontWeight.Bold)
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchWarningTextWithEmailPreview() {
    SobuuTheme {
        SearchWarningTextWithEmail(
            modifier = Modifier.background(Color.White),
            onEmailClick = {},
            bookRequestEmail = "a@b.com"
        )
    }
}