package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun DialogTextButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: String,
    textColor: Color
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val horizontalPadding = 10.dp

    TextButton(
        modifier = Modifier.background(backgroundColor).then(modifier),
        onClick = onClick
    ) {
        Text(
            modifier = Modifier.padding(horizontal = horizontalPadding),
            text = text,
            style = Typography.bodyLarge.copy(color = textColor),
        )
    }
}

@Preview
@Composable
fun DialogTextButtonPreview() {
    SobuuTheme {
        DialogTextButton(
            onClick = {  },
            text = "Yes",
            textColor = MaterialTheme.colorScheme.error
        )
    }
}