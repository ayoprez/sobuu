package com.sobuumedia.sobuu.android.add_book

import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookLanguageEnglish
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.models.bo_models.Book
import java.io.File

data class AddBookState (
    val isLoading: Boolean = false,
    val searchISBN: String = "",
    val error: BookError? = null,
    val displayErrorDialog: Boolean = false,
    val displayBookListResult: Boolean = false,
    val manuallySavedBook: Book? = null,

    // Introduce manually the book info
    val bookId: String? = "",
    val bookName: String = "",
    val bookAuthors: String = "",
    val bookPublisher: String = "",
    val bookImage: File? = null,
    val bookIsTranslated: Boolean = false,
    val bookTranslator: String = "",
    val bookDescription: String = "",
    val bookPublishedDate: String = "",
    val bookGenres: List<String> = listOf(),
    val bookIsSerie: Boolean = false,
    val bookSerieName: String = "",
    val bookSerieNumber: String = "",
    val bookISBN: String = "",
    val bookLanguage: String = addBook_main_bookLanguageEnglish.stringResource(),
    val bookTotalPages: Int = -1
)