package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.authentication

import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_NO
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.SobuuBigLogo
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ForgotPasswordTextButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RegisterButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LegalButtons
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LoginForm
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun LoginViewLandscape(
    usernameValue: String,
    usernameOnValueChange: (String) -> Unit,
    passwordValue: String,
    passwordOnValueChange: (String) -> Unit,
    onLoginButtonClicked: () -> Unit,
    nameHasError: Boolean,
    errorText: String?,
    isLoading: Boolean,
    forgotPasswordButtonOnClick: () -> Unit,
    registerButtonOnClick: () -> Unit,
    termsButtonOnClick: () -> Unit,
    privacyButtonOnClick: () -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.primary

    if (isLoading) {
        CircularLoadingIndicator()
    } else {
        Row(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .fillMaxHeight()
                    .background(backgroundColor),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Spacer(modifier = Modifier.height(1.dp))
                SobuuBigLogo()
                Box(modifier = Modifier.padding(horizontal = 30.dp)) {
                    RegisterButton(
                        onClick = registerButtonOnClick
                    )
                }
                LegalButtons(
                    onTermsAndConditionsButtonClick = termsButtonOnClick,
                    onPrivacyPolicyButtonClick = privacyButtonOnClick
                )
                Spacer(modifier = Modifier.height(1.dp))
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(backgroundColor),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Spacer(modifier = Modifier.height(1.dp))
                Column(
                    Modifier.padding(horizontal = 25.dp)
                ) {

                    Spacer(modifier = Modifier.weight(1.0f))

                    LoginForm(
                        nameFieldValue = usernameValue,
                        onNameValueChange = usernameOnValueChange,
                        passwordFieldValue = passwordValue,
                        onPasswordValueChange = passwordOnValueChange,
                        onLoginButtonClick = onLoginButtonClicked,
                        nameIsError = nameHasError
                    )

                    if (errorText != null) {
                        Spacer(modifier = Modifier.weight(1.0f))

                        Text(
                            text = errorText,
                            modifier = Modifier
                                .padding(horizontal = 20.dp)
                                .semantics { this.contentDescription = errorText },
                            style = Typography.bodyMedium.copy(color = MaterialTheme.colorScheme.error),
                            textAlign = TextAlign.Center,
                        )

                        Spacer(modifier = Modifier.weight(1.0f))
                    } else {
                        Spacer(modifier = Modifier.height(35.dp))
                    }

                    ForgotPasswordTextButton(
                        onClick = forgotPasswordButtonOnClick
                    )

                    Spacer(modifier = Modifier.weight(1.0f))
                }
                Spacer(modifier = Modifier.height(1.dp))
            }
        }
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun LoginViewLandscapeDarkPreview() {
    SobuuAuthTheme {
        LoginViewLandscape(
            usernameValue = "",
            usernameOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            onLoginButtonClicked = {},
            nameHasError = false,
            errorText = null,
            isLoading = true,
            forgotPasswordButtonOnClick = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun LoginViewLandscapeLightPreview() {
    SobuuAuthTheme {
        LoginViewLandscape(
            usernameValue = "",
            usernameOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            onLoginButtonClicked = {},
            nameHasError = false,
            errorText = null,
            isLoading = false,
            forgotPasswordButtonOnClick = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}