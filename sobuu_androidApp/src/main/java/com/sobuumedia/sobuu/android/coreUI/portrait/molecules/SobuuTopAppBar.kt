package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_backButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SobuuTopAppBar(
    navigateBack: () -> Unit,
    title: String,
) {
    val iconColor = MaterialTheme.colorScheme.secondary
    val titleColor = MaterialTheme.colorScheme.secondary
    val backgroundColor = MaterialTheme.colorScheme.background

    CenterAlignedTopAppBar(
        title = {
            Text(
                title,
                style = Typography.titleMedium.copy(color = titleColor)
            )
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        modifier = Modifier.fillMaxWidth(),
        navigationIcon = {
            IconButton(
                modifier = Modifier
                    .semantics {
                        this.contentDescription = contentDescription_topBar_backButton.stringResource()
                    },
                onClick = {
                    navigateBack()
                }
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = Icons.AutoMirrored.Filled.ArrowBack.name,
                    tint = iconColor,
                )
            }
        }
    )
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun TopBarCustomPreview() {
    SobuuTheme {
        SobuuTopAppBar(
            navigateBack = {},
            title = "Sobuu"
        )
    }
}