package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.onboarding

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_button
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_descriptionAnalytics
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_descriptionCrash
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_headAnalytics
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_headCrash
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_subtitle
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_title
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_warn
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RoundRedButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.OnboardingConsentElement
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun OnBoardingPermissionsView(
    navigateToNext: () -> Unit,
    crashReportIsChecked: Boolean,
    crashReportValueChanged: (Boolean) -> Unit,
    analyticsIsChecked: Boolean,
    analyticsValueChanged: (Boolean) -> Unit,
) {

    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.secondary

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(horizontal = 30.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(30.dp),
                text = onBoarding_consent_title.stringResource(context = context),
                style = Typography.titleLarge.copy(textColor),
                textAlign = TextAlign.Center
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                text = onBoarding_consent_subtitle.stringResource(context = context),
                style = Typography.bodyMedium.copy(textColor),
                textAlign = TextAlign.Center
            )
        }

        Column {
            OnboardingConsentElement(
                title = onBoarding_consent_headCrash.stringResource(context = context),
                description = onBoarding_consent_descriptionCrash.stringResource(context = context),
                isChecked = crashReportIsChecked,
                onChangedValue = crashReportValueChanged
            )

            Spacer(modifier = Modifier.height(50.dp))

            OnboardingConsentElement(
                title = onBoarding_consent_headAnalytics.stringResource(context = context),
                description = onBoarding_consent_descriptionAnalytics.stringResource(context = context),
                isChecked = analyticsIsChecked,
                onChangedValue = analyticsValueChanged
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(
                modifier = Modifier.fillMaxWidth(),
                text = onBoarding_consent_warn.stringResource(context = context),
                style = Typography.labelMedium.copy(color = textColor),
                textAlign = TextAlign.Center
            )
        }

        RoundRedButton(
            modifier = Modifier.padding(horizontal = 50.dp, vertical = 40.dp),
            onClick = navigateToNext,
            text = onBoarding_consent_button.stringResource(context = context),
            isEnabled = true
        )
    }
}

@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun OnBoardingPermissionsPreview() {
    SobuuTheme {
        OnBoardingPermissionsView(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = false
        )
    }
}

@Preview(showSystemUi = true, showBackground = true, device = "id:pixel_2", locale = "es", uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun OnBoardingPermissionsSmallDevicePreview() {
    SobuuTheme {
        OnBoardingPermissionsView(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = true
        )
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    device = "id:pixel",
    locale = "de",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun OnBoardingPermissionsSmallDeDevicePreview() {
    SobuuTheme {
        OnBoardingPermissionsView(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = true
        )
    }
}