package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.current_reading

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.sobuumedia.sobuu.android.coreUI.landscape.molecules.BookProgressCardLandscape
import com.sobuumedia.sobuu.android.coreUI.landscape.molecules.UpdateBookProgressBarLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.BookInfoButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime


private const val TAG = "CurrentlyReadingView"

@Composable
fun CurrentlyReadingViewLandscape(
    modifier: Modifier = Modifier,
    bookId: String,
    picture: String,
    progress: Double,
    startedToRead: LocalDateTime?,
    finishedToRead: LocalDateTime?,
    title: String,
    authors: List<String>,
    finished: Boolean,
    giveUp: Boolean,
    errorText: String?,
    isLoading: Boolean,
    navigationToBookScreen: (String, displayStartReadingButton: Boolean) -> Unit,
    navigationToCommentsScreen: (String) -> Unit,
    openUpdateProgressDialog: () -> Unit,
    openGiveUpDialog: () -> Unit,
    openFinishDialog: () -> Unit,
    commentsInPage: Int,
    logs: SobuuLogs
) {
    val errorTextColor = MaterialTheme.colorScheme.error
    val backgroundColor = MaterialTheme.colorScheme.background

    if (isLoading) {
        CircularLoadingIndicator()
    } else if (errorText != null) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor)
                .then(modifier),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = errorText,
                style = Typography.bodyMedium.copy(color = errorTextColor)
            )
        }
    } else {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor)
                .then(modifier),
        ) {
            val (progressCard, bookInfo, bottomBar) = createRefs()
            val middleVerticalGuideline = createGuidelineFromStart(0.65f)

            BookProgressCardLandscape(
                picture = picture,
                progress = progress,
                startedToRead = startedToRead,
                finishedToRead = finishedToRead,
                title = title,
                authors = authors,
                finished = finished,
                giveUp = giveUp,
                navigateToCommentsScreen = { navigationToCommentsScreen(bookId) },
                commentsInPage = commentsInPage,
                modifier = Modifier.constrainAs(progressCard) {
                    top.linkTo(parent.top, margin = 30.dp)
                    start.linkTo(parent.start, margin = 30.dp)
                    end.linkTo(middleVerticalGuideline, margin = 10.dp)
                    bottom.linkTo(parent.bottom, 30.dp)
                }
            )

            BookInfoButton(
                modifier = Modifier.constrainAs(bookInfo) {
                    top.linkTo(parent.top)
                    end.linkTo(progressCard.end, margin = (-20).dp)
                },
                navigateToBookScreen = { navigationToBookScreen(bookId, false) }
            )

            UpdateBookProgressBarLandscape(
                displayGiveUpDialog = { openGiveUpDialog() },
                displayFinishDialog = { openFinishDialog() },
                displayUpdateProgressDialog = { openUpdateProgressDialog() },
                modifier = Modifier.constrainAs(bottomBar) {
                    bottom.linkTo(parent.bottom, 10.dp)
                    top.linkTo(bookInfo.bottom, margin = 10.dp)
                    start.linkTo(middleVerticalGuideline, margin = 40.dp)
                    end.linkTo(parent.end, margin = 10.dp)
                }
            )
        }
    }
}


@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun CurrentlyReadingViewLandscapePreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 10.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        CurrentlyReadingViewLandscape(
            bookId = book.book.id,
            picture = book.book.picture,
            progress = book.bookProgress.progressInPercentage,
            startedToRead = book.bookProgress.startedToRead,
            finishedToRead = book.bookProgress.finishedToRead,
            title = book.book.title,
            authors = book.book.authors,
            finished = book.bookProgress.finished,
            giveUp = book.bookProgress.giveUp,
            errorText = null,
            isLoading = false,
            navigationToBookScreen = { _, _ -> },
            navigationToCommentsScreen = {},
            openUpdateProgressDialog = {},
            openFinishDialog = {},
            openGiveUpDialog = {},
            commentsInPage = 2,
            logs = NapierLogs()
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun CurrentlyReadingViewDarkLandscapePreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        CurrentlyReadingViewLandscape(
            bookId = book.book.id,
            picture = book.book.picture,
            progress = book.bookProgress.progressInPercentage,
            startedToRead = book.bookProgress.startedToRead,
            finishedToRead = book.bookProgress.finishedToRead,
            title = book.book.title,
            authors = book.book.authors,
            finished = book.bookProgress.finished,
            giveUp = book.bookProgress.giveUp,
            errorText = "There was an error",
            isLoading = false,
            navigationToBookScreen = { _, _ -> },
            navigationToCommentsScreen = {},
            openUpdateProgressDialog = {},
            openFinishDialog = {},
            openGiveUpDialog = {},
            commentsInPage = 30,
            logs = NapierLogs()
        )
    }
}