package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.profile

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.ProfileCard
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SobuuLoading
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun ProfileView(
    padding: PaddingValues,
    isLoading: Boolean,
    hasError: Boolean,
    errorText: String? = null,
    userFirstname: String? = null,
    userLastname: String? = null,
    firstNameOnValueChange: (String) -> Unit,
    lastNameOnValueChange: (String) -> Unit,
    username: String? = null,
    isEditMode: Boolean
) {

    val errorTextColor = MaterialTheme.colorScheme.error

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(padding),
    ) {
        ProfileCard(
            firstName = userFirstname,
            lastName = userLastname,
            username = username,
            isEditMode = isEditMode,
            firstNameOnValueChange = firstNameOnValueChange,
            lastNameOnValueChange = lastNameOnValueChange
        )

        if (isLoading) {
            SobuuLoading()
        }

        if (hasError) {
            Text(
                text = errorText ?: "",
                style = Typography.bodyMedium.copy(color = errorTextColor),
                color = Color.Red,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ProfileViewPreview() {
    SobuuTheme {
        ProfileView(
            padding = PaddingValues(10.dp),
            isLoading = false,
            hasError = false,
            userFirstname = "John",
            userLastname = "Hammond",
            username = "Jamon",
            isEditMode = false,
            firstNameOnValueChange = {},
            lastNameOnValueChange = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO,
    device = "spec:parent=pixel_5,orientation=landscape")
@Composable
fun ProfileViewLandscapePreview() {
    SobuuTheme {
        ProfileView(
            padding = PaddingValues(10.dp),
            isLoading = false,
            hasError = false,
            userFirstname = "John",
            userLastname = "Hammond",
            username = "Jamon",
            isEditMode = false,
            firstNameOnValueChange = {},
            lastNameOnValueChange = {}
        )
    }
}
