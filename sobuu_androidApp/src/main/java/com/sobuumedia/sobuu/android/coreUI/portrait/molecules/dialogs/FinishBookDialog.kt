package com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.sobuumedia.sobuu.SharedRes.strings.general_no
import com.sobuumedia.sobuu.SharedRes.strings.general_yes
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishText1
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishText2
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishTitle
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_rateBookHint
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.RatingStarsBar
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.DialogTextButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.NoBorderLongTextInputField
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs

private const val TAG = "FinishBookDialog"

@Composable
fun FinishBookDialog(
    modifier: Modifier = Modifier,
    showDialogState: MutableState<Boolean> = mutableStateOf(false),
    bookTitle: String,
    bookId: String,
    finishBook: (bookId: String, rating: Float, review: String) -> Unit,
    logger: SobuuLogs
) {

    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val negativeButtonColor = MaterialTheme.colorScheme.error
    val positiveButtonColor = MaterialTheme.colorScheme.primary
    val padding = 10.dp
    val roundCornerRadius = 10.dp
    
    var reviewText: String by remember { mutableStateOf("") }
    var ratingNumber: MutableState<Float> = remember { mutableFloatStateOf(0.0f) }

    var emptyRatingWarning: Boolean by remember { mutableStateOf(false) }

    Dialog(
        onDismissRequest = { showDialogState.value = false },
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
    ) {
        Card(shape = RoundedCornerShape(16.dp)) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .background(backgroundColor)
                    .padding(padding)
                    .clip(shape = RoundedCornerShape(roundCornerRadius))
                    .then(modifier),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Title
                Text(
                    modifier = Modifier.padding(padding * 2),
                    text = "${home_currentlyReading_finishTitle.stringResource(context = context)} \"$bookTitle\"",
                    style = Typography.titleSmall.copy(color = textColor)
                )

                // Text 1
                Text(
                    modifier = Modifier.padding(vertical = padding),
                    text = home_currentlyReading_finishText1.stringResource(context = context),
                    style = Typography.bodyMedium.copy(color = textColor)
                )

                // Text 2
                Text(
                    modifier = Modifier.padding(vertical = padding),
                    text = home_currentlyReading_finishText2.stringResource(context = context),
                    style = Typography.bodyMedium.copy(color = textColor)
                )

                Spacer(modifier = Modifier.padding(5.dp))
                // Stars line
                RatingStarsBar(ratingNum = ratingNumber)

                Spacer(modifier = Modifier.padding(5.dp))

                // Review text
                NoBorderLongTextInputField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .background(backgroundColor),
                    placeholderText = home_currentlyReading_rateBookHint.stringResource(
                        context = context
                    ),
                    fieldValue = reviewText,
                    onFieldValueChange = {
                        reviewText = it
                    },
                    onKeyboardActionClicked = {
                        reviewText = "${reviewText}\r\n"
                    }
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.End,
                    verticalAlignment = Alignment.Bottom
                ) {
                    DialogTextButton(
                        modifier = Modifier.padding(vertical = 10.dp),
                        onClick = { showDialogState.value = false },
                        textColor = negativeButtonColor,
                        text = general_no.stringResource(context = context),
                    )
                    DialogTextButton(
                        modifier = Modifier.padding(vertical = 10.dp),
                        onClick = {
                            if (ratingNumber.value <= 0.0f) {
                                emptyRatingWarning = true
                            } else {
                                emptyRatingWarning = false
                                finishBook(bookId, ratingNumber.value, reviewText)
                                showDialogState.value = false
                            }
                        },
                        textColor = positiveButtonColor,
                        text = general_yes.stringResource(context = context),
                    )
                }
            }
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun FinishBookDialogPreview() {
    SobuuTheme {
        FinishBookDialog(
            bookTitle = "Sharp knifes",
            bookId = "e98hinwe98h3ews",
            finishBook = {_,_,_ -> },
            logger = NapierLogs()
        )
    }
}
