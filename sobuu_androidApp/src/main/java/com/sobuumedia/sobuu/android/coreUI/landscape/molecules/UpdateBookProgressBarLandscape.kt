package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishButton
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpButton
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_updateProgressButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun UpdateBookProgressBarLandscape(
    modifier: Modifier = Modifier,
    displayUpdateProgressDialog: (Boolean) -> Unit,
    displayGiveUpDialog: (Boolean) -> Unit,
    displayFinishDialog: (Boolean) -> Unit
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.background
    val textColorColor = MaterialTheme.colorScheme.secondary

    Column(
        modifier = Modifier
            .background(backgroundColor)
            .fillMaxWidth(0.3f)
            .fillMaxHeight(0.8f)
            .then(modifier),
        verticalArrangement = Arrangement.SpaceBetween,
    ) {
        TextButton(
            modifier = Modifier.padding(vertical = 10.dp),
            onClick = { displayUpdateProgressDialog(true) }
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = home_currentlyReading_updateProgressButton.stringResource(context = context),
                style = Typography.bodyMedium.copy(color = textColorColor),
                maxLines = 2,
                textAlign = TextAlign.Center
            )
        }

        TextButton(
            modifier = Modifier.padding(vertical = 10.dp),
            onClick = { displayGiveUpDialog(true) }
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = home_currentlyReading_giveUpButton.stringResource(context = context),
                style = Typography.bodyMedium.copy(color = textColorColor),
                textAlign = TextAlign.Center
            )
        }

        TextButton(
            modifier = Modifier.padding(vertical = 10.dp),
            onClick = { displayFinishDialog(true) },
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = home_currentlyReading_finishButton.stringResource(context = context),
                style = Typography.bodyMedium.copy(color = textColorColor),
                textAlign = TextAlign.Center
            )
        }
    }
}


@Preview(showSystemUi = false, showBackground = true, locale = "en", device = "id:pixel_8",
    uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun UpdateBookProgressBarLandscapeEnPreview() {
    SobuuTheme {
        UpdateBookProgressBarLandscape(
            displayUpdateProgressDialog = {},
            displayGiveUpDialog = {},
            displayFinishDialog = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, locale = "es", device = "id:pixel_8",
    uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun UpdateBookProgressBarLandscapeEsPreview() {
    SobuuTheme {
        UpdateBookProgressBarLandscape(
            displayUpdateProgressDialog = {},
            displayGiveUpDialog = {},
            displayFinishDialog = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, locale = "de", device = "id:pixel_8",
    uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun UpdateBookProgressBarLandscapeDePreview() {
    SobuuTheme {
        UpdateBookProgressBarLandscape(
            displayUpdateProgressDialog = {},
            displayGiveUpDialog = {},
            displayFinishDialog = {}
        )
    }
}
