package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.onboarding

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_button
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_descriptionAnalytics
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_descriptionCrash
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_headAnalytics
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_headCrash
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_subtitle
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_title
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_consent_warn
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RoundRedButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.OnboardingConsentElement
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun OnBoardingPermissionsViewLandscape(
    navigateToNext: () -> Unit,
    crashReportIsChecked: Boolean,
    crashReportValueChanged: (Boolean) -> Unit,
    analyticsIsChecked: Boolean,
    analyticsValueChanged: (Boolean) -> Unit,
) {

    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.secondary

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(horizontal = 50.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {

        Column {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                text = onBoarding_consent_title.stringResource(context = context),
                style = Typography.titleLarge.copy(textColor),
                textAlign = TextAlign.Center
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                text = onBoarding_consent_subtitle.stringResource(context = context),
                style = Typography.bodyMedium.copy(textColor),
                textAlign = TextAlign.Center
            )
        }

        Column {
            OnboardingConsentElement(
                title = onBoarding_consent_headCrash.stringResource(context = context),
                description = onBoarding_consent_descriptionCrash.stringResource(context = context),
                isChecked = crashReportIsChecked,
                onChangedValue = crashReportValueChanged
            )

            OnboardingConsentElement(
                title = onBoarding_consent_headAnalytics.stringResource(context = context),
                description = onBoarding_consent_descriptionAnalytics.stringResource(context = context),
                isChecked = analyticsIsChecked,
                onChangedValue = analyticsValueChanged
            )
        }

        Text(
            modifier = Modifier.fillMaxWidth(),
            text = onBoarding_consent_warn.stringResource(context = context),
            style = Typography.labelMedium.copy(color = textColor),
            textAlign = TextAlign.Center
        )

        RoundRedButton(
            modifier = Modifier.padding(horizontal = 150.dp, vertical = 10.dp),
            onClick = navigateToNext,
            text = onBoarding_consent_button.stringResource(context = context),
            isEnabled = true
        )
    }
}

@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun OnBoardingPermissionsLandscapePreview() {
    SobuuTheme {
        OnBoardingPermissionsViewLandscape(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = false
        )
    }
}

@Preview(
    showBackground = true,
    device = "id:pixel_2",
    locale = "es",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun OnBoardingPermissionsSmallLandscapeDevicePreview() {
    SobuuTheme {
        OnBoardingPermissionsViewLandscape(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = true
        )
    }
}

@Preview(
    showBackground = true,
    device = "id:pixel",
    locale = "de",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun OnBoardingPermissionsSmallDeLandscapeDevicePreview() {
    SobuuTheme {
        OnBoardingPermissionsViewLandscape(
            navigateToNext = {},
            crashReportValueChanged = {},
            analyticsValueChanged = {},
            crashReportIsChecked = false,
            analyticsIsChecked = true
        )
    }
}