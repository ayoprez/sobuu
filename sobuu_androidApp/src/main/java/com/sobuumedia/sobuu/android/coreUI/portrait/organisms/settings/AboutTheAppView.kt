package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.settings

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_addBooksToDB
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_addBooksToDBTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_appCode
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_appCodeTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_contactTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_emailContact
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_goals
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_goalsTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_help
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_helpTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_idea
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_ideaTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaButton
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaButtonsTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_title
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whoAmI
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whoAmITitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whyIDo
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whyIDoTitle
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.EmailButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.FunctionsButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.GitlabButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ShareButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.MadeWithLove
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SobuuTopAppBar
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.TextParagraphWithTitle
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun AboutTheAppView(
    navigateBack: () -> Unit,
    openOnBoardingFeatures: () -> Unit
) {
    val context = LocalContext.current

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                SobuuTopAppBar(
                    navigateBack = { navigateBack() },
                    title = settings_about_title.stringResource(context = context),
                )
            },
            content = { padding ->
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(padding)
                        .verticalScroll(rememberScrollState())
                        .padding(horizontal = 14.dp),
                ) {
                    // About the app (idea and goals)
                    TextParagraphWithTitle(
                        title = settings_about_ideaTitle.stringResource(context = context),
                        text = settings_about_idea.stringResource(context = context)
                    )

                    TextParagraphWithTitle(
                        title = settings_about_goalsTitle.stringResource(context = context),
                        text = settings_about_goals.stringResource(context = context)
                    )

                    FunctionsButton(onClick = openOnBoardingFeatures)

                    // About me (who I am and why I do the app)
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    TextParagraphWithTitle(
                        title = settings_about_whoAmITitle.stringResource(context = context),
                        text = settings_about_whoAmI.stringResource(context = context)
                    )

                    TextParagraphWithTitle(
                        title = settings_about_whyIDoTitle.stringResource(context = context),
                        text = settings_about_whyIDo.stringResource(context = context)
                    )

                    // How to contact me
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    TextParagraphWithTitle(
                        title = settings_about_contactTitle.stringResource(context = context),
                        text = settings_about_emailContact.stringResource(context = context)
                    )
                    // TODO Uncomment this when the Mastodon account is created
//                    SobuuText(
//                        modifier = Modifier.padding(5.dp),
//                        text = settings_about_socialMediaContact.stringResource(context = context),
//                        textSize = 16.sp,
//                        maxLines = 10,
//                    )
//                    Row {
//                        MastodonButton()
                    EmailButton("info@getsobuu.com")
//                    }

                    // Where to find the code of the app
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    TextParagraphWithTitle(
                        title = settings_about_appCodeTitle.stringResource(context = context),
                        text = settings_about_appCode.stringResource(context = context)
                    )
                    GitlabButton()

                    // How to help (donations, translations, adding books to the database, sharing in social media)
                    Spacer(modifier = Modifier.padding(vertical = 10.dp))
                    TextParagraphWithTitle(
                        title = settings_about_helpTitle.stringResource(context = context),
                        text = settings_about_help.stringResource(context = context)
                    )

                    TextParagraphWithTitle(
                        title = settings_about_socialMediaButtonsTitle.stringResource(context = context),
                        text = settings_about_socialMediaButton.stringResource(context = context)
                    )

                    ShareButton()

                    TextParagraphWithTitle(
                        title = settings_about_addBooksToDBTitle.stringResource(context = context),
                        text = settings_about_addBooksToDB.stringResource(context = context)
                    )

                    EmailButton("bookrequest@getsobuu.com")

                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    MadeWithLove()
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                }
            }
        )
    }
}

@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun AboutTheAppPreview() {
    SobuuAuthTheme {
        AboutTheAppView(
            navigateBack = {},
            openOnBoardingFeatures = {}
        )
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun AboutTheAppSmallPreview() {
    SobuuAuthTheme {
        AboutTheAppView(
            navigateBack = {},
            openOnBoardingFeatures = {}
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    device = "id:pixel_2",
    widthDp = 720,
    heightDp = 360,
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun AboutTheAppSmallLandscapePreview() {
    SobuuAuthTheme {
        AboutTheAppView(
            navigateBack = {},
            openOnBoardingFeatures = {}
        )
    }
}