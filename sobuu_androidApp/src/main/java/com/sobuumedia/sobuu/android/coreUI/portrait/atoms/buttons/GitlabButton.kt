package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.sobuumedia.sobuu.SharedRes.strings.general_appName
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.IconAndText
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun GitlabButton() {
    val context: Context = LocalContext.current
    val imageSize = 24.dp
    val textColor = MaterialTheme.colorScheme.secondary

    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            val intent = Intent(Intent.ACTION_VIEW)
            val data = Uri.parse("https://gitlab.com/sobuumedia/sobuu")
            intent.data = data
            startActivity(context, intent, null)
        }) {
        IconAndText(
            text = general_appName.stringResource(context),
            textStyle = Typography.bodyMedium.copy(color = textColor),
            customIcon = {
                Image(
                    modifier = Modifier.size(imageSize),
                    painter = painterResource(id = R.drawable.gitlab),
                    contentDescription = "Gitlab",
                )
            }
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = false,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun GitlabButtonPreview() {
    SobuuAuthTheme {
        GitlabButton()
    }
}