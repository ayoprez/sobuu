package com.sobuumedia.sobuu.android.settings.settings

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.settings.AboutTheAppView

@Composable
fun AboutTheAppScreen(
    navigateBack: () -> Unit,
    openOnBoardingFeatures: (openInSettings: Boolean) -> Unit
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            Box(modifier = Modifier.padding(it)) {
                AboutTheAppView(navigateBack, openOnBoardingFeatures = {
                    openOnBoardingFeatures(true)
                })
            }
        }
    )
}