package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Article
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material.icons.filled.Abc
import androidx.compose.material.icons.filled.Analytics
import androidx.compose.material.icons.filled.Policy
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_title
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_current_version
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_licenses
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SettingsItemButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SettingsItemButtonPosition
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SettingsItemSwitch
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SettingsView(
    actionAboutTheAppButton: () -> Unit,
    actionPrivacyButton: () -> Unit,
    actionTermsButton: () -> Unit,
    actionLicensesButton: () -> Unit,
    isAnalyticsEnabled: Boolean,
    analyticsEnabledValueChanged: (Boolean) -> Unit,
    navigateToChangelogs: () -> Unit,
    analyticsInfoTap: () -> Unit,
    currentVersion: String
) {
    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.primary
    val dividerColor = MaterialTheme.colorScheme.secondary
    val versionColor = MaterialTheme.colorScheme.secondary
    val margin = 20.dp

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
    ) {
        SettingsItemButton(
            modifier = Modifier.padding(margin),
            icon = Icons.Default.Abc,
            text = settings_about_title.stringResource(context = context),
            action = actionAboutTheAppButton,
        )

        SettingsItemSwitch(
            modifier = Modifier.padding(horizontal = margin),
            icon = Icons.Default.Analytics,
            text = settings_main_analytics.stringResource(context = context),
            isSwitchChecked = isAnalyticsEnabled,
            switchOnCheckedChanged = analyticsEnabledValueChanged,
            onTapButton = analyticsInfoTap
        )

        Spacer(modifier = Modifier.weight(1f))

        SettingsItemButton(
            modifier = Modifier.padding(top = margin, start = margin, end = margin),
            icon = Icons.Default.Policy,
            text = settings_main_privacy.stringResource(context = context),
            action = actionPrivacyButton,
            position = SettingsItemButtonPosition.TOP,
        )
        HorizontalDivider(
            modifier = Modifier.padding(horizontal = margin),
            color = dividerColor,
        )
        SettingsItemButton(
            modifier = Modifier.padding(horizontal = margin),
            icon = Icons.AutoMirrored.Default.Article,
            text = settings_main_terms.stringResource(context = context),
            action = actionTermsButton,
            position = SettingsItemButtonPosition.MIDDLE,
        )
        HorizontalDivider(
            modifier = Modifier.padding(horizontal = margin),
            color = dividerColor,
        )
        SettingsItemButton(
            modifier = Modifier.padding(start = margin, end = margin),
            icon = Icons.AutoMirrored.Default.List,
            text = settings_main_licenses.stringResource(context = context),
            action = actionLicensesButton,
            position = SettingsItemButtonPosition.BOTTOM,
        )

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                modifier = Modifier
                    .padding(horizontal = margin, vertical = margin / 1.5f)
                    .clickable { navigateToChangelogs() },
                text = settings_main_current_version.stringResource(
                    context = context,
                    currentVersion
                ),
                style = Typography.labelMedium.copy(color = versionColor)
            )

            if (BuildConfig.DEBUG) {
                Text(
                    modifier = Modifier
                        .padding(horizontal = margin, vertical = margin / 1.5f),
                    text = "Debug Mode",
                    style = Typography.labelMedium.copy(color = versionColor)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SettingsPreview() {
    SobuuTheme {
        SettingsView(
            actionAboutTheAppButton = {},
            actionPrivacyButton = {},
            actionLicensesButton = {},
            actionTermsButton = {},
            isAnalyticsEnabled = true,
            analyticsEnabledValueChanged = {},
            currentVersion = "1.2.5",
            analyticsInfoTap = {},
            navigateToChangelogs = {}
        )
    }
}

@Preview(showBackground = true, locale = "es")
@Composable
fun SettingsESPreview() {
    SobuuTheme {
        SettingsView(
            actionAboutTheAppButton = {},
            actionPrivacyButton = {},
            actionTermsButton = {},
            actionLicensesButton = {},
            isAnalyticsEnabled = false,
            analyticsEnabledValueChanged = {},
            currentVersion = "1.2.5",
            analyticsInfoTap = {},
            navigateToChangelogs = {}
        )
    }
}

@Preview(showBackground = true, locale = "de")
@Composable
fun SettingsDEPreview() {
    SobuuTheme {
        SettingsView(
            actionAboutTheAppButton = {},
            actionPrivacyButton = {},
            actionLicensesButton = {},
            actionTermsButton = {},
            isAnalyticsEnabled = true,
            analyticsEnabledValueChanged = {},
            currentVersion = "1.2.5",
            analyticsInfoTap = {},
            navigateToChangelogs = {}
        )
    }
}