package com.sobuumedia.sobuu.android.settings.settings

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_title
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.settings.SettingsViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SettingsTopBar
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs.SettingsAnalyticsExplanationDialog
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.settings.SettingsView
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.stringResource
import org.koin.androidx.compose.koinViewModel

@Composable
fun SettingsScreen(
    navigateToAboutScreen: () -> Unit,
    navigateBack: () -> Unit,
    navigateToPrivacyPolicy: () -> Unit,
    navigateToTermsAndConditions: () -> Unit,
    navigateToLicenses: () -> Unit,
    navigateToChangelogs: () -> Unit,
    viewModel: SettingsViewModel = koinViewModel()
) {
    val state = viewModel.uiState.collectAsState().value
    val context = LocalContext.current
    val configuration = LocalConfiguration.current

    SobuuAuthTheme {
        Scaffold(
            topBar = {
                SettingsTopBar(
                    navigateBack = { navigateBack() },
                    title = settings_main_title.stringResource(context = context),
                )
            },
            content = { padding ->
                Box(modifier = Modifier.padding(padding)) {
                    if (state.displayAnalyticsExplanationDialog) {
                        SettingsAnalyticsExplanationDialog(
                            onDismissAnalyticsDialog = {
                                viewModel.onEvent(SettingsUIEvent.displayAnalyticsDialog)
                            }
                        )
                    }
                    when (configuration.orientation) {
                        Configuration.ORIENTATION_LANDSCAPE -> {
                            SettingsViewLandscape(
                                actionAboutTheAppButton = navigateToAboutScreen,
                                actionPrivacyButton = navigateToPrivacyPolicy,
                                actionTermsButton = navigateToTermsAndConditions,
                                actionLicensesButton = navigateToLicenses,
                                isAnalyticsEnabled = state.currentAnalyticsEnabled,
                                analyticsEnabledValueChanged = {
                                    viewModel.onEvent(SettingsUIEvent.AnalyticsConsentChanged(it))
                                },
                                analyticsInfoTap = {
                                    viewModel.onEvent(SettingsUIEvent.displayAnalyticsDialog)
                                },
                                currentVersion = state.currentVersionName,
                                navigateToChangelogs = navigateToChangelogs
                            )
                        }

                        else -> {
                            SettingsView(
                                actionAboutTheAppButton = navigateToAboutScreen,
                                actionPrivacyButton = navigateToPrivacyPolicy,
                                actionTermsButton = navigateToTermsAndConditions,
                                actionLicensesButton = navigateToLicenses,
                                isAnalyticsEnabled = state.currentAnalyticsEnabled,
                                analyticsEnabledValueChanged = {
                                    viewModel.onEvent(SettingsUIEvent.AnalyticsConsentChanged(it))
                                },
                                analyticsInfoTap = {
                                    viewModel.onEvent(SettingsUIEvent.displayAnalyticsDialog)
                                },
                                currentVersion = state.currentVersionName,
                                navigateToChangelogs = navigateToChangelogs
                            )
                        }
                    }
                }
            }
        )
    }
}