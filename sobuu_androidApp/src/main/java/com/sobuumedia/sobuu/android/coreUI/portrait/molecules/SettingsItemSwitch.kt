package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AcUnit
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.TextSwitch
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun SettingsItemSwitch(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    text: String,
    isSwitchChecked: Boolean,
    switchOnCheckedChanged: (Boolean) -> Unit,
    onTapButton: () -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val iconColor = MaterialTheme.colorScheme.secondary
    val padding = 10.dp

    Box(modifier = modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = RoundedCornerShape(10.dp)
                )
                .clickable { onTapButton() },
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                modifier = Modifier.padding(padding),
                imageVector = icon,
                colorFilter = ColorFilter.tint(iconColor),
                contentDescription = icon.name
            )

            Text(
                modifier = Modifier.padding(padding),
                text = text,
                style = Typography.bodyMedium.copy(color = textColor)
            )

            Spacer(modifier = Modifier.weight(1f))

            TextSwitch(
                modifier = Modifier.padding(horizontal = padding),
                isChecked = isSwitchChecked,
                onChangedValue = switchOnCheckedChanged
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SettingItemSwitchPreview() {
    SobuuTheme {
        SettingsItemSwitch(
            icon = Icons.Default.AcUnit,
            text = "A/C Options",
            isSwitchChecked = false,
            onTapButton = {},
            switchOnCheckedChanged = {}
        )
    }
}