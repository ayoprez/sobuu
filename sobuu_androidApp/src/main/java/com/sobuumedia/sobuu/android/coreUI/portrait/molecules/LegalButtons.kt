package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.CustomTextButton
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun LegalButtons(
    onTermsAndConditionsButtonClick: () -> Unit,
    onPrivacyPolicyButtonClick: () -> Unit,
) {
    val context = LocalContext.current

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.Bottom,
    ) {
        CustomTextButton(
            modifier = Modifier.weight(0.5f),
            onClick = onTermsAndConditionsButtonClick,
            text = settings_main_terms.stringResource(context = context)
        )

        CustomTextButton(
            modifier = Modifier.weight(0.5f),
            onClick = onPrivacyPolicyButtonClick,
            text = settings_main_privacy.stringResource(context = context)
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LegalButtonsPreview() {
    LegalButtons(
        onTermsAndConditionsButtonClick = {},
        onPrivacyPolicyButtonClick = {},
    )
}