package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes.strings.general_appName
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SobuuBigLogo() {
    var textSize by remember { mutableStateOf(100.sp) }
    val elementColor = MaterialTheme.colorScheme.secondary

    Text(
        text = general_appName.stringResource(context = LocalContext.current),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = 24.dp,
                end = 24.dp
            )
            .semantics { this.contentDescription = general_appName.stringResource() },
        style = Typography.headlineLarge.copy(color = elementColor, fontSize = textSize),
        textAlign = TextAlign.Center,
        maxLines = 1,
        onTextLayout = { textLayoutResult ->
            if (textLayoutResult.didOverflowWidth) {
                textSize = textSize.times(0.9f)
            }
        }
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun SobuuBigLogoPreview() {
    SobuuBigLogo()
}