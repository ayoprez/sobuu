package com.sobuumedia.sobuu.android

import android.app.Application
import android.content.Context
import com.sobuumedia.sobuu.android.di.androidModule
import com.sobuumedia.sobuu.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Object that provides the app context everywhere in the app
 */
object ContextProvider {
    lateinit var applicationContext: Context
        private set

    fun init(context: Context) {
        applicationContext = context.applicationContext
    }
}

class SobuuApp : Application() {
    override fun onCreate() {
        super.onCreate()
        ContextProvider.init(this)
        startKoin {
            androidContext(this@SobuuApp)
//            androidLogger()
            modules(appModule() + androidModule)
        }


    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)


    }
}