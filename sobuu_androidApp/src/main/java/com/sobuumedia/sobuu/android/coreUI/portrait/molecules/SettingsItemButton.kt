package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForwardIos
import androidx.compose.material.icons.filled.AcUnit
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography


enum class SettingsItemButtonPosition {
    TOP, MIDDLE, BOTTOM, SINGLE
}

@Composable
fun SettingsItemButton(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    text: String,
    action: () -> Unit,
    position: SettingsItemButtonPosition = SettingsItemButtonPosition.SINGLE
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val iconColor = MaterialTheme.colorScheme.secondary
    val roundCorner = 10.dp
    val padding = 10.dp

    val roundedCorners: RoundedCornerShape = when (position) {
        SettingsItemButtonPosition.TOP -> RoundedCornerShape(
            topStart = roundCorner, topEnd = roundCorner
        )

        SettingsItemButtonPosition.MIDDLE -> RoundedCornerShape(0.dp)
        SettingsItemButtonPosition.BOTTOM -> RoundedCornerShape(
            bottomStart = roundCorner, bottomEnd = roundCorner
        )

        SettingsItemButtonPosition.SINGLE -> RoundedCornerShape(roundCorner)
    }

    Box(modifier = modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = roundedCorners,
                )
                .clickable { action() }
        ) {
            Image(
                modifier = Modifier.padding(padding),
                imageVector = icon,
                colorFilter = ColorFilter.tint(iconColor),
                contentDescription = icon.name
            )

            Text(
                modifier = Modifier.padding(padding),
                text = text,
                style = Typography.bodyMedium.copy(color = textColor)
            )

            Spacer(modifier = Modifier.weight(1f))

            Image(
                modifier = Modifier.padding(padding),
                imageVector = Icons.AutoMirrored.Filled.ArrowForwardIos,
                colorFilter = ColorFilter.tint(iconColor),
                contentDescription = icon.name
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SettingItemButtonPreview() {
    SobuuTheme {
        SettingsItemButton(
            icon = Icons.Default.AcUnit,
            text = "A/C Options",
            action = {},
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SettingItemButtonPositionTopPreview() {
    SobuuTheme {
        SettingsItemButton(
            icon = Icons.Default.AcUnit,
            text = "A/C Options",
            action = {},
            position = SettingsItemButtonPosition.TOP,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SettingItemButtonPositionMiddlePreview() {
    SobuuTheme {
        SettingsItemButton(
            icon = Icons.Default.AcUnit,
            text = "A/C Options",
            action = {},
            position = SettingsItemButtonPosition.MIDDLE,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SettingItemButtonPositionBottomPreview() {
    SobuuTheme {
        SettingsItemButton(
            icon = Icons.Default.AcUnit,
            text = "A/C Options",
            action = {},
            position = SettingsItemButtonPosition.BOTTOM,
        )
    }
}