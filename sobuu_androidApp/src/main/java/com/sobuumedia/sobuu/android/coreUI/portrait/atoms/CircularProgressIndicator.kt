package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CircularProgressIndicator(
    modifier: Modifier = Modifier,
    progress: Double = 0.0
) {
    val borderColor = MaterialTheme.colorScheme.error.copy(alpha = 0.7f)
    val contentColor = MaterialTheme.colorScheme.primary.copy(alpha = 0.7f)
    val textColor = MaterialTheme.colorScheme.onPrimary

    val indicatorSize = 70.dp
    val contentSize = 55.dp

    Box(
        modifier = Modifier
            .width(indicatorSize)
            .height(indicatorSize)
            .wrapContentSize(Alignment.Center)
            .then(modifier)
    ) {
        Box(
            modifier = Modifier
                .size(indicatorSize)
                .align(Alignment.Center)
                .clip(CircleShape)
                .background(borderColor)
        )
        Box(
            modifier = Modifier
                .size(contentSize)
                .align(Alignment.Center)
                .clip(CircleShape)
                .background(contentColor)
        ) {
            Text(
                text = "${progress}%",
                modifier = Modifier.align(Alignment.Center),
                style = Typography.bodyMedium.copy(color = textColor)
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun CircularProgressIndicatorDarkPreview() {
    SobuuAuthTheme {
        CircularProgressIndicator(
            progress = 100.0
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CircularProgressIndicatorPreview() {
    SobuuAuthTheme {
        CircularProgressIndicator()
    }
}