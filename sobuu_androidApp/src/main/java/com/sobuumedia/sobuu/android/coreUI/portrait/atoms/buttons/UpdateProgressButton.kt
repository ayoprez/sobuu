package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun UpdateProgressButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    isEnabled: Boolean
) {
    RoundRedButton(
        modifier = modifier,
        onClick = onClick,
        isEnabled = isEnabled,
        text = home_updateBookProgress_progressDialogButton.stringResource(context = LocalContext.current),
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun UpdateProgressButtonPreview() {
    SobuuTheme {
        UpdateProgressButton(
            onClick = {},
            isEnabled = true
        )
    }
}