package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication

import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.SobuuBigLogo
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ForgotPasswordTextButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RegisterButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LegalButtons
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LoginForm
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun LoginView(
    usernameValue: String,
    usernameOnValueChange: (String) -> Unit,
    passwordValue: String,
    passwordOnValueChange: (String) -> Unit,
    onLoginButtonClicked: () -> Unit,
    nameHasError: Boolean,
    errorText: String?,
    isLoading: Boolean,
    forgotPasswordButtonOnClick: () -> Unit,
    registerButtonOnClick: () -> Unit,
    termsButtonOnClick: () -> Unit,
    privacyButtonOnClick: () -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.primary

    if (isLoading) {
        CircularLoadingIndicator()
    } else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Spacer(modifier = Modifier.height(1.dp))
            SobuuBigLogo()
            Column(
                Modifier.padding(horizontal = 25.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                LoginForm(
                    nameFieldValue = usernameValue,
                    onNameValueChange = usernameOnValueChange,
                    passwordFieldValue = passwordValue,
                    onPasswordValueChange = passwordOnValueChange,
                    onLoginButtonClick = onLoginButtonClicked,
                    nameIsError = nameHasError
                )

                if (errorText != null) {
                    Spacer(modifier = Modifier.height(5.dp))

                    Text(
                        text = errorText,
                        modifier = Modifier.semantics { this.contentDescription = errorText },
                        style = Typography.bodyMedium.copy(color = MaterialTheme.colorScheme.error),
                        textAlign = TextAlign.Center,
                    )

                    Spacer(modifier = Modifier.height(5.dp))
                } else {
                    Spacer(modifier = Modifier.height(35.dp))
                }

                ForgotPasswordTextButton(
                    onClick = forgotPasswordButtonOnClick
                )
                RegisterButton(
                    onClick = registerButtonOnClick
                )
            }
            LegalButtons(
                onTermsAndConditionsButtonClick = termsButtonOnClick,
                onPrivacyPolicyButtonClick = privacyButtonOnClick
            )
            Spacer(modifier = Modifier.height(1.dp))
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun LoginViewDarkPreview() {
    SobuuAuthTheme {
        LoginView(
            usernameValue = "",
            usernameOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            onLoginButtonClicked = {},
            nameHasError = false,
            errorText = null,
            isLoading = false,
            forgotPasswordButtonOnClick = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = UI_MODE_NIGHT_NO)
@Composable
fun LoginViewLightPreview() {
    SobuuAuthTheme {
        LoginView(
            usernameValue = "",
            usernameOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            onLoginButtonClicked = {},
            nameHasError = false,
            errorText = null,
            isLoading = true,
            forgotPasswordButtonOnClick = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}