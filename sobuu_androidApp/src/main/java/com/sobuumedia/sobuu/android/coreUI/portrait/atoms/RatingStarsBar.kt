package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun RatingStarsBar(
    modifier: Modifier = Modifier,
    ratingNum: MutableState<Float>
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val inactiveRatingColor = MaterialTheme.colorScheme.tertiary
    val activeRatingColor = MaterialTheme.colorScheme.primary
    val padding = 10.dp

    val ratingSize = 40.dp
    val numberOfRating = 5

    RatingBar(
        modifier = modifier
            .background(backgroundColor)
            .padding(vertical = padding),
        value = ratingNum.value,
        config = RatingBarConfig()
            .activeColor(activeRatingColor)
            .inactiveColor(inactiveRatingColor)
            .stepSize(StepSize.HALF)
            .numStars(numberOfRating)
            .size(ratingSize)
            .padding(padding)
            .style(RatingBarStyle.Normal),

        onValueChange = {
            ratingNum.value = it
        },
        onRatingChanged = {
            ratingNum.value = it
        }
    )
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun FinishBookDialogPreview() {
    val rating = remember { mutableFloatStateOf(0.0f) }
    SobuuTheme {
        RatingStarsBar(ratingNum = rating)
    }
}
