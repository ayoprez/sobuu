package com.sobuumedia.sobuu.android.utils

import com.sobuumedia.sobuu.analytics.events.base.Event

interface AnalyticsInterface {
    fun startComponent(analyticsEnabled: Boolean)
    fun trackEvent(event: Event)
}