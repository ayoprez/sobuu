package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.onboarding

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Check
import androidx.compose.material.icons.sharp.Close
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_button
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_descriptionNegative1
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_descriptionNegative2
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_descriptionPositive1
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_descriptionPositive2
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_headNegative1
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_headNegative2
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_headPositive1
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_headPositive2
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_subtitle1
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_subtitle2
import com.sobuumedia.sobuu.SharedRes.strings.onBoarding_main_title
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.RoundPrimaryColorButton
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.OnboardingListElement
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun OnBoardingViewLandscape(
    modifier: Modifier = Modifier,
    navigateToNext: () -> Unit,
) {

    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val secondaryTextColor = MaterialTheme.colorScheme.tertiary

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(horizontal = 10.dp)
            .then(modifier)
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(22.dp),
            text = onBoarding_main_title.stringResource(context = context),
            style = Typography.headlineSmall.copy(textColor),
            textAlign = TextAlign.Center
        )
        Row(
            modifier = Modifier.fillMaxHeight(0.8f)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.5f)
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = onBoarding_main_subtitle1.stringResource(context = context),
                    style = Typography.bodyLarge.copy(secondaryTextColor),
                    textAlign = TextAlign.Center
                )

                OnboardingListElement(
                    title = onBoarding_main_headPositive1.stringResource(context = context),
                    description = onBoarding_main_descriptionPositive1.stringResource(context = context),
                    icon = Icons.Sharp.Check,
                    iconColor = MaterialTheme.colorScheme.primary
                )

                OnboardingListElement(
                    title = onBoarding_main_headPositive2.stringResource(context = context),
                    description = onBoarding_main_descriptionPositive2.stringResource(context = context),
                    icon = Icons.Sharp.Check,
                    iconColor = MaterialTheme.colorScheme.primary
                )
            }
            Column {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = onBoarding_main_subtitle2.stringResource(context = context),
                    style = Typography.bodyLarge.copy(secondaryTextColor),
                    textAlign = TextAlign.Center
                )

                OnboardingListElement(
                    title = onBoarding_main_headNegative1.stringResource(context = context),
                    description = onBoarding_main_descriptionNegative1.stringResource(context = context),
                    icon = Icons.Sharp.Close,
                    iconColor = MaterialTheme.colorScheme.error
                )

                OnboardingListElement(
                    title = onBoarding_main_headNegative2.stringResource(context = context),
                    description = onBoarding_main_descriptionNegative2.stringResource(context = context),
                    icon = Icons.Sharp.Close,
                    iconColor = MaterialTheme.colorScheme.error
                )
            }
        }
        RoundPrimaryColorButton(
            modifier = Modifier.padding(horizontal = 150.dp, vertical = 0.dp),
            onClick = navigateToNext,
            text = onBoarding_main_button.stringResource(context = context),
            isEnabled = true
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360)
@Composable
fun OnBoardingLandscapePreview() {
    SobuuAuthTheme {
        OnBoardingViewLandscape(
            navigateToNext = {}
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    device = "id:pixel_2",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360)
@Composable
fun OnBoardingLandscapeSmallPreview() {
    SobuuAuthTheme {
        OnBoardingViewLandscape(
            navigateToNext = {}
        )
    }
}