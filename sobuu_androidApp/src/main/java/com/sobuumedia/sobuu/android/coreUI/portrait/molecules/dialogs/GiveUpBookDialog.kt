package com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.sobuumedia.sobuu.SharedRes.strings.general_no
import com.sobuumedia.sobuu.SharedRes.strings.general_yes
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpText1
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpText2
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpTitle
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.DialogTextButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import kotlin.random.Random

@Composable
fun GiveUpBookDialog(
    modifier: Modifier = Modifier,
    showDialogState: MutableState<Boolean> = mutableStateOf(true),
    bookTitle: String,
    bookId: String,
    onGiveUpBook: (bookId: String) -> Unit
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val affirmativeButtonTextColor = MaterialTheme.colorScheme.primary
    val negativeButtonTextColor = MaterialTheme.colorScheme.error

    val titleText = home_currentlyReading_giveUpTitle.stringResource(context = context)

    AlertDialog(
        onDismissRequest = {
            showDialogState.value = false
        },
        modifier = Modifier.then(modifier),
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
        title = {
            Text(
                text = buildAnnotatedString {
                    append("$titleText ")
                    withStyle(
                        style = SpanStyle(
                            fontWeight = FontWeight.Bold,
                            fontFamily = Typography.titleSmall.fontFamily,
                            fontSize = Typography.titleSmall.fontSize
                        )
                    ) {
                        append("\"$bookTitle\"")
                    }
                },
                style = Typography.titleSmall,
            )
        },
        containerColor = backgroundColor,
        textContentColor = textColor,
        titleContentColor = textColor,
        shape = RoundedCornerShape(10.dp),
        text = {
            val random = Random.nextInt(0, 100)

            if (random % 2 == 0) {
                Text(text = home_currentlyReading_giveUpText1.stringResource(context = context))
            } else {
                Text(text = home_currentlyReading_giveUpText2.stringResource(context = context))
            }
        },
        confirmButton = {
            DialogTextButton(
                modifier = Modifier.padding(10.dp),
                onClick = {
                    onGiveUpBook(bookId)
                    showDialogState.value = false
                },
                textColor = affirmativeButtonTextColor,
                text = general_yes.stringResource(context = context),
            )
        },
        dismissButton = {
            DialogTextButton(
                modifier = Modifier.padding(10.dp),
                onClick = { showDialogState.value = false },
                textColor = negativeButtonTextColor,
                text = general_no.stringResource(context = context),
            )
        }
    )
}

@Preview
@Composable
fun GiveUpBookDialogPreview() {
    SobuuTheme {
        GiveUpBookDialog(
            bookTitle = "The lord of the Rings: The return of the king",
            bookId = "",
            onGiveUpBook = {}
        )
    }
}