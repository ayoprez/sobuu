package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun CurrentlyReadingIcon(
    modifier: Modifier = Modifier
) {
    val backIconColor = MaterialTheme.colorScheme.error
    val frontIconColor = MaterialTheme.colorScheme.primary
    val backIconSize = 18.dp
    val frontIconSize = 16.dp

    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier,
    ) {
        Icon(
            painterResource(id = R.drawable.ic_book_open_variant),
            modifier = Modifier.size(backIconSize),
            contentDescription = null,
            tint = backIconColor,
        )
        Icon(
            painterResource(id = R.drawable.ic_account_group),
            modifier = Modifier.size(frontIconSize),
            contentDescription = null,
            tint = frontIconColor,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun CurrentlyReadingIconPreview() {
    SobuuTheme {
        CurrentlyReadingIcon()
    }
}