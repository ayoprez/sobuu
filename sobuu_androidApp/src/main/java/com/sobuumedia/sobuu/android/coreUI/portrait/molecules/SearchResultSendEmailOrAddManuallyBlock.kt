package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.SearchWarningTextWithEmail
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchResultSendEmailOrAddManuallyBlock(
    modifier: Modifier = Modifier,
    navigateToAddBookScreen: (Boolean) -> Unit,
    savedOnClipboard: (Boolean) -> Unit
) {
    val clipboardManager: ClipboardManager = LocalClipboardManager.current
    val context = LocalContext.current
    val bookRequestEmail = SharedRes.strings.search_main_requestBooksEmail.stringResource(context = context)

    Column(
        modifier = modifier
    ) {
        SearchWarningTextWithEmail(
            onEmailClick = {
                clipboardManager.setText(AnnotatedString(bookRequestEmail))
                savedOnClipboard(true)
            },
            bookRequestEmail = bookRequestEmail
        )

        SearchISBNButtonBlock(
            navigateToAddBookScreenManually = { navigateToAddBookScreen(true) }
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchResultSendEmailOrAddManuallyBlockPreview() {
    SobuuTheme {
        SearchResultSendEmailOrAddManuallyBlock(
            navigateToAddBookScreen = {},
            savedOnClipboard = {}
        )
    }
}