package com.sobuumedia.sobuu.android.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.utils.CrashReportInterface
import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import com.sobuumedia.sobuu.features.profile.remote.ProfileResult
import com.sobuumedia.sobuu.features.profile.repository.IProfileRepository
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val profileRepo: IProfileRepository,
    private val crashReport: CrashReportInterface
): ViewModel() {

    var state by mutableStateOf(ProfileState())
        private set

    init {
        loadUserProfile()
    }

    fun onEvent(event: ProfileUIEvent) {
        when(event) {
            is ProfileUIEvent.startScreen -> loadUserProfile()
            is ProfileUIEvent.logout -> logout()
            is ProfileUIEvent.ChangeEditMode -> {
                state = state.copy(editMode = event.editMode)
            }
            is ProfileUIEvent.FirstNameChanged -> {
                val profileInfo = state.profileInfo?.copy(firstName = event.firstname)
                state = state.copy(profileInfo = profileInfo)
            }
            is ProfileUIEvent.LastNameChanged -> {
                val profileInfo = state.profileInfo?.copy(lastName = event.lastname)
                state = state.copy(profileInfo = profileInfo)
            }
            is ProfileUIEvent.saveEditedUserData -> saveData()
        }
    }

    private fun handleError(error: ProfileError?) {
        when(error) {
            is ProfileError.UnknownError -> {
                crashReport.reportWithCustomData(
                    "Handle error in ProfileViewModel",
                    error.message ?: "Unknown error with empty message"
                )
            }
            else -> {
                state = state.copy(error = error)
            }
        }
    }

    private fun saveData() {
        viewModelScope.launch {
            state = state.copy(isLoading = true, error = null)

            val result = profileRepo.updateUserProfileData(profile = state.profileInfo)

            if(result.error != null) {
                handleError(result.error)
            } else {
                state = state.copy(error = null)
            }
            state = state.copy(isLoading = false, editMode = false)
        }
    }

    private fun loadUserProfile() {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true,
                error = null,
            )

            state = when(val result = profileRepo.getUserProfile()) {
                is ProfileResult.Error -> {
                    state.copy(
                        isLoading = false,
                        error = result.error
                    )
                }
                is ProfileResult.Success -> {
                    state.copy(
                        profileInfo = result.data,
                        isLoading = false,
                    )
                }
            }
        }
    }

    private fun logout() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            profileRepo.logout()

            state = state.copy(isLoading = false, logout = true)
        }
    }
}