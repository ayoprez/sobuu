package com.sobuumedia.sobuu.android.currently_reading

import com.sobuumedia.sobuu.features.book.remote.BookError

data class CurrentlyReadingState(
    val isLoading: Boolean = false,
    val finished: Boolean = false,
    val gaveUp: Boolean = false,
    val progressUpdated: Boolean = false,
    val commentsInPage: Int = 0,
    val error: BookError? = null,
)