package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun BottomRoundedOutlinedTextField(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    onKeyboardActionClicked: () -> Unit,
    passwordField: Boolean = false,
    isError: Boolean = false
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    val roundedCorner = 10.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(
                        bottomStart = roundedCorner,
                        bottomEnd = roundedCorner,
                    ),
                    color = backgroundColor,
                )
                .semantics { this.contentDescription = placeholderText },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            isError = isError,
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            visualTransformation = if (passwordField) PasswordVisualTransformation() else VisualTransformation.None,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Go,
                keyboardType = if (passwordField) KeyboardType.Password else KeyboardType.Text
            ),
            keyboardActions = KeyboardActions(
                onSend = {
                    onKeyboardActionClicked()
                    keyboardController?.hide()
                }
            ),
            shape = RoundedCornerShape(
                bottomStart = roundedCorner,
                bottomEnd = roundedCorner,
            ),
            singleLine = true,
            leadingIcon = {
                val icon = if(passwordField) Icons.Filled.Lock else Icons.Filled.Person
                Icon(
                    imageVector = icon,
                    contentDescription = icon.name,
                    tint = placeholderColor
                )
            }
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun BottomRoundedOutlinedTextFieldPreview() {
    BottomRoundedOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Password",
        onKeyboardActionClicked = {}
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun BottomRoundedOutlinedTextFieldErrorPreview() {
    BottomRoundedOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Password",
        onKeyboardActionClicked = {},
        isError = true
    )
}