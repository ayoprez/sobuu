package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import com.sobuumedia.sobuu.SharedRes.strings.home_main_finishedBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_gaveUpBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_startedBookOn
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.LinealProgressIndicator
import com.sobuumedia.sobuu.android.main.toStringDateWithDayAndTime
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun BookProgressCard(
    modifier: Modifier = Modifier,
    picture: String,
    progress: Double,
    startedToRead: LocalDateTime?,
    finishedToRead: LocalDateTime?,
    title: String,
    authors: List<String>,
    finished: Boolean,
    giveUp: Boolean,
    commentsInPage: Int,
    navigateToCommentsScreen: () -> Unit
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.secondary
    val textColor = MaterialTheme.colorScheme.background

    val startedTime = startedToRead?.toStringDateWithDayAndTime() ?: Clock.System.now()
        .toLocalDateTime(TimeZone.currentSystemDefault())
        .toStringDateWithDayAndTime()
    val startedReadText =
        "${home_main_startedBookOn.stringResource(context = context)} $startedTime"

    val finishedTime = finishedToRead?.toStringDateWithDayAndTime() ?: ""
    val finishedReadText = "${
        if (finished) {
            home_main_finishedBookOn.stringResource(context = context)
        } else {
            home_main_gaveUpBookOn.stringResource(context = context)
        }
    } $finishedTime"

    Box(
        modifier = Modifier
            .then(modifier)
            .fillMaxHeight(0.75f)
            .fillMaxWidth(0.8f)
            .border(
                border = BorderStroke(2.dp, color = backgroundColor),
                shape = RoundedCornerShape(20.dp)
            )
            .clickable { navigateToCommentsScreen() }
            .clip(RoundedCornerShape(20.dp))
            .background(backgroundColor)
    ) {

        ConstraintLayout(modifier = Modifier.fillMaxSize()) {

            val (bookCover, bookTitle, bookAuthors, startedDate,
                progressIndicator, endDate) = createRefs()

            createVerticalChain(bookTitle, bookAuthors, chainStyle = ChainStyle.Packed)

            Text(
                text = title,
                style = Typography.titleSmall.copy(color = textColor),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .constrainAs(bookTitle) {
                        start.linkTo(parent.start, margin = 50.dp)
                        end.linkTo(parent.end, margin = 50.dp)
                        centerHorizontallyTo(parent)
                        linkTo(parent.top, bookAuthors.top, bias = 0.05f)
                    },
            )
            Text(
                text = authors.joinToString(", "),
                style = Typography.bodyMedium.copy(color = textColor),
                modifier = Modifier
                    .padding(top = 16.dp)
                    .constrainAs(bookAuthors) {
                        start.linkTo(parent.start, margin = 50.dp)
                        end.linkTo(parent.end, margin = 50.dp)
                        centerHorizontallyTo(parent)
                        linkTo(bookTitle.bottom, parent.bottom, bias = 0.90f)
                    },
            )

            AsyncImage(
                model = picture,
                placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                contentDescription = null,
                modifier = Modifier
                    .padding(top = 10.dp)
                    .fillMaxHeight(0.55f)
                    .constrainAs(bookCover) {
                        top.linkTo(bookAuthors.bottom, margin = 10.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        centerHorizontallyTo(parent)
                    },
            )
            LinealProgressIndicator(
                progress = progress,
                commentsInPage = commentsInPage,
                modifier = Modifier.constrainAs(progressIndicator) {
                    top.linkTo(bookCover.bottom, margin = 20.dp)
                    end.linkTo(parent.end, margin = 50.dp)
                    start.linkTo(parent.start, margin = 50.dp)
                }
            )
            Text(
                text = startedReadText,
                style = Typography.bodySmall.copy(color = textColor),
                modifier = Modifier
                    .padding(vertical = 5.dp)
                    .constrainAs(startedDate) {
                        top.linkTo(progressIndicator.bottom)
                        start.linkTo(progressIndicator.start, margin = 10.dp)
                    },
            )
            if (finishedToRead != null && (finished || giveUp)) {
                Text(
                    text = finishedReadText,
                    style = Typography.bodySmall.copy(color = textColor),
                    modifier = Modifier
                        .padding(bottom = 20.dp)
                        .constrainAs(endDate) {
                            top.linkTo(startedDate.bottom)
                            start.linkTo(startedDate.start)
                        },
                )
            }
        }
    }
}

@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BookProgressCardPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "The lord of the rings: The return of the king",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        BookProgressCard(
            picture = book.book.picture,
            progress = book.bookProgress.progressInPercentage,
            startedToRead = book.bookProgress.startedToRead,
            finishedToRead = book.bookProgress.finishedToRead,
            title = book.book.title,
            authors = book.book.authors,
            finished = book.bookProgress.finished,
            giveUp = book.bookProgress.giveUp,
            commentsInPage = 30,
            navigateToCommentsScreen = {}
        )
    }
}