package com.sobuumedia.sobuu.android.custom_widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Book
import androidx.compose.material.icons.filled.Groups
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_books
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_challenges
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_friends
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_shelves
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ProfileIconButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.SearchBar
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.TopBarAppName
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun TopAppBarWithSearchAndProfile(
    navigateToProfileScreen: () -> Unit,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearTextButtonClick: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
) {
    Surface(
        modifier = Modifier
            .background(backgroundColor)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(backgroundColor),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TopBarAppName()
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 12.dp, end = 12.dp)
            ) {
                SearchBar(
                    searchFieldValue = searchFieldValue,
                    onSearchFieldValueChange = onSearchFieldValueChange,
                    modifier = Modifier
                        .weight(3f)
                        .semantics { this.contentDescription = searchFieldValue },
                    onSearchButtonClick = onSearchButtonClick,
                    clearText = clearTextButtonClick,
                    onSearchFieldFocusChange = onSearchFieldFocusChange
                )

                ProfileIconButton(
                    modifier = Modifier.weight(1.0f),
                    navigateToProfileScreen = navigateToProfileScreen
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SimpleTransparentTopAppBar(
    modifier: Modifier = Modifier,
    nav: NavController?,
    backgroundColor: Color = MaterialTheme.colorScheme.background.copy(alpha = 0f),
) {
    TopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .then(modifier),
        title = {
            Text("")
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        navigationIcon = {
            IconButton(onClick = {
                nav?.navigateUp()
            }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    )
}

@Composable
fun CustomBottomAppBar(
    modifier: Modifier = Modifier,
    nav: NavController? = null,
    displayLabels: Boolean = true,
) {
    val context = LocalContext.current
    NavigationBar(
        modifier = Modifier
            .fillMaxWidth(),
        containerColor = MaterialTheme.colorScheme.primary,
    ) {
//        var currentRoute by remember { mutableStateOf(HomeScreenDestination.route) }

        val config = LocalConfiguration.current
        val screenWidth = config.screenWidthDp.dp
        val itemWidth = screenWidth / 4

        Column(
            modifier = Modifier
//                .background(if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(HomeScreenDestination)
//                    currentRoute = HomeScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Book,
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
//                tint = if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_books.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
//                        fontSize = if (currentRoute == HomeScreenDestination.route) 16.sp else 12.sp,
//                        color = if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background,
//                        fontWeight = if (currentRoute == HomeScreenDestination.route) FontWeight.Medium else FontWeight.Normal,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
//                .background(if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(ShelvesScreenDestination)
//                    currentRoute = ShelvesScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_bookshelf),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
//                tint = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_shelves.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
//                        fontSize = if (currentRoute == ShelvesScreenDestination.route) 16.sp else 12.sp,
//                        color = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background,
//                        fontWeight = if (currentRoute == ShelvesScreenDestination.route) FontWeight.Medium else FontWeight.Normal,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Groups,
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_friends.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_trophy),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )

            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_challenges.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }
    }
}

data class MenuItemData(val text: String, val icon: ImageVector, val action: () -> Unit, val displayIt: Boolean = true)

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "Transparent")
@Composable
fun ComposableHomeScreenTopBarTransparentPreview() {
    SimpleTransparentTopAppBar(
        nav = null,
    )
}

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "WithSearchAndProfile")
@Composable
fun ComposableHomeScreenTopBarPreview() {
    TopAppBarWithSearchAndProfile(
        {},
        searchFieldValue = "",
        onSearchFieldValueChange = {},
        onSearchButtonClick = {},
        clearTextButtonClick = {},
        onSearchFieldFocusChange = {}
    )
}