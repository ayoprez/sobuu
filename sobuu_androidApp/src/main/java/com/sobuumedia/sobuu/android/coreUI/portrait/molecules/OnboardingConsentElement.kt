package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.TextSwitch
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun OnboardingConsentElement(
    modifier: Modifier = Modifier,
    title: String,
    description: String,
    isChecked: Boolean,
    onChangedValue: (Boolean) -> Unit
) {

    val backgroundColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.secondary
    val textSecondaryColor = MaterialTheme.colorScheme.background

    Row(
        modifier = Modifier
            .background(backgroundColor)
            .then(modifier)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 15.dp, vertical = 5.dp)
                .then(modifier),
            verticalArrangement = Arrangement.Center,
        ) {
            Row {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = title,
                    style = Typography.bodyMedium.copy(color = textColor, fontWeight = FontWeight.Medium)
                )
            }
            Text(
                modifier = Modifier.padding(horizontal = 5.dp),
                text = description,
                style = Typography.bodySmall.copy(color = textSecondaryColor)
            )
        }
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextSwitch(
                isChecked = isChecked,
                onChangedValue = onChangedValue
            )
        }
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    backgroundColor = -1,
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun OnboardingConsentElementPreview() {
    SobuuAuthTheme {
        OnboardingConsentElement(
            title = "Track your reading",
            description = "Easily log the pages you've read and see your progress at a glance. Set reading goals and watch as you move closer to completing your book.",
            isChecked = true,
            onChangedValue = {}
        )
    }
}