package com.sobuumedia.sobuu.android.currently_reading

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.utils.CrashReportInterface
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.features.comments.repository.ICommentRepository
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import kotlinx.coroutines.launch

class CurrentlyReadingViewModel(
    private val bookRepo: IBookRepository,
    private val commentsRepo: ICommentRepository,
    private val crashReport: CrashReportInterface
) : ViewModel() {

    var state by mutableStateOf(CurrentlyReadingState())
    private var pageProgress by mutableStateOf<Int?>(null)
    private var percentageProgress by mutableStateOf<Double?>(null)
    var bookData by mutableStateOf<BookWithProgress?>(null)
    var updatedProgress by mutableStateOf<BookProgress?>(null)

    fun onEvent(event: CurrentlyReadingUIEvent) {
        when (event) {
            is CurrentlyReadingUIEvent.UpdateProgress -> updateBookProgress(
                bookId = event.bookId,
                page = if (pageProgress == null) bookData?.bookProgress?.page else pageProgress,
                percentage = if (percentageProgress == null) bookData?.bookProgress?.percentage else percentageProgress,
            )

            is CurrentlyReadingUIEvent.FinishBook -> finishBook(
                event.bookId, event.rate, event.ratingText
            )

            is CurrentlyReadingUIEvent.GiveUpBook -> giveUpBook(event.bookId)
            is CurrentlyReadingUIEvent.UpdateProgressChanged -> {
                pageProgress = event.page
                percentageProgress = event.percentage
            }

            is CurrentlyReadingUIEvent.FetchBookProgressData -> fetchBookProgress(event.bookId)
            is CurrentlyReadingUIEvent.StartScreen -> getBookDataFromDB(event.bookId)
            is CurrentlyReadingUIEvent.setVariablesToFalse -> setVariablesToFalse()
        }
    }

    fun handleError(error: BookError?) {
        if (error is BookError.UnknownError) {
            crashReport.reportWithCustomData(
                "Handle error in CurrentlyReadingViewModel",
                error.message ?: "Unknown error with empty message"
            )
        }
        state = state.copy(error = error)
    }

    private fun fetchBookProgress(bookId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.getBookProgress(bookId = bookId)

            if (result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(progressUpdated = true)
                bookData = result.data
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun updateBookProgress(bookId: String, page: Int? = null, percentage: Double? = null) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.updateBookProgress(
                bookId = bookId,
                percentage = percentage,
                page = page,
                finished = false,
                giveUp = false,
            )

            if (result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(progressUpdated = true)
                bookData = result.data
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun finishBook(bookId: String, rate: Float, rateText: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.finishBook(
                bookId = bookId,
                rateNumber = rate,
                rateText = rateText
            )

            if (result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(finished = true)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun giveUpBook(bookId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.giveUpWithBook(
                bookId = bookId
            )

            if (result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(gaveUp = true)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun getBookDataFromDB(bookId: String) {
        viewModelScope.launch {
            if (bookData == null) {
                state = state.copy(isLoading = true)
                val result = bookRepo.getBookWithProgressByIdFromDB(bookId)

                if (result.error != null) {
                    handleError(result.error)
                } else {
                    handleError(null)
                    state = state.copy(progressUpdated = true)
                    bookData = result.data
                    getCommentsInPage(
                        bookId = result.data?.book?.id,
                        page = result.data?.bookProgress?.page,
                        percentage = result.data?.bookProgress?.percentage,
                        totalPages = result.data?.book?.totalPages ?: 0
                    )
                }

                state = state.copy(isLoading = false)
            } else {
                getCommentsInPage(
                    bookId = bookData?.book?.id,
                    page = bookData?.bookProgress?.page,
                    percentage = bookData?.bookProgress?.percentage,
                    totalPages = bookData?.book?.totalPages ?: 0
                )
            }
        }
    }

    private fun getCommentsInPage(
        bookId: String?,
        page: Int?,
        percentage: Double?,
        totalPages: Int
    ) {
        viewModelScope.launch {
            if (bookId != null) {
                val comments = commentsRepo.getCommentsInPageOrPercentage(
                    bookId = bookId,
                    page = page,
                    totalPages = totalPages,
                    percentage = percentage
                )

                state = state.copy(commentsInPage = comments.data?.size ?: 0)
            }
        }
    }

    /**
     * Function to avoid a weird behaviour in the navigation when going back to home
     */
    private fun setVariablesToFalse() {
        state = state.copy(gaveUp = false, finished = false, isLoading = false, error = null)
    }
}
