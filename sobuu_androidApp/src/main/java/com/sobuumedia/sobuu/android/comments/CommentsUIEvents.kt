package com.sobuumedia.sobuu.android.comments

import com.sobuumedia.sobuu.models.bo_models.ReportReason

enum class CommentFilterType {
    ONLY_COMMENT_PEOPLE_I_KNOW,
    ONLY_COMMENTS_WITHOUT_SPOILERS,
    ONLY_COMMENTS_LAST_WEEK,
    ONLY_COMMENTS_LAST_MONTH,
}

sealed class CommentsUIEvents {
    data class FilterByType(val type: CommentFilterType): CommentsUIEvents()
    data class NewCommentTextChanged(val value: String): CommentsUIEvents()
    data class SpoilerSwitchChanged(val value: Boolean): CommentsUIEvents()
    data class IncreaseVote(val commentId: String): CommentsUIEvents()
    data class DecreaseVote(val commentId: String): CommentsUIEvents()
    data class DisplayReportReasonsDialog(val commentId: String): CommentsUIEvents()
    data class ReportComment(val commentId: String, val reportReason: ReportReason): CommentsUIEvents()
    data class DisplayCommentsScreen(val bookId: String): CommentsUIEvents()
    data class CreateNewComment(val bookId: String, val totalPages: Int,
                                val page: Int? = null, val percentage: Double? = null,
                                val hasSpoilers: Boolean, val commentText: String,
                                val parentCommentId: String?): CommentsUIEvents()
    data class GetBookData(val bookId: String): CommentsUIEvents()
    object DisplayFilterScreen: CommentsUIEvents()
    object CloseResultDialog: CommentsUIEvents()
}
