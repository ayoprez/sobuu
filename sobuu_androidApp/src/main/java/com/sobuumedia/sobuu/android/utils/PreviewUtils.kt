package com.sobuumedia.sobuu.android.utils

import android.content.res.Configuration
import androidx.compose.material3.ColorScheme
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.sobuumedia.sobuu.android.settings.theme.DarkColorScheme
import com.sobuumedia.sobuu.android.settings.theme.LightColorScheme

@Preview(name = "Dark Mode", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "Light Mode", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
annotation class ThemePreviews

@Preview(
    name = "Landscape Mode",
    showBackground = true,
    device = Devices.AUTOMOTIVE_1024p,
    widthDp = 640
)
@Preview(name = "Portrait Mode", showBackground = true, device = Devices.PIXEL_4)
annotation class OrientationPreviews

@Preview(name = "Small Phone Mode", showBackground = true, device = Devices.PIXEL_7_PRO)
@Preview(name = "Big Phone Mode", showBackground = true, device = Devices.PIXEL_2)
annotation class PhoneSizePreviews

@Preview(name = "Small Tablet Mode", showBackground = true, device = Devices.PIXEL_FOLD)
@Preview(name = "Big Tablet Mode", showBackground = true, device = Devices.PIXEL_TABLET)
annotation class TabletSizePreviews

@Preview(name = "Default Font Size", fontScale = 1f)
@Preview(name = "Large Font Size", fontScale = 1.5f)
@Preview(name = "Small Tablet Mode", fontScale = 0.5f)
annotation class FontScalePreviews

@Preview(name = "Spanish", locale = "es")
@Preview(name = "English", locale = "en")
@Preview(name = "German", locale = "de")
annotation class LanguagesPreviews

class TextPreviewProvider : PreviewParameterProvider<String> {
    override val values = sequenceOf(
        "Short text",
        "A bit longer text",
        "A text that long that can break some designs",
        "A very long text that should be covered by some of the screens, but obviously not all of them!"
    )
}

class ThemePreviewProvider : PreviewParameterProvider<ColorScheme> {
    override val values = sequenceOf(LightColorScheme, DarkColorScheme)
}




