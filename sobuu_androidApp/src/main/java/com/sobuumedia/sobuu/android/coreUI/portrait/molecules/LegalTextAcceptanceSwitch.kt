package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_registration_privacySwitch
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.AcceptanceLegalTextWithLinks
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun LegalTextAcceptanceSwitch(
    modifier: Modifier = Modifier,
    navigateToTermsText: () -> Unit,
    navigateToPrivacyText: () -> Unit,
    privacyPolicyConfirmationSwitchValue: Boolean,
    onPrivacyPolicyConfirmationSwitchValueChange: (Boolean) -> Unit
) {
    val uncheckedBackgroundColor = MaterialTheme.colorScheme.secondary
    val uncheckedThumbColor = MaterialTheme.colorScheme.tertiary
    val checkedBackgroundColor = MaterialTheme.colorScheme.error
    val checkedThumbColor = MaterialTheme.colorScheme.background

    Row(
        modifier = Modifier.fillMaxWidth().then(modifier),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Switch(
            modifier = Modifier.semantics { this.contentDescription = contentDescription_registration_privacySwitch.stringResource() },
            colors = SwitchDefaults.colors(
                uncheckedTrackColor = uncheckedBackgroundColor,
                uncheckedBorderColor = uncheckedBackgroundColor,
                uncheckedIconColor = uncheckedThumbColor,
                uncheckedThumbColor = uncheckedThumbColor,
                checkedTrackColor = checkedBackgroundColor,
                checkedBorderColor = checkedBackgroundColor,
                checkedIconColor = checkedThumbColor,
                checkedThumbColor = checkedThumbColor,
            ),
            checked = privacyPolicyConfirmationSwitchValue,
            onCheckedChange = onPrivacyPolicyConfirmationSwitchValueChange
        )
        AcceptanceLegalTextWithLinks(
            modifier = Modifier
                .padding(start = 15.dp, top = 5.dp, bottom = 5.dp, end = 5.dp)
                .fillMaxWidth(0.7f),
            navigateToTerms = navigateToTermsText,
            navigateToPrivacy = navigateToPrivacyText
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, widthDp = 500)
@Composable
fun LegalTextAcceptanceSwitchOffPreview() {
    SobuuAuthTheme {
        LegalTextAcceptanceSwitch(
            navigateToTermsText = {},
            navigateToPrivacyText = {},
            privacyPolicyConfirmationSwitchValue = false,
            onPrivacyPolicyConfirmationSwitchValueChange = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LegalTextAcceptanceSwitchOnPreview() {
    SobuuAuthTheme {
        LegalTextAcceptanceSwitch(
            navigateToTermsText = {},
            navigateToPrivacyText = {},
            privacyPolicyConfirmationSwitchValue = true,
            onPrivacyPolicyConfirmationSwitchValueChange = {}
        )
    }
}