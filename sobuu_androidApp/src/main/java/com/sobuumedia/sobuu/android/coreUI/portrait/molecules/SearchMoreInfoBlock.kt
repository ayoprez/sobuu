package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchMoreInfoBlock(
    modifier: Modifier = Modifier,
    navigateToAddBookScreen: (Boolean) -> Unit,
    displayMoreInfo: Boolean,
    displaySearchFurther: Boolean,
    displayWarningWithEmail: Boolean
) {
    val context = LocalContext.current
    val savedOnClipboard = remember { mutableStateOf(false) }

    if (displayMoreInfo) {
        Column(modifier = modifier) {
            if (savedOnClipboard.value) {
                Toast.makeText(
                    context,
                    SharedRes.strings.search_main_emailSavedInClipboard.stringResource(context = context),
                    Toast.LENGTH_LONG
                ).show()
            }

            if (displaySearchFurther) {
                SearchFurtherBlock(
                    navigateToAddBookScreenByISBN = { navigateToAddBookScreen(false) }
                )
            }

            if (displayWarningWithEmail) {
                SearchResultSendEmailOrAddManuallyBlock(
                    navigateToAddBookScreen = { navigateToAddBookScreen(true) },
                    savedOnClipboard = { savedOnClipboard.value = it }
                )
            }
        }
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchMoreInfoBlockPreview() {
    SobuuTheme {
        SearchMoreInfoBlock(
            navigateToAddBookScreen = {},
            displayMoreInfo = true,
            displaySearchFurther = true,
            displayWarningWithEmail = true
        )
    }
}