package com.sobuumedia.sobuu.android.profile

import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_processingQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unauthorizedQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.profile_error_invalidId
import com.sobuumedia.sobuu.SharedRes.strings.profile_error_user
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.ProfileTopBar
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.profile.ProfileView
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import org.koin.androidx.compose.koinViewModel

@Composable
fun ProfileScreen(
    navigateBackOrHome: () -> Unit,
    navigateToLoginScreen: () -> Unit,
    navigateToSettingsScreen: () -> Unit,
    viewModel: ProfileViewModel = koinViewModel(),
    logs: SobuuLogs
) {
    val state = viewModel.state

    if(state.logout) {
        navigateToLoginScreen()
    }

    if(state.editMode) {
        logs.info("ProfileScreen", "Edit mode is on")
    }

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                ProfileTopBar(
                    navigateBackOrHome = navigateBackOrHome,
                    logout = { viewModel.onEvent(ProfileUIEvent.logout) },
                    navigateToSettingsScreen = { navigateToSettingsScreen() },
                    editUserData = { viewModel.onEvent(ProfileUIEvent.ChangeEditMode(!state.editMode)) },
                    isEditMode = state.editMode,
                    saveData = { viewModel.onEvent(ProfileUIEvent.saveEditedUserData)  }
                )
            },
            content = { padding ->
                ProfileView(
                    padding = padding,
                    isLoading = state.isLoading,
                    hasError = state.error != null,
                    errorText = getStringFromProfileError(state.error),
                    userFirstname = state.profileInfo?.firstName,
                    firstNameOnValueChange = {viewModel.onEvent(ProfileUIEvent.FirstNameChanged(it))},
                    userLastname = state.profileInfo?.lastName,
                    lastNameOnValueChange = {viewModel.onEvent(ProfileUIEvent.LastNameChanged(it))},
                    username = state.profileInfo?.username,
                    isEditMode = state.editMode
                )
            }
        )
    }
}

@Composable
private fun getStringFromProfileError(error: ProfileError?): String {
    val context = LocalContext.current
    if(error == null) return ""
    return when (error) {
        ProfileError.InvalidProfileIdError -> profile_error_invalidId.stringResource(context = context)
        ProfileError.InvalidSessionTokenError -> general_errors_invalidSessionToken.stringResource(context = context)
        ProfileError.ProcessingQueryError -> general_errors_processingQuery.stringResource(context = context)
        ProfileError.TimeOutError -> general_errors_timeout.stringResource(context = context)
        ProfileError.UnauthorizedQueryError -> general_errors_unauthorizedQuery.stringResource(context = context)
        ProfileError.GetUserProfileError -> profile_error_user.stringResource(context = context)
        else -> general_errors_unknown.stringResource(context = context)
    }
}