package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createAccount
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun CreateAccountButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    isEnabled: Boolean
) {
    RoundRedButton(
        modifier = modifier,
        onClick = onClick,
        isEnabled = isEnabled,
        text = authorization_auth_createAccount.stringResource(context = LocalContext.current)
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CreateAccountButtonPreview() {
    CreateAccountButton(
        onClick = {},
        isEnabled = true
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CreateAccountButtonDisabledPreview() {
    CreateAccountButton(
        onClick = {},
        isEnabled = false
    )
}