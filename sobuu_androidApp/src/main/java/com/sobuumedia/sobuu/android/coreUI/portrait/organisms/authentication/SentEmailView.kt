package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.CrossButtonIcon
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SentEmailView(
    navToLoginScreen: () -> Unit,
    emailType: EmailType,
) {
    val backgroundColor = MaterialTheme.colorScheme.primary
    val textColor = MaterialTheme.colorScheme.background
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.email_tailwind))
    val progress by animateLottieCompositionAsState(composition)
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        CrossButtonIcon(
            modifier = Modifier
                .align(Alignment.End)
                .padding(15.dp),
            onButtonClick = navToLoginScreen
        )

        Spacer(modifier = Modifier.weight(0.3f))

        LottieAnimation(
            modifier = Modifier
                .width(150.dp)
                .height(150.dp),
            composition = composition,
            progress = { progress },
        )

        Text(
            text = if(emailType == EmailType.VERIFICATION)
                SharedRes.strings.authorization_auth_verificationEmail.stringResource(context = context)
            else
                SharedRes.strings.authorization_auth_resetPasswordEmail.stringResource(context = context),
            style = Typography.bodyLarge.copy(color = textColor),
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(32.dp)
        )

        Spacer(modifier = Modifier.weight(0.7f))
    }
}


@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun SentEmailViewPreview() {
    SobuuAuthTheme {
        SentEmailView(
            navToLoginScreen = {},
            emailType = EmailType.RESET_PASSWORD
        )
    }
}

@Preview(showSystemUi = true, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SentEmailViewDarkPreview() {
    SobuuAuthTheme {
        SentEmailView(
            navToLoginScreen = {},
            emailType = EmailType.VERIFICATION
        )
    }
}