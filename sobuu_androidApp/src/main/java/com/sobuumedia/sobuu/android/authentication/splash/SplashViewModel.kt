package com.sobuumedia.sobuu.android.authentication.splash

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class SplashViewModel(
    private val auth: IAuthenticationRepository,
    private val settings: ISettingsRepository,
    private val connectivityObserver: ConnectivityObserver
): ViewModel() {

    var state by mutableStateOf(SplashState())

    private val resultChannel = Channel<AuthenticationResult<Unit>>()
    val authResult = resultChannel.receiveAsFlow()

    init {
        checkConnectivity()
        displayOnBoarding()
        authentication()
    }

    fun handleError(error: AuthenticationError?) {
        state = state.copy(error = error)
    }

    private fun displayOnBoarding() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val displayedConsent = settings.displayedConsentScreen()
            val displayedFeature = settings.displayedFeatureScreen()
            val displayedChangelog = BuildConfig.VERSION_CODE == settings.getStoredVersionCode()
            state = state.copy(
                isLoading = false,
                displayOnBoardingConsent = !displayedConsent,
                displayOnBoardingFeature = !displayedFeature,
                displayOnBoardingNewVersion = !displayedChangelog
            )
        }
    }

    private fun authentication() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.authenticate()
            resultChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun checkConnectivity() {
        viewModelScope.launch {
            val isConnected = connectivityObserver.isConnected()

            state = state.copy(
                disconnected = !isConnected
            )
        }
    }
}