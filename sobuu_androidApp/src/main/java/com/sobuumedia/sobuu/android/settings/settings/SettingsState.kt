package com.sobuumedia.sobuu.android.settings.settings

import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions

data class SettingsState(
    val isLoading: Boolean = false,
    val error: String? = null,
    val displayThemeDialog: Boolean = false,
    val displayAnalyticsExplanationDialog: Boolean = false,
    var currentAnalyticsEnabled: Boolean = false,
    var currentCrashReportsEnabled: Boolean = false,
    val currentTheme: ThemeOptions = ThemeOptions.LIGHT,
    val displayFeature: Boolean = false,
    val displayNewVersionScreen: Boolean = false,
    val isOpenedFromSettings: Boolean = false,
    val currentVersionName: String = ""
)