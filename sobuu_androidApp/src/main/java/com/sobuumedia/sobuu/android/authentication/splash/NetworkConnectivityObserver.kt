package com.sobuumedia.sobuu.android.authentication.splash

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*

class NetworkConnectivityObserver(
    context: Context
) : ConnectivityObserver {

    private val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun observe(): Flow<ConnectivityObserver.Status> {
        return callbackFlow {
            val callback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    launch { send(ConnectivityObserver.Status.Available) }
                }

                override fun onLosing(network: Network, maxMsToLive: Int) {
                    super.onLosing(network, maxMsToLive)
                    launch { send(ConnectivityObserver.Status.Losing) }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    launch { send(ConnectivityObserver.Status.Lost) }
                }

                override fun onUnavailable() {
                    super.onUnavailable()
                    launch { send(ConnectivityObserver.Status.Unavailable) }
                }
            }

            connectivityManager.registerDefaultNetworkCallback(callback)
            awaitClose {
                connectivityManager.unregisterNetworkCallback(callback)
            }
        }.distinctUntilChanged()
    }

    override fun isConnected(): Boolean {
        val networkCapabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)

        return networkCapabilities != null
    }

    /*
    // TODO Finish the concept
    private suspend fun pingWebToCheckConnectivity(): Boolean {
        return withContext(Dispatchers.IO) {
                try {
                    val url = URL("https://www.google.com")
                    val urlc: HttpURLConnection = url.openConnection() as HttpURLConnection
                    urlc.setRequestProperty(
                        "User-Agent",
                        "Android Application: ${BuildConfig.VERSION_CODE}"
                    )
                    urlc.setRequestProperty("Connection", "close")
                    urlc.connectTimeout = 1000 * 10 // Timeout is in seconds
                    urlc.connect()
                    urlc.responseCode == 200
                } catch (e1: MalformedURLException) {
                    false
                } catch (e: IOException) {
                    false
                } catch (e: TimeoutException) {
                    false
                } catch (e: TimeoutCancellationException) {
                    false
                }
        }
    }
    */
}