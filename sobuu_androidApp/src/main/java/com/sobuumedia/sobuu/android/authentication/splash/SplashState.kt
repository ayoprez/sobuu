package com.sobuumedia.sobuu.android.authentication.splash

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError

data class SplashState (
    val isLoading: Boolean = false,
    val error: AuthenticationError? = null,
    val disconnected: Boolean = false,
    val displayOnBoardingConsent: Boolean = false,
    val displayOnBoardingFeature: Boolean = false,
    val displayOnBoardingNewVersion: Boolean = false
)