import co.touchlab.skie.configuration.DefaultArgumentInterop
import com.codingfeline.buildkonfig.compiler.FieldSpec.Type.STRING
import org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSetTree
import java.io.FileInputStream
import java.util.Properties

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version "2.0.21"
    id("com.android.library")
    id("com.codingfeline.buildkonfig")
    id("dev.icerock.mobile.multiplatform-resources")
    alias(libs.plugins.skie.plug)
    alias(libs.plugins.dev.ksp)
    alias(libs.plugins.ktorfit.plugin)
    alias(libs.plugins.kontest.plugin)
}

kotlin {
    // This is needed to run the Android app
    androidTarget {
        @OptIn(ExperimentalKotlinGradlePluginApi::class)
        instrumentedTestVariant {
            sourceSetTree.set(KotlinSourceSetTree.test)
        }
    }

    jvmToolchain(17)

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
            isStatic = true
        }
    }

    task("testClasses")

    sourceSets {
        commonMain.dependencies {
            // Database
            api(libs.room.runtime)
            api(libs.sqlite.bundled)

            // Others
            implementation(libs.kotlinx.datetime)
            // Logger
            implementation(libs.napier)

            // Ktor
            implementation(libs.kotlinx.serialization.json)
            implementation(libs.ktor.client.content.negotiation)
            implementation(libs.ktor.serialization.kotlinx.json)
            implementation(libs.ktor.client.logging)
            implementation(libs.ktorfit.lib)

            // Koin
            implementation(libs.koin.core)
            // This has been added temporally to avoid a problem starting iOS project
            implementation("co.touchlab:stately-common:2.0.6")
            implementation(libs.skie.configuration.annotations)

            // Coroutines
            implementation(libs.kotlinx.coroutines.core)

            // Share resources
            implementation(libs.moko.resources)
        }
        commonTest.dependencies {
            implementation(kotlin("test"))
            implementation(kotlin("test-common"))
            implementation(kotlin("test-annotations-common"))
            implementation(libs.mockative)
            implementation(libs.kotlinx.coroutines.test)
            implementation(libs.ktor.test.mock)
            implementation(libs.moko.test.resources)
        }
        androidMain.dependencies {
            implementation(libs.ktor.ktor.client.okhttp)
        }
        //val androidInstrumentedTest by getting
        iosMain.dependencies {
            implementation(libs.ktor.ktor.client.darwin)
        }

        getByName("androidMain") {
            kotlin.srcDir("build/generated/moko/androidMain/src")
        }
    }
}

dependencies {
    add("kspCommonMainMetadata", libs.ktorfit.ksp)
    add("kspAndroid", libs.ktorfit.ksp)
    add("kspIosArm64", libs.ktorfit.ksp)
    add("kspIosX64", libs.ktorfit.ksp)
    add("kspIosSimulatorArm64", libs.ktorfit.ksp)
    add("kspAndroid", libs.room.compiler)
    add("kspIosSimulatorArm64", libs.room.compiler)
    add("kspIosX64", libs.room.compiler)
    add("kspIosArm64", libs.room.compiler)

    configurations
        .filter { it.name.startsWith("ksp") && it.name.contains("Test") }
        .forEach {
            add(it.name, libs.mockative.processor)
        }
}

ksp {
    arg("mockative.stubsUnitByDefault", "false")
    arg("room.schemaLocation", "${projectDir}/schemas")
}

multiplatformResources {
    multiplatformResourcesPackage = "com.sobuumedia.sobuu"
    multiplatformResourcesClassName = "SharedRes"
}

skie {
    analytics {
        disableUpload.set(true)
        enabled.set(false)
    }
    features {
        group {
            DefaultArgumentInterop.Enabled(true)
        }
    }
}

buildkonfig {
    packageName = "com.sobuumedia.sobuu.shared"

    val properties =
        Properties().apply { load(FileInputStream(File(rootProject.rootDir, "local.properties"))) }

    defaultConfigs {
        buildConfigField(
            STRING,
            "API_KEY",
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im92ZGFreGp5amZxZWt5cWRzeHRxIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzE4NzgyNTIsImV4cCI6MTk4NzQ1NDI1Mn0.Kbx8OcqjB8VSNNZNXGbCTq_kh_PjvItRwJtQ1FD5qLo"
        )
        buildConfigField(
            STRING,
            "API_URL",
            "https://ovdakxjyjfqekyqdsxtq.functions.supabase.co/"
        )

        buildConfigField(
            STRING,
            "MATOMO_API_URL",
            "https://matomo.getsobuu.com/"
        )

        buildConfigField(
            STRING,
            "PLAUSIBLE_API_URL",
            "http://crash.getsobuu.com:6060/"
        )
    }
    defaultConfigs("prod") {
        buildConfigField(
            STRING,
            "API_KEY",
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImNpeHphaXpyeWppcWxoeW1rcG1mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzk1MDQ0NjksImV4cCI6MTk5NTA4MDQ2OX0.bD8kv7qwY-tTLgoBBQ5Bm0Kbdm9QsogrFvuMeRU8fNI"
        )
        buildConfigField(
            STRING,
            "API_URL",
            "https://cixzaizryjiqlhymkpmf.functions.supabase.co/"
        )
    }

    targetConfigs { // dev target to pass BuildConfig to iOS
        create("ios") {
            buildConfigField(
                STRING,
                "API_KEY",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im92ZGFreGp5amZxZWt5cWRzeHRxIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzE4NzgyNTIsImV4cCI6MTk4NzQ1NDI1Mn0.Kbx8OcqjB8VSNNZNXGbCTq_kh_PjvItRwJtQ1FD5qLo"
            )
            buildConfigField(
                STRING,
                "API_URL",
                "https://ovdakxjyjfqekyqdsxtq.functions.supabase.co/"
            )
        }
    }

    targetConfigs("prod") {
        create("android") {
            buildConfigField(
                STRING,
                "API_KEY",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImNpeHphaXpyeWppcWxoeW1rcG1mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzk1MDQ0NjksImV4cCI6MTk5NTA4MDQ2OX0.bD8kv7qwY-tTLgoBBQ5Bm0Kbdm9QsogrFvuMeRU8fNI"
            )
            buildConfigField(
                STRING,
                "API_URL",
                "https://cixzaizryjiqlhymkpmf.functions.supabase.co/"
            )
        }

        create("ios") {
            buildConfigField(
                STRING,
                "API_KEY",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImNpeHphaXpyeWppcWxoeW1rcG1mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Nzk1MDQ0NjksImV4cCI6MTk5NTA4MDQ2OX0.bD8kv7qwY-tTLgoBBQ5Bm0Kbdm9QsogrFvuMeRU8fNI"
            )
            buildConfigField(
                STRING,
                "API_URL",
                "https://cixzaizryjiqlhymkpmf.functions.supabase.co/"
            )
        }
    }
}

android {
    namespace = "com.sobuumedia.sobuu"
    compileSdk = 34
    defaultConfig {
        minSdk = 26
    }
    lint {
        baseline = file("lint-baseline.xml")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

tasks.configureEach {
    if (name == "kspKotlinIosSimulatorArm64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosSimulatorArm64Main"))
    }
    if (name == "kspKotlinIosX64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosX64Main"))
    }
    if (name == "kspKotlinIosArm64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosArm64Main"))
    }
}