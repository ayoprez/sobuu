package com.sobuumedia.sobuu.book

import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.remote.ISBN
import com.sobuumedia.sobuu.features.book.remote.SearchBookRequest
import com.sobuumedia.sobuu.features.book.remote.SearchByISBNRequest
import com.sobuumedia.sobuu.features.book.remote.SearchData
import com.sobuumedia.sobuu.models.api_models.SearchBookItemApi
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class SearchBookTest {
    @Mock
    private val api = mock(classOf<BookApi>())

    private lateinit var bookRemoteData: IBookRemoteData
    private val sessionToken = "w09wiohnkwe"
    private val bookTerm = "Test"
    private val bookISBN = "1324512652"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        bookRemoteData = BookRemoteDataImpl(api)
    }

    @Test
    fun tstGetBookSearchedWithFullData() = runTest {
        // Given
        val searchData = SearchData(term = bookTerm)
        val requestBody = SearchBookRequest(content = searchData)

        val serverResponse = """
            [
                {
                    "book": {
                        "id": "3115fece-5211-4c8c-83b7-921124b08a0d",
                        "title": "Der Test des App",
                        "authors": [
                            "Martin R. Clam"
                        ],
                        "picture_url": "https://foo.test/image",
                        "thumbnail_url": "https://foo.test/image",
                        "picture": null,
                        "genres": [
                            "Adventure",
                            "Fantasy",
                            "Fiction",
                            "High Fantasy"
                        ],
                        "publisher": "Test Editions",
                        "published_date": "12/7/2011",
                        "description": "Just a test",
                        "total_pages": 830,
                        "isbn10": "1376985425",
                        "isbn13": "9957645310410",
                        "serie": "A Serie of Tests",
                        "serie_number": -1,
                        "lang": "en",
                        "credits": {
                            "translators": [],
                            "illustrators": [],
                            "contributors": [],
                            "others": []
                        }
                    },
                    "totalComments": 0,
                    "peopleReadingIt": 5,
                    "totalReviews": 0,
                    "allReviews": [],
                    "userReview": null,
                    "averageRating": 0,
                    "readStatus": 0
                }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<SearchBookItemApi>>(serverResponse)

        coEvery { api.searchBook("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.searchBook(sessionToken, bookTerm, "en")

        // Then
        assertTrue { bookProgress.data != null }
        assertTrue { bookProgress.data?.get(0)?.title == "Der Test des App" }
        assertTrue { bookProgress.data?.get(0)?.peopleReadingIt == 5 }
        assertTrue { bookProgress.data?.get(0)?.lang == "en" }
        assertTrue { bookProgress.data?.get(0)?.readingStatus == BookReadingStatus.NOT_READ }
    }

    @Test
    fun testGetBookSearchedWithMissingData() = runTest {
        // Given
        val searchData = SearchData(term = bookTerm)
        val requestBody = SearchBookRequest(content = searchData)

        val serverResponse = """
            [
                {
                    "book": {
                        "id": "3115fece-5211-4c8c-83b7-921124b08a0d",
                        "title": "Der Test des App",
                        "authors": [ "Martin R. Clam" ],
                        "picture_url": "https://foo.test/image",
                        "thumbnail_url": "https://foo.test/image",
                        "picture": null,
                        "genres": [
                            "Adventure",
                            "Fantasy",
                            "Fiction",
                            "High Fantasy"
                        ],
                        "publisher": "Test Editions",
                        "published_date": null,
                        "description": "Just a test",
                        "total_pages": 830,
                        "isbn10": "1376985425",
                        "serie": null,
                        "serie_number": -1,
                        "lang": "en",
                        "credits": {
                            "translators": [],
                            "illustrators": [],
                            "contributors": [],
                            "others": []
                        }
                    },
                    "totalComments": 0,
                    "peopleReadingIt": 5,
                    "totalReviews": 0,
                    "allReviews": [],
                    "userReview": null,
                    "averageRating": 0,
                    "readStatus": 1
                }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<SearchBookItemApi>>(serverResponse)

        coEvery { api.searchBook("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.searchBook(sessionToken, bookTerm, "en")

        // Then
        assertTrue { bookProgress.data != null }
        assertTrue { bookProgress.data?.get(0)?.title == "Der Test des App" }
        assertTrue { bookProgress.data?.get(0)?.peopleReadingIt == 5 }
        assertTrue { bookProgress.data?.get(0)?.lang == "en" }
        assertTrue { bookProgress.data?.get(0)?.readingStatus == BookReadingStatus.READING }
    }

    @Test
    fun testGetBookByIsbnWithFullData() = runTest {
        // Given
        val searchData = ISBN(isbn = bookISBN)
        val requestBody = SearchByISBNRequest(content = searchData)

        val serverResponse = """[
	       {
            "book": {
                "id": "",
                "title": "Testing",
                "authors": [
                    "Test Vernteston"
                ],
                "picture_url": "https://foo.images",
                "thumbnail_url": "https://foo.images",
                "picture": null,
                "genres": [
                    "Science Fiction",
                    "Young Adult"
                ],
                "publisher": "Testing Publishing",
                "published_date": "21/11/2023",
                "description": "Just a simple test",
                "total_pages": "464",
                "credits": {
                    "translators": [
                        "Test Guy"
                    ],
                    "illustrators": [],
                    "contributors": [],
                    "others": [
                        ""
                    ]
                },
                "isbn10": "",
                "isbn13": "",
                "serie": "Testward",
                "serie_number": 4,
                "lang": "en"
            },
            "totalComments": 0,
            "peopleReadingIt": 0,
            "totalReviews": 0,
            "allReviews": [],
            "userReview": null,
            "averageRating": 0,
            "readStatus": 0
        }
        ]
        """.trimIndent()
        val response = Json.decodeFromString<List<SearchBookItemApi>>(serverResponse)

        coEvery { api.searchISBN("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.searchISBN(sessionToken, bookISBN)

        // Then
        assertTrue { bookProgress.data != null }
        assertTrue { bookProgress.data?.get(0)?.title == "Testing" }
        assertTrue { bookProgress.data?.get(0)?.peopleReadingIt == 0 }
        assertTrue { bookProgress.data?.get(0)?.lang == "en" }
        assertTrue { bookProgress.data?.get(0)?.readingStatus == BookReadingStatus.NOT_READ }
    }
}