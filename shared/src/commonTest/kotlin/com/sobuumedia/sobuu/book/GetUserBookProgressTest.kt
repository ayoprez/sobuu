package com.sobuumedia.sobuu.book

import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.remote.BookIdData
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.remote.UserBookProgressRequest
import com.sobuumedia.sobuu.models.api_models.ReadingBooksItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class GetUserBookProgressTest {
    @Mock
    private val api = mock(classOf<BookApi>())

    private lateinit var bookRemoteData: IBookRemoteData
    private val sessionToken = "w09wiohnkwe"
    private val bookId = "e5cac4d4-c619-4415-a734-c3cb5453117f"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        bookRemoteData = BookRemoteDataImpl(api)
    }

    @Test
    fun testGetUserBookProcessWithFullData() = runTest {
        // Given
        val bookIdData = BookIdData(bookId = bookId)
        val requestBody = UserBookProgressRequest(content = bookIdData)

        val serverResponse = """
            {
                "book": {
                    "id": "e5cac4d4-c619-4415-a734-c3cb5453117f",
                    "title": "Hombres buenos",
                    "authors": [
                        "Arturo Perez-Reverte"
                    ],
                    "picture_url": "https://m.media-amazon.com/images/I/51VVwnjOs-L.jpg",
                    "thumbnail_url": "https://m.media-amazon.com/images/I/51VVwnjOs-L.jpg",
                    "picture": null,
                    "genres": [
                        "Aventura",
                        "Ficción de época"
                    ],
                    "publisher": "Alfaguara",
                    "published_date": "12/03/2015",
                    "description": "A finales del siglo XVIII, cuando dos miembros de la Real Academia Española, el bibliotecario don Hermógenes Molina y el almirante don Pedro Zárate, recibieron de sus compañeros el encargo de viajar a París para conseguir de forma casi clandestina los 28 volúmenes de la Encyclopédie de D'Alembert y Diderot, que estaba prohibida en España, nadie podía sospechar que los dos académicos iban a enfrentarse a una peligrosa sucesión de intrigas, a un viaje de incertidumbres y sobresaltos que los llevaría, por caminos infestados de bandoleros e incómodas ventas y posadas, desde el Madrid ilustrado de Carlos III al París de los cafés, los salones, las tertulias filosóficas, la vida libertina y las agitaciones políticas en vísperas de la Revolución francesa.\n\nBasada en hechos y personajes reales, documentada con extremo rigor, conmovedora y fascinante en cada página, Hombres buenos narra la heroica aventura de quienes, orientados por las luces de la Razón, quisieron cambiar el mundo con libros cuando el futuro arrinconaba las viejas ideas y el ansia de libertad hacía tambalearse tronos y mundos establecidos.",
                    "total_pages": 479,
                    "isbn10": "8420403245",
                    "isbn13": "9788420403243",
                    "serie": null,
                    "serie_number": -1,
                    "lang": "es",
                    "credits": {
                        "translators": [],
                        "illustrators": [],
                        "contributors": [],
                        "others": []
                    }
                },
                "bookProgress": {
                    "id": "4cd73627-fd30-4967-b772-0965f72b6601",
                    "pageNumber": -1,
                    "percentage": 98,
                    "gaveup": false,
                    "finished": false,
                    "createdAt": "2023-02-18T13:44:15.384631+00:00"
                },
                "comments": []
            }
        """.trimIndent()
        val response = Json.decodeFromString<ReadingBooksItemApi>(serverResponse)

        coEvery { api.getBookProgress("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.getBookProgress(sessionToken, bookId)

        // Then
        assertTrue { bookProgress.data != null }
        assertTrue { bookProgress.data?.book?.title == "Hombres buenos" }
        assertTrue { bookProgress.data?.bookProgress?.percentage == 98.0 }
        assertTrue { bookProgress.data?.bookProgressComments?.size == 0 }
    }

    @Test
    fun testGetErrorIfBookIdIsEmpty() = runTest {
        // Given
        val bookIdData = BookIdData(bookId = "")
        val requestBody = UserBookProgressRequest(content = bookIdData)

        val serverResponse = """
            {
                "book": {
                    "id": "e5cac4d4-c619-4415-a734-c3cb5453117f",
                    "title": "Good Tests",
                    "authors": [
                        "Arthur T. Conan"
                    ],
                    "picture_url": "https://foo.test/image",
                    "thumbnail_url": "https://foo.test/image",
                    "picture": null,
                    "genres": [
                        "Adventure"
                    ],
                    "publisher": "Test Publication",
                    "published_date": "12/03/2015",
                    "description": "Just some tests",
                    "total_pages": 479,
                    "isbn10": "3690403741",
                    "isbn13": "9638420403258",
                    "serie": null,
                    "serie_number": -1,
                    "lang": "es",
                    "credits": {
                        "translators": [],
                        "illustrators": [],
                        "contributors": [],
                        "others": []
                    }
                },
                "bookProgress": {
                    "id": "4cd73627-fd30-4967-b772-0965f72b6601",
                    "pageNumber": -1,
                    "percentage": 98,
                    "gaveup": false,
                    "finished": false,
                    "createdAt": "2023-02-18T13:44:15.384631+00:00"
                },
                "comments": []
            }
        """.trimIndent()
        val response = Json.decodeFromString<ReadingBooksItemApi>(serverResponse)

        coEvery { api.getBookProgress("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.getBookProgress(sessionToken, "")

        // Then
        assertTrue { bookProgress.error != null }
        assertTrue { bookProgress.error is BookError.InvalidBookIdError }
    }

    @Test
    fun testGetUserBookProcessWithoutFullData() = runTest {
        // Given
        val bookIdData = BookIdData(bookId = bookId)
        val requestBody = UserBookProgressRequest(content = bookIdData)

        val serverResponse = """
            {
                "book": {
                    "id": "e5cac4d4-c619-4415-a734-c3cb5453117f",
                    "title": "Good Tests",
                    "authors": [
                        "Arthur T. Conan"
                    ],
                    "picture_url": "https://foo.test/image",
                    "thumbnail_url": "https://foo.test/image",
                    "picture": null,
                    "genres": [
                        "Adventure"
                    ],
                    "publisher": "Test Publication",
                    "published_date": "12/03/2015",
                    "description": "Just some tests",
                    "total_pages": 479,
                    "isbn10": "3690403741",
                    "isbn13": "9638420403258",
                    "serie": null,
                    "serie_number": -1,
                    "lang": "es",
                    "credits": {
                        "translators": [],
                        "illustrators": [],
                        "contributors": [],
                        "others": []
                    }
                },
                "bookProgress": {
                    "id": "4cd73627-fd30-4967-b772-0965f72b6601",
                    "pageNumber": -1,
                    "percentage": 98,
                    "gaveup": false,
                    "finished": false,
                    "createdAt": "2023-02-18T13:44:15.384631+00:00"
                },
                "comments": []
            }
        """.trimIndent()
        val response = Json.decodeFromString<ReadingBooksItemApi>(serverResponse)

        coEvery { api.getBookProgress("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val bookProgress = bookRemoteData.getBookProgress(sessionToken, bookId)

        // Then
        assertTrue { bookProgress.data != null }
        assertTrue { bookProgress.data?.book?.title == "Good Tests" }
        assertTrue { bookProgress.data?.bookProgress?.percentage == 98.0 }
        assertTrue { bookProgress.data?.bookProgressComments?.size == 0 }
    }
}