package com.sobuumedia.sobuu.book

import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.remote.UserGiveUpBooksRequest
import com.sobuumedia.sobuu.models.api_models.GiveUpBookItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class GetGiveUpBooksDataTest {

    @Mock
    private val api = mock(classOf<BookApi>())

    private lateinit var bookRemoteData: IBookRemoteData
    private val sessionToken = "w09wiohnkwe"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        bookRemoteData = BookRemoteDataImpl(api)
    }

    @Test
    fun testGetGiveUpBooksWhenTheUserHasNotGiveUpNothingYet() = runTest {
        // Given
        val requestBody = UserGiveUpBooksRequest()
        val response = emptyList<GiveUpBookItemApi>()

        coEvery { api.getUserGiveUpBooks("Bearer $sessionToken", requestBody) }
            .returnsMany(response)

        // When
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks(sessionToken)

        // Then
        assertTrue(giveUpBooks.data != null)
        assertTrue(giveUpBooks.data?.size == 0)
    }

    @Test
    fun testShouldGetInvalidSessionTokenErrorIfSessionTokenIsEmpty() = runTest {
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks("")
        assertTrue(giveUpBooks.error != null)
        assertTrue(giveUpBooks.error is BookError.InvalidSessionTokenError)
    }

    @Test
    fun testGetGiveUpBooksWithProperData() = runTest {
        // Given
        val serverResponse = """[
        	{
		"book": {
			"id": "41274df4-79b6-4610-a244-8e908e069d25",
			"title": "Morning Tests",
			"authors": [
				"Maria Tests"
			],
			"picture_url": "https://foo.test/image",
			"thumbnail_url": "https://foo.test/image",
			"picture": null,
			"genres": [
				"Fiction"
			],
			"publisher": "Test Editions",
			"published_date": "14/04/2021",
			"description": "Just some tests",
			"total_pages": 648,
			"isbn10": "3218241456",
			"isbn13": "7458408241665",
			"serie": "A Serie of Tests",
			"serie_number": 2,
			"lang": "en"
		},
		"pageNumber": -1,
		"percentage": 18,
		"createdAt": "2023-02-18T14:04:47.88141+00:00",
		"updatedAt": "2023-02-19T14:04:47+00:00"
	},
	{
		"book": {
			"id": "5f2c7b67-20c1-4fc2-b5ad-cea5e9eeb97e",
			"title": "Talented Tests",
			"authors": [
				"Patricia Testing"
			],
			"picture_url": "https://foo.test/image",
			"thumbnail_url": "https://foo.test/image",
			"picture": null,
			"genres": [
				"Spies"
			],
			"publisher": "Testing Editions",
			"published_date": "01/12/2001",
			"description": "Just some tests",
			"total_pages": 288,
			"isbn10": "3259592487",
			"isbn13": "4778439592641",
			"serie": "Test of the Lord",
			"serie_number": 1,
			"lang": "en"
		},
		"pageNumber": -1,
		"percentage": 20,
		"createdAt": "2022-12-26T13:01:11.30523+00:00",
		"updatedAt": "2022-12-26T13:01:11.30523+00:00"
	}]
    """.trimIndent()
        val response = Json.decodeFromString<List<GiveUpBookItemApi>>(serverResponse)

        coEvery { api.getUserGiveUpBooks("Bearer $sessionToken", UserGiveUpBooksRequest()) }
            .returns(response)

        // When
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks(sessionToken)

        // Then
        assertTrue { giveUpBooks.data != null }
        assertTrue { giveUpBooks.data?.size == 2 }
        assertTrue { giveUpBooks.data?.get(0)?.book?.title == "Morning Tests" }
        assertTrue { giveUpBooks.data?.get(0)?.bookProgress?.finished == false }
        assertTrue { giveUpBooks.data?.get(0)?.bookProgress?.giveUp == true }
    }

    @Test
    fun tstShouldGetErrorIfRequestTypeDoesNotExistsAndResponseIsAnError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "ApiRouteNotFoundError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserGiveUpBooks("Bearer $sessionToken", UserGiveUpBooksRequest()) }
            .throws(errorResponse)

        // When
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks(sessionToken)

        // Then
        assertTrue(giveUpBooks.error != null)
        assertTrue(giveUpBooks.error is BookError.ApiRouteNotFoundError)
    }

    @Test
    fun testShouldGetGetUserGiveUpBooksErrorIfTheRequestReturnThatError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "GetUserGiveUpBooksError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserGiveUpBooks("Bearer $sessionToken", UserGiveUpBooksRequest()) }
            .throws(errorResponse)

        // When
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks(sessionToken)

        // Then
        assertTrue(giveUpBooks.error != null)
        assertTrue(giveUpBooks.error is BookError.GetUserGiveUpBooksError)
    }

    @Test
    fun testShouldGetUnknownErrorIfTheRequestReturnAnErrorThatIsNotExpected() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "TestError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserGiveUpBooks("Bearer $sessionToken", UserGiveUpBooksRequest()) }
            .throws(errorResponse)

        // When
        val giveUpBooks = bookRemoteData.getUserGiveUpBooks(sessionToken)

        // Then
        assertTrue(giveUpBooks.error != null)
        assertTrue(giveUpBooks.error is BookError.UnknownError)
    }
}