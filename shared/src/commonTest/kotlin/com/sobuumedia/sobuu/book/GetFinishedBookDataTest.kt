package com.sobuumedia.sobuu.book

import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.remote.UserFinishedBooksRequest
import com.sobuumedia.sobuu.models.api_models.FinishedBookItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.LocalTime
import kotlinx.datetime.Month
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class GetFinishedBookDataTest {

    @Mock
    private val api = mock(classOf<BookApi>())

    private lateinit var bookRemoteData: IBookRemoteData
    private val sessionToken = "w09wiohnkwe"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        bookRemoteData = BookRemoteDataImpl(api)
    }

    @Test
    fun testGetFinishedBooksWhenTheUserHasNotFinishedNothingYet() = runTest {
        // Given
        val requestBody = UserFinishedBooksRequest()
        val response = emptyList<FinishedBookItemApi>()

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", requestBody) }
            .returnsMany(response)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        assertTrue(finishedBooks.data != null)
        assertTrue(finishedBooks.data?.size == 0)
    }

    @Test
    fun testShouldGetInvalidSessionTokenErrorIfSessionTokenIsEmpty() = runTest {
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks("")
        assertTrue(finishedBooks.error != null)
        assertTrue(finishedBooks.error is BookError.InvalidSessionTokenError)
    }

    @Test
    fun testGetFinishedBooksWithProperData() = runTest {
        // Given
        val serverResponse = """
            [
                {  
                    "book": {
                        "id": "00e977cb-cf66-4a6e-adec-39671e739544",
                        "title": "The test over the cliff",
                        "authors": [
                            "Anton Smith"
                        ],
                        "picture_url": "https://foo.test/image",
                        "thumbnail_url": "https://foo.test/image",
                        "picture": null,
                        "genres": [
                            "Fiction"
                        ],
                        "publisher": "Test Editions",
                        "published_date": "2010-07-15",
                        "description": "Just some tests",
                        "total_pages": 672,
                        "isbn10": "784534447X",
                        "isbn13": "971534445003X",
                        "serie": null,
                        "serie_number": -1,
                        "lang": "en",
                        "credits": {
                            "translators": [],
                            "illustrators": [],
                            "contributors": [],
                            "others": []
                        }
                    },
                    "pageNumber": -1,
                    "percentage": 100,
                    "createdAt": "2023-04-23T16:39:26.865449+00:00",
                    "updatedAt": "2023-04-23T16:48:59.664+00:00",
                    "rating": {
                        "id": "4a61f60f-c0d9-4d08-8d93-80a705a74788",
                        "rating": 5,
                        "createdAt": "2023-04-23T16:48:59.888Z",
                        "user": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Joen Testen"
                        }
                    }
	            }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<FinishedBookItemApi>>(serverResponse)

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", UserFinishedBooksRequest()) }
            .returns(response)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        assertTrue { finishedBooks.data != null }
        assertTrue { finishedBooks.data?.size == 1 }
        assertTrue { finishedBooks.data?.get(0)?.bookWithProgress?.book?.title == "The test over the cliff" }
        assertTrue { finishedBooks.data?.get(0)?.bookWithProgress?.bookProgress?.finished == true }
        assertTrue { finishedBooks.data?.get(0)?.userRating?.rating == 5.0 }
    }

    @Test
    fun testRatingCreationDateUsingCurrentSystemDefaultTimeZone() = runTest {
        // Given
        val serverResponse = """
            [
                {  
                    "book": {
                        "id": "00e977cb-cf66-4a6e-adec-39671e739544",
                        "title": "The test over the cliff",
                        "authors": [
                            "Anton Smith"
                        ],
                        "picture_url": "https://foo.test/image",
                        "thumbnail_url": "https://foo.test/image",
                        "picture": null,
                        "genres": [
                            "Fiction"
                        ],
                        "publisher": "Test Editions",
                        "published_date": "2010-07-15",
                        "description": "Just some tests",
                        "total_pages": 672,
                        "isbn10": "784534447X",
                        "isbn13": "971534445003X",
                        "serie": null,
                        "serie_number": -1,
                        "lang": "en",
                        "credits": {
                            "translators": [],
                            "illustrators": [],
                            "contributors": [],
                            "others": []
                        }
                    },
                    "pageNumber": -1,
                    "percentage": 100,
                    "createdAt": "2023-04-23T16:39:26.865449+00:00",
                    "updatedAt": "2023-04-23T16:48:59.664+00:00",
                    "rating": {
                        "id": "4a61f60f-c0d9-4d08-8d93-80a705a74788",
                        "rating": 5,
                        "createdAt": "2023-04-23T16:48:59.888Z",
                        "user": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Joen Testen"
                        }
                    }
	            }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<FinishedBookItemApi>>(serverResponse)

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", UserFinishedBooksRequest()) }
            .returns(response)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        val expectedDate = LocalDateTime(
            date = LocalDate(year = 2023, month = Month.APRIL, dayOfMonth = 23),
            time = LocalTime(hour = 18, minute = 48, second = 59, nanosecond = 888000000)
        )
        assertTrue { finishedBooks.data != null }
        assertTrue { finishedBooks.data?.get(0)?.userRating?.date == expectedDate }
    }

    @Test
    fun testShouldGetErrorIfRequestTypeDoesNotExistsAndResponseIsAnError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "ApiRouteNotFoundError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", UserFinishedBooksRequest()) }
            .throws(errorResponse)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        assertTrue(finishedBooks.error != null)
        assertTrue(finishedBooks.error is BookError.ApiRouteNotFoundError)
    }

    @Test
    fun testShouldGetGetUserFinishedBooksErrorIfTheRequestReturnThatError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "GetUserFinishedBooksError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", UserFinishedBooksRequest()) }
            .throws(errorResponse)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        assertTrue(finishedBooks.error != null)
        assertTrue(finishedBooks.error is BookError.GetUserFinishedBooksError)
    }

    @Test
    fun testShouldGetUnknownErrorIfTheRequestReturnAnErrorThatIsNotExpected() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "TestError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserAlreadyReadBooks("Bearer $sessionToken", UserFinishedBooksRequest()) }
            .throws(errorResponse)

        // When
        val finishedBooks = bookRemoteData.getUserAlreadyReadBooks(sessionToken)

        // Then
        assertTrue(finishedBooks.error != null)
        assertTrue(finishedBooks.error is BookError.UnknownError)
    }

}