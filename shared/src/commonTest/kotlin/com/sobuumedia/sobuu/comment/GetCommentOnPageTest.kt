package com.sobuumedia.sobuu.comment

import com.sobuumedia.sobuu.features.comments.remote.BookCommentData
import com.sobuumedia.sobuu.features.comments.remote.BookCommentsInPageOrPercentageRequest
import com.sobuumedia.sobuu.features.comments.remote.CommentApi
import com.sobuumedia.sobuu.features.comments.remote.CommentError
import com.sobuumedia.sobuu.features.comments.remote.CommentRemoteDataImpl
import com.sobuumedia.sobuu.features.comments.remote.ICommentRemoteData
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.CommentItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class GetCommentOnPageTest {
    @Mock
    private val api = mock(classOf<CommentApi>())

    private lateinit var commentRemoteData: ICommentRemoteData
    private val sessionToken = "w09wiohnkwe"
    private val bookId = "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b"
    private val page = 67
    private val percentage = 9.63
    private val totalPages = 696

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        commentRemoteData = CommentRemoteDataImpl(api)
    }

    @Test
    fun testGetCommentsOnPageOrPercentageOnBooksWhenTheAreNoComments() = runTest {
        // Given
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            page = page
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)
        val response = emptyList<CommentItemApi>()

        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .returnsMany(response)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            page = page, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue(comments.data != null)
        assertTrue(comments.data?.size == 0)
    }

    @Test
    fun testShouldGetInvalidSessionTokenErrorIfSessionTokenIsEmpty() = runTest {
        val pageComments = commentRemoteData.getCommentsInPageOrPercentage("",
            page = page, bookTotalPages = totalPages, bookId = bookId)
        assertTrue(pageComments.error != null)
        assertTrue(pageComments.error is CommentError.InvalidSessionTokenError)
    }

    @Test
    fun testGetCommentsFromPageWithProperData() = runTest {
        // Given
        val serverResponse = """[
            {
                "id": "5894bc96-fef0-4a20-b771-3f433f86c752",
                "hasSpoilers": false,
                "commentText": "Aquí la rompió, pero en verdad tiene razón.",
                "percentage": 9.63,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2023-02-02T15:42:09.458397+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "JoenTon",
                    "firstname": "Joen",
                    "lastname": "Tristanian"
                }
            },
            {
                "id": "60cfaee9-0611-4416-b6a1-27487b691aa9",
                "hasSpoilers": false,
                "commentText": "Increíble lo que le ha dicho. Debería partirle la cara ahí mismo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T11:36:51.244821+00:00",
                "votes": 40,
                "profile": {
                    "id": "4c03181a-d8b0-4ab9-8443-477febae7d0e",
                    "username": "Aclammed",
                    "firstname": "Soul",
                    "lastname": "Free"
                }
            },
            {
                "id": "c429b243-a176-4829-8213-5aa7c6b66809",
                "hasSpoilers": false,
                "commentText": "La verdad es que, después de lo que le hizo en el 
                segundo libro, no me esperaba que Spensa hiciera esto a Caracapullo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T08:13:45.770236+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "CrossinAges",
                    "firstname": "Antoni",
                    "lastname": "Smith"
                }
            }
        ]
        """.trimIndent()
        val response = Json.decodeFromString<List<CommentItemApi>>(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            page = page
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)


        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            page = page, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue { comments.data != null }
        assertTrue { comments.data?.size == 3 }
        assertTrue { comments.data?.get(0)?.text == "Aquí la rompió, pero en verdad tiene razón." }
        assertTrue { comments.data?.get(1)?.hasSpoilers == false }
    }

    @Test
    fun testShouldGetErrorIfRequestTypeDoesNotExistsAndResponseIsAnError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "ApiRouteNotFoundError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            page = page
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)

        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .throws(errorResponse)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            page = page, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue(comments.error != null)
        assertTrue(comments.error is CommentError.ApiRouteNotFoundError)
    }

    @Test
    fun testShouldGetGetBookCommentsErrorIfTheRequestReturnThatError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "GetBookCommentsError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            page = page
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)


        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .throws(errorResponse)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            page = page, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue(comments.error != null)
        assertTrue(comments.error is CommentError.GetPageOrPercentageCommentsError)
    }

    @Test
    fun testShouldGetUnknownErrorIfTheRequestReturnAnErrorThatIsNotExpected() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "TestError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            page = page
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)

        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .throws(errorResponse)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            page = page, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue(comments.error != null)
        assertTrue(comments.error is CommentError.UnknownError)
    }

    @Test
    fun testShouldGetErrorIfNoPageNorPercentageIsIndicated() = runTest {
        // Given
        val serverResponse = """[
            {
                "id": "5894bc96-fef0-4a20-b771-3f433f86c752",
                "hasSpoilers": false,
                "commentText": "Aquí la rompió, pero en verdad tiene razón.",
                "percentage": 9.63,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2023-02-02T15:42:09.458397+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "JoenTon",
                    "firstname": "Joen",
                    "lastname": "Tristanian"
                }
            },
            {
                "id": "60cfaee9-0611-4416-b6a1-27487b691aa9",
                "hasSpoilers": false,
                "commentText": "Increíble lo que le ha dicho. Debería partirle la cara ahí mismo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T11:36:51.244821+00:00",
                "votes": 40,
                "profile": {
                    "id": "4c03181a-d8b0-4ab9-8443-477febae7d0e",
                    "username": "Aclammed",
                    "firstname": "Soul",
                    "lastname": "Free"
                }
            },
            {
                "id": "c429b243-a176-4829-8213-5aa7c6b66809",
                "hasSpoilers": false,
                "commentText": "La verdad es que, después de lo que le hizo en el 
                segundo libro, no me esperaba que Spensa hiciera esto a Caracapullo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T08:13:45.770236+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "CrossinAges",
                    "firstname": "Antoni",
                    "lastname": "Smith"
                }
            }
        ]
        """.trimIndent()
        val errorResponse = Exception(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)

        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .throws(errorResponse)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue(comments.error != null)
        assertTrue(comments.error is CommentError.InvalidPageNumberError)
    }

    @Test
    fun testGetCommentsFromPercentageWithProperData() = runTest {
        // Given
        val serverResponse = """[
            {
                "id": "5894bc96-fef0-4a20-b771-3f433f86c752",
                "hasSpoilers": false,
                "commentText": "Aquí la rompió, pero en verdad tiene razón.",
                "percentage": 9.63,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2023-02-02T15:42:09.458397+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "JoenTon",
                    "firstname": "Joen",
                    "lastname": "Tristanian"
                }
            },
            {
                "id": "60cfaee9-0611-4416-b6a1-27487b691aa9",
                "hasSpoilers": false,
                "commentText": "Increíble lo que le ha dicho. Debería partirle la cara ahí mismo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T11:36:51.244821+00:00",
                "votes": 40,
                "profile": {
                    "id": "4c03181a-d8b0-4ab9-8443-477febae7d0e",
                    "username": "Aclammed",
                    "firstname": "Soul",
                    "lastname": "Free"
                }
            },
            {
                "id": "c429b243-a176-4829-8213-5aa7c6b66809",
                "hasSpoilers": false,
                "commentText": "La verdad es que, después de lo que le hizo en el 
                segundo libro, no me esperaba que Spensa hiciera esto a Caracapullo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T08:13:45.770236+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "CrossinAges",
                    "firstname": "Antoni",
                    "lastname": "Smith"
                }
            }
        ]
        """.trimIndent()
        val response = Json.decodeFromString<List<CommentItemApi>>(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            percentage = percentage
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)


        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .returns(response)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            percentage = percentage, bookTotalPages = totalPages, bookId = bookId)

        // Then
        assertTrue { comments.data != null }
        assertTrue { comments.data?.size == 3 }
        assertTrue { comments.data?.get(0)?.text == "Aquí la rompió, pero en verdad tiene razón." }
        assertTrue { comments.data?.get(1)?.hasSpoilers == false }
    }

    @Test
    fun testShouldGetErrorIfThePassedPercentageIsMoreThan100() = runTest {
        // Given
        val serverResponse = """[
            {
                "id": "5894bc96-fef0-4a20-b771-3f433f86c752",
                "hasSpoilers": false,
                "commentText": "Aquí la rompió, pero en verdad tiene razón.",
                "percentage": 9.63,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2023-02-02T15:42:09.458397+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "JoenTon",
                    "firstname": "Joen",
                    "lastname": "Tristanian"
                }
            },
            {
                "id": "60cfaee9-0611-4416-b6a1-27487b691aa9",
                "hasSpoilers": false,
                "commentText": "Increíble lo que le ha dicho. Debería partirle la cara ahí mismo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T11:36:51.244821+00:00",
                "votes": 40,
                "profile": {
                    "id": "4c03181a-d8b0-4ab9-8443-477febae7d0e",
                    "username": "Aclammed",
                    "firstname": "Soul",
                    "lastname": "Free"
                }
            },
            {
                "id": "c429b243-a176-4829-8213-5aa7c6b66809",
                "hasSpoilers": false,
                "commentText": "La verdad es que, después de lo que le hizo en el 
                segundo libro, no me esperaba que Spensa hiciera esto a Caracapullo.",
                "pageNumber": 67,
                "bookID": "b969378f-0478-4a7b-b5cd-e5e7aa0ee29b",
                "createdAt": "2022-12-27T08:13:45.770236+00:00",
                "votes": 0,
                "profile": {
                    "id": "1d650563-1a8c-4634-96ab-683098d66321",
                    "username": "CrossinAges",
                    "firstname": "Antoni",
                    "lastname": "Smith"
                }
            }
        ]
        """.trimIndent()
        val errorResponse = Exception(serverResponse)
        val bookCommentData = BookCommentData(
            bookId = bookId,
            totalPages = totalPages,
            percentage = 103.0
        )
        val requestBody = BookCommentsInPageOrPercentageRequest(content = bookCommentData)

        coEvery { api.getCommentsFromPageOrPercentage("Bearer $sessionToken", requestBody) }
            .throws(errorResponse)

        // When
        val comments = commentRemoteData.getCommentsInPageOrPercentage(sessionToken,
            bookTotalPages = totalPages, bookId = bookId, percentage = 103.0)

        // Then
        assertTrue(comments.error != null)
        assertTrue(comments.error is CommentError.InvalidPercentageNumberError)
    }
}