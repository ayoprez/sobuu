package com.sobuumedia.sobuu

import dev.icerock.moko.resources.StringResource
import dev.icerock.moko.resources.desc.Resource
import dev.icerock.moko.resources.desc.StringDesc

expect class StringsHelper {
    fun get(id: StringResource, args: List<Any>): String
}

fun StringResource.get(): String {
    return StringDesc.Resource(this).toString()
}
