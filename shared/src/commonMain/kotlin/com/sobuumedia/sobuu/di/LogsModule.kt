package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs
import org.koin.dsl.module

val logsModule = module {
    single<SobuuLogs> { NapierLogs() }
}