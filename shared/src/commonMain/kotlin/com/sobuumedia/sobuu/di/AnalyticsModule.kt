package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.analytics.matomo_kit.remote.IMatomoRemoteData
import com.sobuumedia.sobuu.analytics.matomo_kit.remote.MatomoApi
import com.sobuumedia.sobuu.analytics.matomo_kit.remote.MatomoRemoteDataImpl
import com.sobuumedia.sobuu.analytics.plausible_kit.remote.IPlausibleRemoteData
import com.sobuumedia.sobuu.analytics.plausible_kit.remote.PlausibleApi
import com.sobuumedia.sobuu.analytics.plausible_kit.remote.PlausibleRemoteDataImpl
import com.sobuumedia.sobuu.shared.BuildKonfig
import de.jensklingenberg.ktorfit.Ktorfit
import io.ktor.client.HttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module

val analyticsModule = module {
    single(named("matomo_url")) { BuildKonfig.MATOMO_API_URL }
    single(named("plausible_url")) { BuildKonfig.PLAUSIBLE_API_URL }

    single(named("matomo")){
        Ktorfit.Builder()
            .httpClient(get<HttpClient>())
            .baseUrl(get(named("matomo_url")))// TODO Decouple to be able to use any analytics kit with any URL
    }

    single(named("plausible")){
        Ktorfit.Builder()
            .httpClient(get<HttpClient>())
            .baseUrl(get(named("plausible_url")))// TODO Decouple to be able to use any analytics kit with any URL
    }

    single { get<Ktorfit.Builder>(named("matomo")).build().create<MatomoApi>() }

    single { get<Ktorfit.Builder>(named("plausible")).build().create<PlausibleApi>() }

    single<IMatomoRemoteData> { MatomoRemoteDataImpl(get()) }

    single<IPlausibleRemoteData> { PlausibleRemoteDataImpl(get()) }
}