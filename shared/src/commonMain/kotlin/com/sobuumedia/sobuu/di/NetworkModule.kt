package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.initNapier
import com.sobuumedia.sobuu.shared.BuildKonfig
import de.jensklingenberg.ktorfit.Ktorfit
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.header
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import org.koin.core.qualifier.named
import org.koin.dsl.module

val networkModule = module {
    single { BuildKonfig.API_URL }

    single(named("main")) {
        Ktorfit.Builder()
            .httpClient(get<HttpClient>())
            .baseUrl(get())
    }

    single {
        HttpClient {
            expectSuccess = true
            install(Logging) {
                level = LogLevel.ALL
                logger = object : Logger {
                    override fun log(message: String) {
                        Napier.d(
                            tag = "HTTP Client",
                            message = message
                        )
                    }
                }
            }
            install(ContentNegotiation) {
                json(Json {
                    encodeDefaults = true
                    ignoreUnknownKeys = true
                    prettyPrint = true
                    isLenient = true
                })
            }
            install(HttpTimeout) {
                // This is done for the AddBook request, that can take until 2 minutes.
                // For a better implementation, add those numbers only in that request
                requestTimeoutMillis = 100000
                connectTimeoutMillis = 100000
                socketTimeoutMillis = 100000
            }
//            HttpResponseValidator {
//                validateResponse { response ->
//                    val statusCode: Int = response.status.value
//                    if (statusCode != 200) {
//                        throw Exception("Exception in Ktor: error status code $statusCode")
//                    }
//                }
//            }
            install(DefaultRequest) {
                url(get<String>())
                header("Content-Type", "application/json")
                // If I include here the Authorization header for the login it will
                // be mixed with the session token and make every request fail.
            }
        }.also {
            initNapier()
        }
    }
}