package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.features.settings.database.ISettingsLocalData
import com.sobuumedia.sobuu.features.settings.database.SettingsLocalDataImpl
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import com.sobuumedia.sobuu.features.settings.repository.SettingsRepositoryImpl
import org.koin.dsl.module

val settingsModule = module {
    single<ISettingsLocalData> { SettingsLocalDataImpl(get()) }

    single<ISettingsRepository> { SettingsRepositoryImpl(get()) }
}