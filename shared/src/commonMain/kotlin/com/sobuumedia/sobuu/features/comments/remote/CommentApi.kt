package com.sobuumedia.sobuu.features.comments.remote


import com.sobuumedia.sobuu.models.api_models.CommonItemApi.CommentItemApi
import de.jensklingenberg.ktorfit.http.Body
import de.jensklingenberg.ktorfit.http.Header
import de.jensklingenberg.ktorfit.http.POST

interface CommentApi {

    @POST("main")
    suspend fun addComment(
        @Header("Authorization") sessionToken: String,
        @Body data: AddNewCommentRequest,
    ): List<CommentItemApi>

/*
//    @FormUrlEncoded
//    @POST("functions/removeComment")
//    suspend fun removeComment(
//        @Field("sessionToken") sessionToken: String,
//        @Field("bookId") bookId: String,
//        @Field("commentId") commentId: String,
//    ): Response<Unit>
//
//    @FormUrlEncoded
//    @POST("functions/editComment")
//    suspend fun editComment(
//        @Field("sessionToken") sessionToken: String,
//        @Field("bookId") bookId: String,
//        @Field("commentId") commentId: String,
//        @Field("text") text: String,
//        @Field("hasSpoilers") hasSpoilers: Boolean,
//    ): Response<Comment>
 */

    @POST("main")
    suspend fun getCommentsFromPageOrPercentage(
        @Header("Authorization") sessionToken: String,
        @Body data: BookCommentsInPageOrPercentageRequest,
    ): List<CommentItemApi>

    @POST("main")
    suspend fun increaseVoteCounterComment(
        @Header("Authorization") sessionToken: String,
        @Body data: BookCommentIncreaseVoteRequest,
    ): List<CommentItemApi>

    @POST("main")
    suspend fun decreaseVoteCounterComment(
        @Header("Authorization") sessionToken: String,
        @Body data: BookCommentDecreaseVoteRequest,
    ): List<CommentItemApi>

    @POST("main")
    suspend fun reportComment(
        @Header("Authorization") sessionToken: String,
        @Body data: ReportCommentRequest,
    ): Boolean
}