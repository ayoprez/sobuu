package com.sobuumedia.sobuu.features.authentication.remote

import com.sobuumedia.sobuu.models.api_models.login.LoginResponse
import de.jensklingenberg.ktorfit.http.Body
import de.jensklingenberg.ktorfit.http.Header
import de.jensklingenberg.ktorfit.http.POST

interface AuthenticationApi {

    @POST("main")
    suspend fun getSessionToken(
        @Header("Authorization") apiKey: String,
        @Body data: LoginRequest
    ): LoginResponse

    @POST("main")
    suspend fun authenticate(
        @Header("Authorization") apiKey: String,
        @Body sessionToken: AuthenticateRequest
    ): String

    @POST("main")
    suspend fun refreshSession(
        @Header("Authorization") apiKey: String,
        @Body sessionToken: RefreshSessionRequest
    ): LoginResponse

    @POST("main")
    suspend fun signUp(
        @Header("Authorization") apiKey: String,
        @Body data: SignUpRequest
    ): Boolean

    @POST("main")
    suspend fun resetPassword(
        @Header("Authorization") apiKey: String,
        @Body data: ResetPasswordRequest
    )
}