package com.sobuumedia.sobuu.features.authentication.remote

import co.touchlab.skie.configuration.annotations.SuspendInterop
import com.sobuumedia.sobuu.shared.BuildKonfig
import kotlinx.serialization.Serializable

@Serializable
data class Credentials(val email: String, val password: String)

@Serializable
data class LoginRequest(val type: String = "LoginUser", val content: Credentials)

@Serializable
data class AccessTokens(val accessToken: String, val refreshToken: String)

@Serializable
data class AuthenticateRequest(val type: String = "Authenticate", val content: SessionTokenContent)

@Serializable
data class SessionTokenContent(val access_token: String)

// Refresh token
@Serializable
data class RefreshSessionRequest(
    val type: String = "RefreshToken",
    val content: RefreshTokenContent
)

@Serializable
data class RefreshTokenContent(val refresh_token: String)

// Reset Password
@Serializable
data class ResetPasswordRequest(
    val type: String = "ResetPassword",
    val content: ResetPasswordEmailData
)

@Serializable
data class ResetPasswordEmailData(val email: String)

// SignUp
@Serializable
data class SignUpRequest(val type: String = "RegisterUser", val content: SignUpUserData)

@Serializable
data class SignUpUserData(
    val firstName: String,
    val lastName: String,
    val username: String,
    val email: String,
    val password: String,
    val language: String
)

class AuthenticationRemoteDataImpl(
    private val authApi: AuthenticationApi
) : IAuthenticationRemoteData {

    @SuspendInterop.Enabled
    override suspend fun login(
        username: String,
        password: String
    ): AuthenticationResult<AccessTokens> {
        return try {
            if (username.isBlank() || password.isBlank()) {
                return AuthenticationResult.Error(AuthenticationError.EmptyCredentialsError)
            }

            val data = LoginRequest(content = Credentials(username, password))

            val response = authApi.getSessionToken(
                apiKey = "Bearer ${BuildKonfig.API_KEY}",
                data = data
            )

            val accessRefreshToken = AccessTokens(
                accessToken = response.session.access_token,
                refreshToken = response.session.refresh_token
            )

            AuthenticationResult.Authorized(accessRefreshToken)
        } catch (e: Exception) {
            errorBodyHandle(e)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun register(
        username: String,
        email: String,
        password: String,
        firstname: String,
        lastname: String,
        language: String
    ): AuthenticationResult<Unit> {
        return try {
            if (username.isBlank() || password.isBlank() || email.isBlank() || firstname.isBlank() || lastname.isBlank()) {
                return AuthenticationResult.Error(AuthenticationError.EmptyCredentialsError)
            }

            if (!email.contains('@')) {
                return AuthenticationResult.Error(AuthenticationError.WrongEmailFormatError)
            }

            if (password.length < 6) {
                return AuthenticationResult.Error(AuthenticationError.PasswordTooShort)
            }

            val response = authApi.signUp(
                apiKey = "Bearer ${BuildKonfig.API_KEY}",
                data = SignUpRequest(
                    content = SignUpUserData(
                        username = username,
                        email = email,
                        password = password,
                        firstName = firstname,
                        lastName = lastname,
                        language = language
                    )
                )
            )

            if (response) {
                AuthenticationResult.Registered()
            } else {
                AuthenticationResult.Error(AuthenticationError.UnknownError)
            }
        } catch (e: Exception) {
            errorBodyHandle(e)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun authenticate(sessionToken: String?): AuthenticationResult<Unit> {
        return try {
            if (sessionToken.isNullOrBlank()) {
                return AuthenticationResult.Unauthorized()
            }

            authApi.authenticate(
                apiKey = "Bearer ${BuildKonfig.API_KEY}",
                sessionToken = AuthenticateRequest(
                    content = SessionTokenContent(sessionToken)
                )
            )

            AuthenticationResult.Authorized()
        } catch (e: Exception) {
            if (e.message?.contains("SessionTokenError") == true) {
                AuthenticationResult.Error(AuthenticationError.InvalidSessionToken)
            } else {
                AuthenticationResult.Error(AuthenticationError.UnknownError)
            }

//            when(e.code()) {
//                401 -> {
//                    AuthenticationResult.Unauthorized()
//                }
//                209 -> {
//                    AuthenticationResult.Error(AuthenticationError.InvalidSessionToken)
//                }
//                else -> {
//                    AuthenticationResult.Error(AuthenticationError.UnknownError)
//                }
//            }
        }
    }

    @SuspendInterop.Enabled
    override suspend fun refreshSession(refreshToken: String?): AuthenticationResult<AccessTokens> {
        return try {
            if (refreshToken.isNullOrBlank()) {
                return AuthenticationResult.Unauthorized()
            }

            val result = authApi.refreshSession(
                apiKey = "Bearer ${BuildKonfig.API_KEY}",
                sessionToken = RefreshSessionRequest(
                    content = RefreshTokenContent(refreshToken)
                )
            )

            AuthenticationResult.Authorized(
                data = AccessTokens(
                    accessToken = result.session.access_token,
                    refreshToken = result.session.refresh_token
                )
            )
        } catch (e: Exception) {
            AuthenticationResult.Error(AuthenticationError.UnknownError)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun resetPassword(email: String?): AuthenticationResult<Unit> {
        return try {
            if (email.isNullOrBlank()) {
                return AuthenticationResult.Error(AuthenticationError.EmptyCredentialsError)
            }

            authApi.resetPassword(
                apiKey = "Bearer ${BuildKonfig.API_KEY}",
                data = ResetPasswordRequest(
                    content = ResetPasswordEmailData(
                        email = email
                    )
                )
            )

            AuthenticationResult.ResetPassword()
        } catch (e: Exception) {
            errorBodyHandle(e)
        }
    }

    @SuspendInterop.Enabled
    private fun <T> errorBodyHandle(errorBody: Exception): AuthenticationResult<T> {
        val response = errorBody.message
            ?: return AuthenticationResult.Error(AuthenticationError.UnknownError)

        return if (response.contains("LoginFalseCredentialsError")) {
            AuthenticationResult.Error(AuthenticationError.InvalidCredentials)
        } else if (response.contains("timeout")) {
            AuthenticationResult.Error(AuthenticationError.TimeOutError)
        } else if (response.contains("ResetPasswordError")) {
            AuthenticationResult.Error(AuthenticationError.ResetPassword)
        } else if (response.contains("UserEmailAlreadyTakenError")) {
            AuthenticationResult.Error(AuthenticationError.EmailAlreadyTaken)
        } else if (response.contains("UsernameAlreadyTakenError")) {
            AuthenticationResult.Error(AuthenticationError.UsernameAlreadyTaken)
        } else if (response.contains("RegisterUserError")) {
            AuthenticationResult.Error(AuthenticationError.RegistrationError)
        } else if (response.contains("WeakPasswordError")) {
            AuthenticationResult.Error(AuthenticationError.WeakPassword)
        } else {
            // TODO create more possible errors

            AuthenticationResult.Error(AuthenticationError.UnknownError)
        }

//        return JSONObject(response)
//            .get("code")
//            .let {
//                when(it) {
//                    101 -> AuthenticationResult.Error(AuthenticationError.InvalidCredentials)
//                    209 -> AuthenticationResult.Error(AuthenticationError.InvalidSessionToken)
//                    124 -> AuthenticationResult.Error(AuthenticationError.TimeOutError)
//                    125 -> AuthenticationResult.Error(AuthenticationError.InvalidEmailError)
//                    202 -> AuthenticationResult.Error(AuthenticationError.UsernameAlreadyTaken)
//                    203 -> AuthenticationResult.Error(AuthenticationError.EmailAlreadyTaken)
//                    else -> AuthenticationResult.Error(AuthenticationError.UnknownError)
//                }
//            }
    }
}