package com.sobuumedia.sobuu.features.book.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import kotlinx.serialization.Serializable


// Get Current Reading Books
@Serializable
data class UserReadingBooksRequest(val type: String = "GetUserCurrentReadingBooks")

// Search Book
@Serializable
data class SearchData(val term: String)

@Serializable
data class SearchBookRequest(val type: String = "SearchBook", val content: SearchData)

// Get Finished Books
@Serializable
data class UserFinishedBooksRequest(val type: String = "GetUserAlreadyReadBooks")

// Get Give Up Books
@Serializable
data class UserGiveUpBooksRequest(val type: String = "GetUserGiveUpBooks")

// Get Book Progress
@Serializable
data class BookIdData(val bookId: String)

// Update Book Progress
@Serializable
data class UserBookProgressRequest(
    val type: String = "GetUserBookProgress",
    val content: BookIdData
)

@Serializable
data class UpdateBookProgress(
    val bookId: String, val page: Int?, val percentage: Double?,
    val finished: Boolean = false, val giveUp: Boolean = false
)

@Serializable
data class UserBookUpdateProgressRequest(
    val type: String = "UpdateUserBookProgress",
    val content: UpdateBookProgress
)

// Get Book by Id
@Serializable
data class GetBookByIdRequest(
    val type: String = "GetBookById",
    val content: BookId
)

@Serializable
data class BookId(val id: String)

// Give Up Book
@Serializable
data class GiveUpBookRequest(
    val type: String = "GiveUpBook",
    val content: GiveUpBookData
)

@Serializable
data class GiveUpBookData(val bookId: String)

// Finish Book
@Serializable
data class FinishBookRequest(
    val type: String = "FinishBook",
    val content: FinishBookData
)

@Serializable
data class FinishBookData(val bookId: String, val rateNumber: Float, val rateText: String)

// Start to read book
@Serializable
data class StartToReadBookRequest(
    val type: String = "StartBook",
    val content: StartToReadBookData
)

@Serializable
data class StartToReadBookData(val bookId: String, val title: String, val isbn: String)

// Report book missing data
@Serializable
data class ReportMissingInBookDataRequest(
    val type: String = "MissingDataInBook",
    val content: ReportMissingInBookData
)

@Serializable
data class ReportMissingInBookData(val bookId: String, val title: String, val isbn: String)

// Search by ISBN
@Serializable
data class SearchByISBNRequest(
    val type: String = "AddBookByISBN",
    val content: ISBN
)

@Serializable
data class ISBN(
    val isbn: String,
    val isDebug: Boolean = false,
    val withBackup: Boolean = true
)

// Add book to Database
@Serializable
data class AddBookToDatabaseRequest(
    val type: String = "SaveBook",
    val content: BookData
)

@Serializable
data class BookData(
    val title: String,
    val authors: List<String>,
    val description: String,
    val picture_url: String,
    val thumbnail_url: String,
    val publisher: String,
    val credits: CommonItemApi.CreditsItemApi?,
    val total_pages: Int,
    val isbn10: String?,
    val isbn13: String?,
    val published_date: String,
    val genres: List<String>,
    val serie: String,
    val serie_number: Int,
    val language: String,
    val returnRequest: Boolean
)