package com.sobuumedia.sobuu.features.profile.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager

class ProfileLocalDataImpl(
    private val prefs: KMMStorage
): IProfileLocalData, SessionTokenManager(prefs) {

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun getRefreshToken(): String {
        return super.obtainRefreshToken()
    }

    override suspend fun setSessionToken(token: String?) {
        prefs.putString(Constants.SESSION_TOKEN_KEY, token)
    }

    override suspend fun setRefreshToken(token: String?) {
        prefs.putString(Constants.REFRESH_TOKEN_KEY, token)
    }
}