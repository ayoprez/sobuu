package com.sobuumedia.sobuu.features.comments.database

import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.db_models.CommentDB
import com.sobuumedia.sobuu.models.db_models.ProfileDB
import kotlinx.datetime.LocalDateTime

// region BO to DB
fun Comment.toCommentDB(bookId: String) : CommentDB {
    val commentDB = CommentDB()
    commentDB.bookId = bookId
    commentDB.commentId = this.id
    commentDB.parentCommentId = this.parentCommentId
    commentDB.percentage = this.percentage
    commentDB.hasSpoilers = this.hasSpoilers
    commentDB.pageNumber = this.pageNumber
    commentDB.text = this.text
    commentDB.votesCounter = this.votesCounter
    commentDB.publishedDate = this.publishedDate.toString()

    return commentDB
}

fun Profile.toProfileDB() : ProfileDB {
    val profileDB = ProfileDB()
    profileDB.profileId = this.id
    profileDB.email = this.email
    profileDB.firstName = this.firstName
    profileDB.lastName = this.lastName
    profileDB.username = this.username
    profileDB.createdAt = this.createdAt

    return profileDB
}

// endregion

// region DB to BO

fun CommentDB.toCommentBO() : Comment {
    return Comment(
        id = this.commentId,
        percentage = this.percentage,
        user = ProfileDB().toProfileBO(),
        parentCommentId = this.parentCommentId,
        hasSpoilers = this.hasSpoilers,
        pageNumber = this.pageNumber,
        publishedDate = LocalDateTime.parse(this.publishedDate),
        text = this.text,
        votesCounter = this.votesCounter
    )
}

fun ProfileDB?.toProfileBO() : Profile {
    return Profile(
        id = this?.profileId ?: "",
        firstName = this?.firstName ?: "",
        lastName = this?.lastName ?: "",
        username = this?.username ?: "",
        email = this?.email ?: "",
        createdAt = this?.createdAt ?: ""
    )
}

// endregion