package com.sobuumedia.sobuu.features.shelf.database

interface IShelfLocalData {

    suspend fun getSessionToken(): String?

}