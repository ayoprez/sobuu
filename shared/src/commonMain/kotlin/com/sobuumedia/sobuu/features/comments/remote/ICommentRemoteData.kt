package com.sobuumedia.sobuu.features.comments.remote

import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.ReportReason

interface ICommentRemoteData {

    suspend fun addComment(sessionToken: String?, bookId: String,
                           text: String?, page: Int?, percentage: Double?,
                           parentCommentId: String?, hasSpoilers: Boolean,
                           bookTotalPages: Int) : CommentResult<List<Comment>>
//
//    suspend fun removeComment(sessionToken: String?, bookId: String,
//                           commentId: String) : CommentResult<Unit>
//
//    suspend fun editComment(sessionToken: String?, bookId: String,
//                            commentId: String, text: String?,
//                            hasSpoilers: Boolean) : CommentResult<Comment>
//
    suspend fun getCommentsInPageOrPercentage(sessionToken: String?, bookId: String,
                                              page: Int? = null, percentage: Double? = null,
                                              bookTotalPages: Int) : CommentResult<List<Comment>>

    suspend fun increaseCommentVoteCounter(sessionToken: String?, commentId: String) : CommentResult<List<Comment>>

    suspend fun decreaseCommentVoteCounter(sessionToken: String?, commentId: String) : CommentResult<List<Comment>>

    suspend fun reportComment(sessionToken: String?, commentId: String, reason: ReportReason) : CommentResult<Boolean>
}