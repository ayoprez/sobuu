package com.sobuumedia.sobuu.features.authentication.repository

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult

interface IAuthenticationRepository {

    suspend fun loginUser(username: String, password: String): AuthenticationResult<Unit>

    suspend fun registerUser(username: String, email: String, password: String, firstname: String, lastname: String, language: String): AuthenticationResult<Unit>

    suspend fun authenticate(): AuthenticationResult<Unit>

    suspend fun resetPassword(email: String?): AuthenticationResult<Unit>

    suspend fun getSessionToken(username: String, password: String): String?

    suspend fun setSessionToken(token: String?)

    suspend fun setRefreshToken(token: String?)
}