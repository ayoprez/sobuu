package com.sobuumedia.sobuu.features.book.database

import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import com.sobuumedia.sobuu.models.db_models.BookDB
import com.sobuumedia.sobuu.models.db_models.BookProgressDB
import com.sobuumedia.sobuu.models.db_models.CreditsDB
import com.sobuumedia.sobuu.models.db_models.UserBookRatingDB
import kotlinx.datetime.LocalDateTime

// region BO to DB
fun Book.toBookDB(): BookDB {
    val bookDB = BookDB()
    bookDB.bookId = this.id
    bookDB.title = this.title
    bookDB.authors = this.authors.joinToString(",")
    bookDB.description = this.bookDescription
    bookDB.picture = this.picture
    bookDB.thumbnail = this.thumbnail
    bookDB.publisher = this.publisher
    bookDB.totalPages = this.totalPages
    bookDB.isbn10 = this.isbn.first
    bookDB.isbn13 = this.isbn.second
    bookDB.publishedDate = this.publishedDate
    bookDB.genres = this.genres.joinToString(",")
    bookDB.totalComments = this.totalComments
    bookDB.peopleReadingIt = this.peopleReadingIt
    bookDB.readingStatus = this.readingStatus.value
    bookDB.totalRating = this.totalRating
    bookDB.serie = this.serie
    bookDB.serieNumber = this.serieNumber
    bookDB.lang = this.lang

    return bookDB
}

fun CreditsBO.toCreditDB(bookId: String): CreditsDB {
    val creditsDB = CreditsDB()
    creditsDB.bookId = bookId
    creditsDB.translators = this.translators.toList()
    creditsDB.illustrators = this.illustrators.toList()
    creditsDB.contributors = this.contributors.toList()
    creditsDB.others = this.others.toList()

    return creditsDB
}

fun UserBookRating.toUserBookRatingDB(bookId: String): UserBookRatingDB {
    val userBookRatingDB = UserBookRatingDB()
    userBookRatingDB.bookId = bookId
    userBookRatingDB.userBookRatingId = this.id
    userBookRatingDB.rating = this.rating
    userBookRatingDB.date = this.date.toString()
    userBookRatingDB.review = this.review ?: ""

    return userBookRatingDB
}

fun BookProgress.toBookProgressDB(bookId: String): BookProgressDB {
    val progressDB = BookProgressDB()

    progressDB.bookId = bookId
    progressDB.progressId = this.id ?: ""
    progressDB.percentage = this.percentage
    progressDB.page = this.page
    progressDB.progressInPercentage = this.progressInPercentage
    progressDB.finished = this.finished
    progressDB.giveUp = this.giveUp
    progressDB.startedToRead = this.startedToRead.toString()
    progressDB.finishedToRead = if(this.finishedToRead == null) null else this.finishedToRead.toString()

    return progressDB
}

// endregion

// region DB to BO

fun BookDB.toBook(creditsDB: CreditsDB?, userRatingDB: UserBookRatingDB?, reviewsDB: List<UserBookRatingDB>): Book {
    return Book(
        id = this.bookId,
        title = this.title,
        authors = this.authors.split(","),
        bookDescription = this.description,
        picture = this.picture,
        thumbnail = this.thumbnail,
        publisher = this.publisher,
        totalPages = this.totalPages,
        isbn = Pair(this.isbn10, this.isbn13),
        publishedDate = this.publishedDate,
        genres = this.genres.split(","),
        totalComments = this.totalComments,
        peopleReadingIt = this.peopleReadingIt,
        readingStatus = this.readingStatus.toReadingStatus(),
        totalRating = this.totalRating,
        serie = this.serie,
        serieNumber = this.serieNumber,
        lang = this.lang,
        allReviews = reviewsDB.map { it.toUserBookRating() },
        credits = creditsDB.toCreditBO(),
        userRating = userRatingDB?.toUserBookRating()
    )
}

fun CreditsDB?.toCreditBO(): CreditsBO {
    return if(this == null) {
        CreditsBO(
            translators = emptyArray(),
            illustrators = emptyArray(),
            contributors = emptyArray(),
            others = emptyArray(),
        )
    } else {
        CreditsBO(
            translators = this.translators.toTypedArray(),
            illustrators = this.illustrators.toTypedArray(),
            contributors = this.contributors.toTypedArray(),
            others = this.others.toTypedArray(),
        )
    }
}

fun UserBookRatingDB.toUserBookRating(): UserBookRating {
    return UserBookRating(
        id = this.userBookRatingId,
        rating = this.rating,
        date = LocalDateTime.parse(this.date),
        review = this.review,
        user = null
    )
}

fun BookProgressDB.toBookProgress(): BookProgress {
    return BookProgress(
        id = this.progressId,
        percentage = this.percentage,
        page = this.page,
        progressInPercentage = this.progressInPercentage,
        finished = this.finished,
        giveUp = this.giveUp,
        startedToRead = LocalDateTime.parse(this.startedToRead),
        finishedToRead = if(this.finishedToRead == null) null else LocalDateTime.parse(this.finishedToRead!!)
    )
}

private fun Int.toReadingStatus(): BookReadingStatus {
    return when(this) {
        1 -> BookReadingStatus.READING
        2 -> BookReadingStatus.FINISHED
        3 -> BookReadingStatus.GIVE_UP
        else -> BookReadingStatus.NOT_READ
    }
}
// endregion