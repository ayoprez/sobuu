package com.sobuumedia.sobuu.features.settings.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager

const val ANALYTICS_CONSENT_KEY = "analyticsConsentKey"
const val CRASH_REPORTS_CONSENT_KEY = "crashReportsConsentKey"
internal const val DISPLAY_CONSENTS_KEY = "displayConsentsKey"
internal const val DISPLAY_FEATURES_KEY = "displayFeaturesKey"
internal const val VERSION_CODE_KEY = "versionCodeKey"

class SettingsLocalDataImpl(
    private val prefs: KMMStorage
): ISettingsLocalData, SessionTokenManager(prefs) {

    object SettingsConstants {
        const val THEME: String = "theme"
    }

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun getRefreshToken(): String {
        return super.obtainRefreshToken()
    }

    override suspend fun setSessionToken(token: String?) {
        prefs.putString(Constants.SESSION_TOKEN_KEY, token)
    }

    override suspend fun setRefreshToken(token: String?) {
        prefs.putString(Constants.REFRESH_TOKEN_KEY, token)
    }

    override suspend fun getThemeMode(): Int {
        return prefs.getInt(SettingsConstants.THEME)
    }

    override suspend fun setThemeMode(mode: Int) {
        prefs.putInt(SettingsConstants.THEME, mode)
    }

    override suspend fun getAnalyticsEnabled(): Boolean {
        return prefs.getBoolean(ANALYTICS_CONSENT_KEY)
    }

    override suspend fun setAnalyticsEnabled(enabled: Boolean) {
        prefs.putBoolean(ANALYTICS_CONSENT_KEY, enabled)
    }

    override suspend fun getCrashReportsEnabled(): Boolean {
        return prefs.getBoolean(CRASH_REPORTS_CONSENT_KEY)
    }

    override suspend fun setCrashReportsEnabled(enabled: Boolean) {
        prefs.putBoolean(CRASH_REPORTS_CONSENT_KEY, enabled)
    }

    override suspend fun displayedConsentScreen(): Boolean {
        return prefs.getBoolean(DISPLAY_CONSENTS_KEY)
    }

    override suspend fun displayedFeatureScreen(): Boolean {
        return prefs.getBoolean(DISPLAY_FEATURES_KEY)
    }

    override suspend fun setDisplayedConsentScreen(display: Boolean) {
        prefs.putBoolean(DISPLAY_CONSENTS_KEY, display)
    }

    override suspend fun setDisplayedFeatureScreen(display: Boolean) {
        prefs.putBoolean(DISPLAY_FEATURES_KEY, display)
    }

    override suspend fun storeVersionCode(versionCode: Int) {
        prefs.putInt(VERSION_CODE_KEY, versionCode)
    }

    override suspend fun getStoredVersionCode(): Int {
        return prefs.getInt(VERSION_CODE_KEY)
    }
}