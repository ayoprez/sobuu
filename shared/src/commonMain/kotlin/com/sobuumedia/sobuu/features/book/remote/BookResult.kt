package com.sobuumedia.sobuu.features.book.remote

import co.touchlab.skie.configuration.annotations.SealedInterop

@SealedInterop.Enabled
sealed class BookResult<T>(val data: T? = null, val error: BookError? = null) {
    class Success<T>(data: T? = null) : BookResult<T>(data = data)
    class Error<T>(error: BookError? = null) : BookResult<T>(error = error)
}

@SealedInterop.Enabled
sealed class BookError(val errorMessage: String? = null) {
    object UnauthorizedQueryError : BookError()
    object EmptyField : BookError()
    object InvalidSessionTokenError : BookError()
    object EmptySearchTermError : BookError()
    object InvalidBookIdError : BookError()
    object GetCurrentReadingBooksError : BookError()
    object GetUserFinishedBooksError : BookError()
    object GetUserGiveUpBooksError : BookError()
    object ApiRouteNotFoundError : BookError()
    object InvalidRateIdError : BookError()
    object InvalidISBNError : BookError()
    object SearchBookError : BookError()
    object AddBookByISBNError : BookError()
    object InvalidPageNumberError : BookError()
    object InvalidRateNumberError : BookError()
    object InvalidPercentageNumberError : BookError()
    object InvalidFinishedAndGiveUpBookValuesError : BookError()
    object InvalidDoubleValueError : BookError()
    object ProcessingQueryError : BookError()
    object TimeOutError : BookError()
    object MissingRequiredDataError : BookError()
    object DateIsFutureError : BookError()
    data class UnknownError(val message: String? = null) : BookError(errorMessage = message)
}