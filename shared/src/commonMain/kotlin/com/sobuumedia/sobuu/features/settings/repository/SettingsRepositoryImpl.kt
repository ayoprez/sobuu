package com.sobuumedia.sobuu.features.settings.repository

import co.touchlab.skie.configuration.annotations.EnumInterop
import co.touchlab.skie.configuration.annotations.SuspendInterop
import com.sobuumedia.sobuu.features.settings.database.ISettingsLocalData

@EnumInterop.Enabled
enum class ThemeOptions(val value: Int) {
    LIGHT(0), DARK(1), SYSTEM(2)
}

class SettingsRepositoryImpl(
    private val settingsLocalData: ISettingsLocalData
): ISettingsRepository {

    @SuspendInterop.Enabled
    override suspend fun getAppTheme(): ThemeOptions {
        return settingsLocalData.getThemeMode().toThemeOptions()
    }

    @SuspendInterop.Enabled
    override suspend fun setAppTheme(value: ThemeOptions) {
        settingsLocalData.setThemeMode(value.value)
    }

    @SuspendInterop.Enabled
    override suspend fun getAnalyticsEnabled(): Boolean {
        return settingsLocalData.getAnalyticsEnabled()
    }

    @SuspendInterop.Enabled
    override suspend fun setAnalyticsEnabled(value: Boolean) {
        settingsLocalData.setAnalyticsEnabled(value)
    }

    override suspend fun getCrashReportsEnabled(): Boolean {
        return settingsLocalData.getCrashReportsEnabled()
    }

    override suspend fun setCrashReportsEnabled(value: Boolean) {
        settingsLocalData.setCrashReportsEnabled(value)
    }

    override suspend fun displayedConsentScreen(): Boolean {
        return settingsLocalData.displayedFeatureScreen()
    }

    override suspend fun displayedFeatureScreen(): Boolean {
        return settingsLocalData.displayedFeatureScreen()
    }

    override suspend fun setDisplayedConsentScreen(display: Boolean) {
        settingsLocalData.setDisplayedConsentScreen(display)
    }

    override suspend fun setDisplayedFeatureScreen(display: Boolean) {
        settingsLocalData.setDisplayedFeatureScreen(display)
    }

    override suspend fun storeVersionCode(versionCode: Int) {
        settingsLocalData.storeVersionCode(versionCode)
    }

    override suspend fun getStoredVersionCode(): Int {
        return settingsLocalData.getStoredVersionCode()
    }

    private fun Int.toThemeOptions(): ThemeOptions {
        return when(this) {
            1 -> ThemeOptions.DARK
            2 -> ThemeOptions.SYSTEM
            else -> ThemeOptions.LIGHT
        }
    }
}