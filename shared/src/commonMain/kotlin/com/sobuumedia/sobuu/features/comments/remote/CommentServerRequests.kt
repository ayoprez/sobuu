package com.sobuumedia.sobuu.features.comments.remote

import kotlinx.serialization.Serializable


// Get Book Comments
@Serializable
data class BookCommentData(val bookId: String, val page: Int? = null, val percentage: Double? = null, val totalPages: Int)

@Serializable
data class BookCommentsInPageOrPercentageRequest(
    val type: String = "GetBookCommentsOnPageOrPercentage",
    val content: BookCommentData
)

// Add new Comment
@Serializable
data class AddNewCommentRequest(
    val type: String = "SaveNewComment",
    val content: NewCommentData
)

@Serializable
data class NewCommentData(val bookId: String, val commentText: String, val hasSpoilers: Boolean,
                          val pageNumber: Int?, val percentage: Double?, val parentCommentId: String?, val bookTotalPages: Int)

// Votes
@Serializable
data class BookCommentIncreaseVoteRequest(
    val type: String = "UpdateVote",
    val content: CommentIdData
)

@Serializable
data class BookCommentDecreaseVoteRequest(
    val type: String = "UpdateVote",
    val content: CommentIdData
)

@Serializable
data class CommentIdData(val commentId: String, val up: Boolean, val down: Boolean)

// Report Comment
@Serializable
data class ReportCommentRequest(
    val type: String = "ReportComment",
    val content: ReportCommentData
)

@Serializable
data class ReportCommentData(val commentId: String, val reason: String)
