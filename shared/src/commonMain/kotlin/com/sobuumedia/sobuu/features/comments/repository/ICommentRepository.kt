package com.sobuumedia.sobuu.features.comments.repository

import com.sobuumedia.sobuu.features.comments.remote.CommentResult
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.ReportReason


interface ICommentRepository {
    suspend fun addComment(bookId: String, text: String?, page: Int?,
                           percentage: Double?, parentCommentId: String?,
                           hasSpoilers: Boolean, bookTotalPages: Int) : CommentResult<List<Comment>>

//    suspend fun removeComment(bookId: String, commentId: String) : CommentResult<Unit>
//
//    suspend fun editComment(bookId: String, commentId: String, text: String?,
//                            hasSpoilers: Boolean) : CommentResult<Comment>

    suspend fun getBookInfoById(bookId: String): CommentResult<BookWithProgress>

    suspend fun getCommentsInPageOrPercentage(
        bookId: String,
        page: Int?,
        totalPages: Int,
        percentage: Double?) : CommentResult<List<Comment>>

    suspend fun increaseCommentVoteCounter(commentId: String) : CommentResult<List<Comment>>

    suspend fun decreaseCommentVoteCounter(commentId: String) : CommentResult<List<Comment>>

    suspend fun reportComment(commentId: String, reason: ReportReason) : CommentResult<Boolean>
}