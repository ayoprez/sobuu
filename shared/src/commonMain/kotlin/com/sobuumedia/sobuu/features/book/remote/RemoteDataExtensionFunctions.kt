package com.sobuumedia.sobuu.features.book.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.api_models.FinishedBookItemApi
import com.sobuumedia.sobuu.models.api_models.GiveUpBookItemApi
import com.sobuumedia.sobuu.models.api_models.ReadingBooksItemApi
import com.sobuumedia.sobuu.models.api_models.ReviewItemApi
import com.sobuumedia.sobuu.models.api_models.SearchBookItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

//Converters
fun List<ReadingBooksItemApi>.toCurrentReadingBookList(): List<BookWithProgress> {
    return this.map {
        it.toCurrentReadingBook()
    }
}

fun ReadingBooksItemApi.toCurrentReadingBook(): BookWithProgress {
    return BookWithProgress(
        book = this.book.toBook(),
        bookProgress = BookProgress(
            percentage = if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) -1.0 else if (this.bookProgress.percentage > -1.0) this.bookProgress.percentage else null,
            page = if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) -1 else if (this.bookProgress.pageNumber > -1) this.bookProgress.pageNumber else null,
            giveUp = false,
            finished = false,
            id = this.bookProgress.id,
            startedToRead = Instant.parse(this.bookProgress.createdAt)
                .toLocalDateTime(TimeZone.currentSystemDefault()),
            progressInPercentage = if (this.bookProgress.percentage > -1) this.bookProgress.percentage else if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) 0.0 else ((this.bookProgress.pageNumber * 100) / this.book.total_pages) * 1.0,
        ),
        bookProgressComments = this.comments?.toCommentList(),
    )
}

fun CommonItemApi.BookItemApi.toBook(): Book {
    return Book(
        id = this.id,
        title = this.title,
        authors = this.authors,
        bookDescription = this.description,
        picture = this.picture_url ?: "",
        thumbnail = this.thumbnail_url ?: "",
        publisher = this.publisher ?: "",
        credits = this.credits.convertToCreditsBO(),
        totalPages = this.total_pages,
        isbn = Pair(this.isbn10 ?: "", this.isbn13 ?: ""),
        publishedDate = this.published_date ?: "",
        genres = this.genres,
        lang = this.lang,
        serie = this.serie ?: "",
        serieNumber = this.serie_number,
        totalComments = 0,
        totalRating = 0.0,
        userRating = null,
        allReviews = emptyList(),
        peopleReadingIt = 0,
        readingStatus = BookReadingStatus.NOT_READ
    )
}

fun ReadingBooksItemApi.toBookWithProgress(): BookWithProgress {
    return BookWithProgress(
        book = this.book.toBook(),
        bookProgress = BookProgress(
            percentage = if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) -1.0 else if (this.bookProgress.percentage > -1.0) this.bookProgress.percentage else null,
            page = if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) -1 else if (this.bookProgress.pageNumber > -1) this.bookProgress.pageNumber else null,
            giveUp = false,
            finished = false,
            id = this.bookProgress.id,
            startedToRead = Instant.parse(this.bookProgress.createdAt)
                .toLocalDateTime(TimeZone.currentSystemDefault()),
            progressInPercentage = if (this.bookProgress.percentage > -1) this.bookProgress.percentage else if (this.bookProgress.percentage == -1.0 && this.bookProgress.pageNumber == -1) 0.0 else ((this.bookProgress.pageNumber * 100) / this.book.total_pages) * 1.0,
        ),
        bookProgressComments = this.comments?.toCommentList(),
    )
}

fun List<CommonItemApi.CommentItemApi>.toCommentList(): List<Comment> {
    return this.map {
        Comment(
            id = it.id,
            user = it.profile.toProfile(),
            publishedDate = Instant.parse(it.createdAt)
                .toLocalDateTime(TimeZone.currentSystemDefault()),
            text = it.commentText,
            hasSpoilers = it.hasSpoilers,
            votesCounter = it.votes,
            percentage = it.percentage,
            pageNumber = it.pageNumber,
            parentCommentId = it.parentCommentID
        )
    }
}

fun CommonItemApi.ProfileItemApi.toProfile(): Profile {
    return Profile(
        id = this.id,
        firstName = this.firstname ?: "",
        lastName = this.lastname ?: "",
        username = this.username,
        email = "", // Not needed yet
        createdAt = this.createdAt ?: ""
    )
}

fun List<FinishedBookItemApi>.toFinishedBookList(): List<FinishedReadingBook> {
    return this.map {
        FinishedReadingBook(
            bookWithProgress = BookWithProgress(
                book = it.book.toBook(),
                bookProgress = BookProgress(
                    percentage = if (it.percentage == -1.0 && it.pageNumber == -1) -1.0 else if ((it.percentage) > -1.0) it.percentage else null,
                    page = if (it.percentage == -1.0 && it.pageNumber == -1) -1 else if ((it.pageNumber) > -1
                    ) it.pageNumber else null,
                    giveUp = false,
                    finished = true,
                    startedToRead = Instant.parse(it.createdAt)
                        .toLocalDateTime(TimeZone.currentSystemDefault()),
                    finishedToRead = Instant.parse(it.updatedAt)
                        .toLocalDateTime(TimeZone.currentSystemDefault()),
                    progressInPercentage = if (it.percentage > -1.0) it.percentage else if (it.percentage == -1.0 && it.pageNumber == -1) 0.0 else ((it.pageNumber * 100) / it.book.total_pages) * 1.0,
                ),
            ),
            userRating = if (it.rating == null) null else UserBookRating(
                id = it.rating.id,
                user = it.rating.user?.toProfile(),
                rating = it.rating.rating ?: 0.0,
                review = it.rating.ratingText,
                date = it.rating.createdAt?.parseTimeWithZone() ?: Clock.System.now().toLocalDateTime(
                    TimeZone.currentSystemDefault())
            )
        )
    }
}

fun String.parseTimeWithZone(): LocalDateTime {
    return Instant.parse(this).toLocalDateTime(TimeZone.currentSystemDefault())
}

fun List<GiveUpBookItemApi>.toGiveUpBookList(): List<BookWithProgress> {
    return this.map {
        BookWithProgress(
            book = it.book.toBook(),
            bookProgress = BookProgress(
                percentage = if (it.percentage == -1.0 && it.pageNumber == -1) -1.0 else if (it.percentage > -1.0) it.percentage else null,
                page = if (it.percentage == -1.0 && it.pageNumber == -1) -1 else if (it.pageNumber > -1) it.pageNumber else null,
                progressInPercentage = if (it.percentage > -1.0) it.percentage else if (it.percentage == -1.0 && it.pageNumber == -1) 0.0 else ((it.pageNumber * 100) / it.book.total_pages) * 1.0,
                finished = false,
                giveUp = true,
                startedToRead = Instant.parse(it.createdAt)
                    .toLocalDateTime(TimeZone.currentSystemDefault()),
                finishedToRead = Instant.parse(it.updatedAt)
                    .toLocalDateTime(TimeZone.currentSystemDefault()),
            )
        )
    }
}

fun List<SearchBookItemApi>.toBookList(): List<Book> {
    return this.map { it.toBook() }
}

fun SearchBookItemApi.toBook(): Book {
    return Book(
        id = this.book.id,
        title = this.book.title,
        credits = this.book.credits.convertToCreditsBO(),
        totalRating = this.averageRating,
        peopleReadingIt = this.peopleReadingIt,
        allReviews = this.allReviews.toUserBookRatingList(),
        thumbnail = this.book.thumbnail_url ?: "",
        picture = this.book.picture_url ?: "",
        authors = this.book.authors,
        bookDescription = this.book.description,
        genres = this.book.genres,
        isbn = Pair(this.book.isbn10 ?: "", this.book.isbn13 ?: ""),
        publishedDate = this.book.published_date ?: "",
        publisher = this.book.publisher ?: "",
        totalComments = this.totalComments,
        totalPages = this.book.total_pages,
        userRating = this.userReview.toUserBookRating(),
        serie = this.book.serie ?: "",
        serieNumber = this.book.serie_number,
        lang = this.book.lang,
        readingStatus = when (this.readStatus) {
            1 -> BookReadingStatus.READING
            2 -> BookReadingStatus.FINISHED
            3 -> BookReadingStatus.GIVE_UP
            else -> {
                BookReadingStatus.NOT_READ
            }
        }
    )
}

fun CommonItemApi.CreditsItemApi?.convertToCreditsBO(): CreditsBO {
    return CreditsBO(
        translators = this?.translators?.toTypedArray() ?: emptyArray(),
        illustrators = this?.illustrators?.toTypedArray() ?: emptyArray(),
        contributors = this?.contributors?.toTypedArray() ?: emptyArray(),
        others = this?.others?.toTypedArray() ?: emptyArray()
    )
}

fun CreditsBO.convertToCreditsAPI(): CommonItemApi.CreditsItemApi {
    return CommonItemApi.CreditsItemApi(
        translators = this.translators.asList(),
        illustrators = this.illustrators.asList(),
        contributors = this.contributors.asList(),
        others = this.others.asList()
    )
}

fun List<ReviewItemApi>?.toUserBookRatingList(): ArrayList<UserBookRating> {
    val list = this?.mapNotNull {
        it.toUserBookRating()
    }

    return ArrayList(list ?: emptyList())
}

fun ReviewItemApi?.toUserBookRating(): UserBookRating? {
    return if (this == null) {
        null
    } else {
        UserBookRating(
            id = this.id,
            rating = this.rating,
            review = this.ratingText,
            date = if(this.createdAt == null) null else Instant.parse(this.createdAt)
                .toLocalDateTime(TimeZone.currentSystemDefault()),
            user = if(this.user?.id == null) null else Profile(
                id = this.user.id,
                firstName = this.user.firstname ?: "",
                lastName = this.user.lastname ?: "",
                username = this.user.username,
                email = "", // Here is not needed to display the email
                createdAt = this.user.createdAt ?: ""
            ),
        )
    }
}