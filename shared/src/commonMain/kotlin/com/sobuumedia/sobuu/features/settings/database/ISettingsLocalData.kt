package com.sobuumedia.sobuu.features.settings.database

interface ISettingsLocalData {
    suspend fun getSessionToken(): String

    suspend fun getRefreshToken(): String

    suspend fun setSessionToken(token: String?)

    suspend fun setRefreshToken(token: String?)

    suspend fun getThemeMode(): Int

    suspend fun setThemeMode(mode: Int)

    suspend fun getAnalyticsEnabled(): Boolean

    suspend fun setAnalyticsEnabled(enabled: Boolean)

    suspend fun getCrashReportsEnabled(): Boolean

    suspend fun setCrashReportsEnabled(enabled: Boolean)

    suspend fun displayedConsentScreen(): Boolean

    suspend fun displayedFeatureScreen(): Boolean

    suspend fun setDisplayedConsentScreen(display: Boolean)

    suspend fun setDisplayedFeatureScreen(display: Boolean)

    suspend fun storeVersionCode(versionCode: Int)

    suspend fun getStoredVersionCode(): Int
}