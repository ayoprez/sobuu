package com.sobuumedia.sobuu.features.authentication.remote

import co.touchlab.skie.configuration.annotations.SealedInterop

@SealedInterop.Enabled
sealed class AuthenticationResult<T> (val data: T? = null, val error: AuthenticationError? = null) {
    class Authorized<T>(data: T? = null) : AuthenticationResult<T>(data = data)
    class LoggedOut<T> : AuthenticationResult<T>()
    class ResetPassword<T> : AuthenticationResult<T>()
    class Unauthorized<T> : AuthenticationResult<T>()
    class Registered<T> : AuthenticationResult<T>()
    class Error<T>(error: AuthenticationError? = null): AuthenticationResult<T>(error = error)
}

@SealedInterop.Enabled
sealed class AuthenticationError {
    object InvalidSessionToken: AuthenticationError()
    object InvalidCredentials: AuthenticationError()
    object EmptyCredentialsError: AuthenticationError()
    object TimeOutError: AuthenticationError()
    object ResetPassword: AuthenticationError()
    object PasswordTooShort: AuthenticationError()
    object PasswordConfirmationDifferent: AuthenticationError()
    object InvalidEmailError: AuthenticationError()
    object UsernameAlreadyTaken: AuthenticationError()
    object WeakPassword: AuthenticationError()
    object EmailAlreadyTaken: AuthenticationError()
    object WrongEmailFormatError: AuthenticationError()
    object RegistrationError: AuthenticationError()
    object UnknownError: AuthenticationError()
}