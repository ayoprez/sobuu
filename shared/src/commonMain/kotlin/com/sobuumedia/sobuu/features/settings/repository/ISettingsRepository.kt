package com.sobuumedia.sobuu.features.settings.repository

interface ISettingsRepository {

    suspend fun getAppTheme(): ThemeOptions

    suspend fun setAppTheme(value: ThemeOptions)

    suspend fun getAnalyticsEnabled(): Boolean

    suspend fun setAnalyticsEnabled(value: Boolean)

    suspend fun getCrashReportsEnabled(): Boolean

    suspend fun setCrashReportsEnabled(value: Boolean)

    suspend fun displayedConsentScreen(): Boolean

    suspend fun displayedFeatureScreen(): Boolean

    suspend fun setDisplayedConsentScreen(display: Boolean)

    suspend fun setDisplayedFeatureScreen(display: Boolean)

    suspend fun storeVersionCode(versionCode: Int)

    suspend fun getStoredVersionCode(): Int
}