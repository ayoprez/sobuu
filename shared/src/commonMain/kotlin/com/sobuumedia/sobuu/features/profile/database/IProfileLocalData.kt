package com.sobuumedia.sobuu.features.profile.database

interface IProfileLocalData {

    suspend fun getSessionToken(): String

    suspend fun getRefreshToken(): String

    suspend fun setSessionToken(token: String?)

    suspend fun setRefreshToken(token: String?)
}