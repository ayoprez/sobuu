package com.sobuumedia.sobuu.analytics

/**
 * Holds some things for the Analytics class
 * @property isAnalyticsEnabled - no events will be sent if this is set to *false*
 * @property exceptionHandler - implementation of @ExceptionHandler
 */
open class AnalyticsSettings(val analyticsEnabled: Boolean)