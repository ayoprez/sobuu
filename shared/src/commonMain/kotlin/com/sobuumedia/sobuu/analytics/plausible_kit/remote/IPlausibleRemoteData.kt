package com.sobuumedia.sobuu.analytics.plausible_kit.remote

interface IPlausibleRemoteData {
    suspend fun sendAnalyticsData(actionName: String, resolution: String, operativeSystem: String,
                                  osVersion: String, appVersion: String, language: String,
                                  otherVariables: Map<String, String>)
}