package com.sobuumedia.sobuu.analytics

import com.sobuumedia.sobuu.analytics.events.base.Event
import com.sobuumedia.sobuu.analytics.exceptions.UnsupportedEventException

interface AnalyticsDispatcher {
    val init: Boolean
    val resolution: String
    val operativeSystem: String
    val osVersion: String
    val appVersion: String
    val language: String

    val kit: AnalyticsKit

    val dispatcherName: String

    /**
     * Should call the analytics library's initiation methods
     */
    fun initDispatcher()

    fun trackContentView(contentView: AnalyticsContentView)

    fun trackCustomEvent(event: AnalyticsEvent)

    /**
     * This method is called from the parent @Analytics class for each event.
     * Override this method if you plan on interfacing your own event types.
     */
    fun track(event: Event) {
        // track the event only if it is not configured as excluded
        if (event.isConsideredIncluded(kit)) {

            var handled = false

            // track for each type differently, including multiple implementations
            if (event is AnalyticsEvent) {
                trackCustomEvent(event)
                handled = true
            }

            if (event is AnalyticsContentView) {
                trackContentView(event)
                handled = true
            }

            if (!handled) {
                throw UnsupportedEventException(event)
            }

        }
    }
}

interface AnalyticsEvent : Event {
    fun getParameters(): Map<String, String> {
        // default empty map implementation
        return emptyMap()
    }

    fun getEventName(): String
}
interface AnalyticsContentView : Event {

    fun getViewName(): String
}