package com.sobuumedia.sobuu.analytics.plausible_kit

import com.sobuumedia.sobuu.analytics.AnalyticsKit

class PlausibleKit : AnalyticsKit {
    override val name: String = "Plausible Dispatcher"

    private object Holder {
        val INSTANCE = PlausibleKit()
    }

    companion object {
        val instance: PlausibleKit by lazy { Holder.INSTANCE }
    }
}
