package com.sobuumedia.sobuu.analytics

import com.sobuumedia.sobuu.analytics.events.base.Event
import com.sobuumedia.sobuu.analytics.exceptions.EventNotTrackedException
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

// Source and idea from: https://medium.com/@nadavfima/how-to-build-better-analytics-with-kotlin-60ab50ce25ac
class Analytics(private vararg val dispatchers: AnalyticsDispatcher): KoinComponent {

    private val settingsRepo : ISettingsRepository by inject()

    var exceptionHandler: ExceptionHandler? = null

    init {
        // init all dispatchers
        dispatchers.forEach { dispatcher ->
            if (dispatcher.init) {
                dispatcher.initDispatcher()
            }
        }
    }

    /**
     * Call this to track one or more *Events*
     */
    suspend fun track(vararg events: Event) {
        // set an analytics enabled / disabled via SharedPrefs, Database, or anything else
        val settings = AnalyticsSettings(settingsRepo.getAnalyticsEnabled())

        if (settings.analyticsEnabled.not()) return

        events.forEach { event ->

            dispatchers.forEach { dispatcher ->

                try {
                    dispatcher.track(event)
                } catch (e: Exception) {
                    exceptionHandler?.onException(EventNotTrackedException(dispatcher, event, e))
                }
            }
        }
    }

    /**
     * Just an exception callback to log/monitor exceptions,
     * thrown by the *Analytics* class or any of its dispatchers.
     */
    interface ExceptionHandler {
        fun onException(e: Exception)
    }
}