package com.sobuumedia.sobuu.analytics.matomo_kit.remote

import de.jensklingenberg.ktorfit.http.GET
import de.jensklingenberg.ktorfit.http.Query
import kotlinx.serialization.json.JsonObject

interface MatomoApi {

    @GET("matomo.php")
    suspend fun sendTrackingEvent(
        @Query("idsite") siteId: String = "1",
        @Query("rec") rec: String = "1",
        @Query("url") url: String = "app://sobuu",
        @Query("action_name") actionName: String,
        @Query("apiv") apiVersion: String = "1",
        @Query("res") resolution: String,
        @Query("_cvar") customVariables: JsonObject
    )
}

//@FieldMap Map<String, String> params
//