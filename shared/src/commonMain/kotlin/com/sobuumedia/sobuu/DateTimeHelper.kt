package com.sobuumedia.sobuu

import kotlinx.datetime.LocalDateTime

expect class DateTimeHelper<T> {
    fun toLocalDateTime(dateTime: T): LocalDateTime
    fun fromLocalDateTime(dateTime: LocalDateTime): T?
    fun toStringWithTime(dateTime: T): String
    fun toStringWithTime(dateTime: LocalDateTime): String
    fun fromStringWithTime(dateTime: String): T?
    fun fromStringWithTimeToLocalDateTime(dateTime: String): LocalDateTime
}