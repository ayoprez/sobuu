package com.sobuumedia.sobuu.core

import io.github.aakira.napier.Napier

interface SobuuLogs {
    fun error(tag: String, log: String, error: Throwable? = null)
    fun error(tag: String, log: String, error: Exception? = null)
    fun info(tag: String, log: String)
    fun debug(tag: String, log: String)
    fun warn(tag: String, log: String)
}

class NapierLogs: SobuuLogs {
    override fun error(tag: String, log: String, error: Throwable?) {
        Napier.e(
            tag = tag,
            message = log,
            throwable = error
        )
    }

    override fun error(tag: String, log: String, error: Exception?) {
        Napier.e(
            tag = tag,
            message = log,
            throwable = error
        )
    }

    override fun info(tag: String, log: String) {
        Napier.i(
            tag = tag,
            message = log
        )
    }

    override fun debug(tag: String, log: String) {
        Napier.d(
            tag = tag,
            message = log
        )
    }

    override fun warn(tag: String, log: String) {
        Napier.w(
            tag = tag,
            message = log
        )
    }
}