package com.sobuumedia.sobuu.core.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.sobuumedia.sobuu.models.db_models.CreditsDB

@Dao
interface CreditsDBApi : DBApi<CreditsDB> {

    @Insert
    override suspend fun saveData(data: CreditsDB)

    @Insert
    override suspend fun saveDataList(data: List<CreditsDB>)

    @Update
    override suspend fun updateData(data: CreditsDB)

    @Query("SELECT * FROM CreditsDB WHERE bookId LIKE :id")
    override suspend fun getData(id: String): CreditsDB?

    @Query("SELECT * FROM CreditsDB")
    override suspend fun getAllData(): List<CreditsDB>

    @Query("DELETE FROM CreditsDB")
    override suspend fun removeAll()

    @Query("DELETE FROM CreditsDB WHERE bookId = :id")
    override suspend fun removeById(id: String)

    @Delete
    override suspend fun remove(data: CreditsDB)
}