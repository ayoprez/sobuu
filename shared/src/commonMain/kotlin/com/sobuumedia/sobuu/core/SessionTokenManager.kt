package com.sobuumedia.sobuu.core

open class SessionTokenManager constructor(private val prefs: KMMStorage) {

    object Constants {
        const val SESSION_TOKEN_KEY = "authToken"
        const val REFRESH_TOKEN_KEY = "refreshToken"
    }

    fun obtainSessionToken(): String {
        return prefs.getString(Constants.SESSION_TOKEN_KEY)
    }

    fun obtainRefreshToken(): String {
        return prefs.getString(Constants.REFRESH_TOKEN_KEY)
    }
}
