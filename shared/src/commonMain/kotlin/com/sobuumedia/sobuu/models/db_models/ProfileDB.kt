package com.sobuumedia.sobuu.models.db_models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProfileDB(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var profileId: String = "",
    var firstName: String? = "",
    var lastName: String? = "",
    var username: String = "",
    var email: String = "",
    var createdAt: String = ""
)