package com.sobuumedia.sobuu.models.bo_models

import kotlinx.datetime.LocalDateTime


data class Comment(
    val id: String,
    val user: Profile,
    val publishedDate: LocalDateTime,
    val text: String,
    val hasSpoilers: Boolean,
    val votesCounter: Long,
    val percentage: Double?,
    val pageNumber: Int?,
    val parentCommentId: String?
)
