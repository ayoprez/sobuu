package com.sobuumedia.sobuu.models.db_models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class CommentDB(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var bookId: String = "",
    var commentId: String = "",
    var user: String? = null,
    var publishedDate: String = "",
    var text: String = "",
    var hasSpoilers: Boolean = true,
    var votesCounter: Long = 0,
    var percentage: Double? = null,
    var pageNumber: Int? = null,
    var parentCommentId: String? = null
)

data class CommentWithUserProfileDB(
    @Embedded val comment: CommentDB,
    @Relation(
        parentColumn = "user",
        entityColumn = "profileId"
    )
    val profile: ProfileDB?
)