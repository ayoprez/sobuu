package com.sobuumedia.sobuu.models.api_models.login

@kotlinx.serialization.Serializable
data class LoginResponse(
    val session: Session
)