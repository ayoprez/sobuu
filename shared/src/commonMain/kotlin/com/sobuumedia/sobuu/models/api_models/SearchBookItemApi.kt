package com.sobuumedia.sobuu.models.api_models

import kotlinx.serialization.Serializable

@Serializable
data class SearchBookItemApi (
    val book: CommonItemApi.BookItemApi,
    val totalComments: Int,
    val peopleReadingIt: Int,
    val totalReviews: Int,
    val allReviews: List<ReviewItemApi>,
    val userReview: ReviewItemApi? = null,
    val averageRating: Double,
    val readStatus: Int
)