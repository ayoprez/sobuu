package com.sobuumedia.sobuu.models.bo_models

import kotlinx.datetime.LocalDateTime

data class BookProgress(
    val id: String? = null,
    val percentage: Double?,
    val page: Int?,
    val progressInPercentage: Double,
    val finished: Boolean = false,
    val giveUp: Boolean = false,
    val startedToRead: LocalDateTime,
    val finishedToRead: LocalDateTime? = null,
)