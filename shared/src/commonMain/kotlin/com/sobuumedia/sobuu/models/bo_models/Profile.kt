package com.sobuumedia.sobuu.models.bo_models

data class Profile(
    val id: String,
//                   val giveUp: List<Book>,
//                   val alreadyRead: List<Book>,
    val firstName: String?,
    val lastName: String?,
    val username: String,
    val email: String,
    val createdAt: String
//                   val following: List<Profile>,
//                   val userShelves: List<Shelf>,
//                   val bookProgress: List<BookProgress>
)