package com.sobuumedia.sobuu.models.bo_models

data class BookWithProgress(
    val book: Book,
    val bookProgress: BookProgress,
    val bookProgressComments: List<Comment>? = null,
)