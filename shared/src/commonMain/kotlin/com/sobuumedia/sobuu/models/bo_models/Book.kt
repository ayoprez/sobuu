package com.sobuumedia.sobuu.models.bo_models

import co.touchlab.skie.configuration.annotations.EnumInterop

@EnumInterop.Enabled
enum class BookReadingStatus(val value: Int) {
    NOT_READ(0),
    READING(1),
    FINISHED(2),
    GIVE_UP(3)
}

data class Book(
    val id: String,
    val title: String,
    val authors: List<String>,
    val bookDescription: String,
    val picture: String,
    val thumbnail: String,
    val publisher: String,
    val credits: CreditsBO,
    val totalPages: Int,
    val isbn: Pair<String, String>,
    val publishedDate: String,
    val genres: List<String>,
    val totalComments: Int,
    val peopleReadingIt: Int,
    val readingStatus: BookReadingStatus,
    val allReviews: List<UserBookRating>,
    val userRating: UserBookRating?,
    val totalRating: Double,
    val serie: String,
    val serieNumber: Int,
    val lang: String,
)