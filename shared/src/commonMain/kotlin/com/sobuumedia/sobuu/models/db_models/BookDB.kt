package com.sobuumedia.sobuu.models.db_models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class BookDB (
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var bookId: String = "",
    var title: String = "",
    var authors: String = "", // Stored as comma separated string
    var description: String = "",
    var picture: String = "",
    var thumbnail: String = "",
    var publisher: String = "",
    var totalPages: Int = -1,
    var isbn10: String = "",
    var isbn13: String = "",
    var publishedDate: String = "",
    var genres: String = "",
    var totalComments: Int = -1,
    var peopleReadingIt: Int = -1,
    var readingStatus: Int = 0,
    var totalRating: Double = 0.0,
    var serie: String = "",
    var serieNumber: Int = -1,
    var lang: String = ""
)

data class BookWithCreditsDB(
    @Embedded val book: BookDB,
    @Relation(
        parentColumn = "bookId",
        entityColumn = "bookId"
    )
    val credits: CreditsDB?
)

data class BookWithUserRatingDB(
    @Embedded val book: BookDB,
    @Relation(
        parentColumn = "bookId",
        entityColumn = "bookId"
    )
    val userRating: UserBookRatingDB?,
)

data class BookWithAllReviewsDB(
    @Embedded val book: BookDB,
    @Relation(
        parentColumn = "bookId",
        entityColumn = "bookId"
    )
    val allReviews: List<UserBookRatingDB>
)