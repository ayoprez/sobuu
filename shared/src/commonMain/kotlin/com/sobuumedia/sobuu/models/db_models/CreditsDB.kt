package com.sobuumedia.sobuu.models.db_models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CreditsDB (
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var bookId: String = "",
    var contributors: List<String> = emptyList(),
    var illustrators: List<String> = emptyList(),
    var others: List<String> = emptyList(),
    var translators: List<String> = emptyList(),
)