package com.sobuumedia.sobuu

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase

fun getDatabaseBuilder(ctx: Application): RoomDatabase.Builder<SobuuDB> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath(dbFileName)
    return Room.databaseBuilder<SobuuDB>(
        context = appContext,
        name = dbFile.absolutePath
    )
}