package com.sobuumedia.sobuu

import com.sobuumedia.sobuu.SharedRes.strings.general_today
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toKotlinInstant
import kotlinx.datetime.toLocalDateTime
import kotlinx.datetime.toNSDateComponents
import platform.Foundation.NSCalendar
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSDate as iDate

actual class DateTimeHelper<NSDate> {
    actual fun toLocalDateTime(dateTime: NSDate): LocalDateTime {
        val date = dateTime as iDate
        return date.toKotlinInstant().toLocalDateTime(TimeZone.UTC)
    }

    actual fun fromLocalDateTime(dateTime: LocalDateTime): NSDate? {
        val calendar = NSCalendar.currentCalendar
        val components = dateTime.toNSDateComponents()
        val date = calendar.dateFromComponents(components)

        return date as NSDate
    }

    actual fun toStringWithTime(dateTime: NSDate): String {
        val dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy, HH:mm"

        if (NSCalendar.currentCalendar.isDateInToday(dateTime as iDate)) {
            return general_today.get()
        }

        return dateFormatter.stringFromDate(dateTime as iDate)
    }

    actual fun toStringWithTime(dateTime: LocalDateTime): String {
        TODO("Not yet implemented")
    }

    actual fun fromStringWithTime(dateTime: String): NSDate? {
        val dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy, HH:mm"
        val date = dateFormatter.dateFromString(dateTime)

        return date as NSDate
    }

    actual fun fromStringWithTimeToLocalDateTime(dateTime: String): LocalDateTime {
        val dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy, HH:mm"
        val date = dateFormatter.dateFromString(dateTime)
        return date?.toKotlinInstant()?.toLocalDateTime(TimeZone.UTC) ?: LocalDateTime.parse(dateTime)
    }
}