pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven (url = "https://jitpack.io")
    }
}

dependencyResolutionManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven (url = "https://jitpack.io")
    }

    versionCatalogs {
        create("androidLib") { from(files("gradle/android.versions.toml")) }
    }
}

rootProject.name = "Sobuu"
include(":sobuu_androidApp")
include(":shared")