# Sobuu

Sobuu is an Kotlin Multiplatform Mobile (KMM) app, ready to be used in Android (v1.0.0) and 
iOS (under development). It's used to track books reading and organise your readings.
You can also let comments in a page or in the percentage of the book that you feel it should be commented.
Others can read those comments and reply them.
